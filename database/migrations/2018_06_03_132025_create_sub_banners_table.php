<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('alt', 255)->nullable();
            $table->string('url', 255)->nullable();
            $table->string('target', 12);
            $table->tinyInteger('status');
            $table->integer('sort');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_banners');
    }
}
