<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeywordDescriptionToProductsInfosNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('seo_keyword', 80)->nullable();
            $table->text('seo_description')->nullable();
        });

        Schema::table('news', function (Blueprint $table) {
            $table->string('seo_keyword', 80)->nullable();
        });

        Schema::table('infos', function (Blueprint $table) {
            $table->string('seo_keyword', 80)->nullable();
            $table->text('excerpt')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('seo_keyword');
            $table->dropColumn('seo_description');
        });

        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('seo_keyword');
        });

        Schema::table('infos', function (Blueprint $table) {
            $table->dropColumn('seo_keyword');
            $table->text('excerpt')->change();
        });
    }
}
