<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyProductsSpecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products_specifications', function (Blueprint $table) {
            $table->integer('specification_item_id');
            $table->dropColumn('product_category_id');
            $table->dropColumn('product_category_name');
            $table->dropColumn('specification_category_name');
            $table->dropColumn('specification_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
