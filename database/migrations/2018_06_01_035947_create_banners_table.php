<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned()->nullable();
            $table->string('category_code', 128);
            $table->string('name', 255);
            $table->string('alt', 255)->nullable();
            $table->string('url', 255)->nullable();
            $table->string('target', 12);
            $table->tinyInteger('status');
            $table->integer('sort');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('banner_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
