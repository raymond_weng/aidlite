<?php

use Illuminate\Database\Seeder;
use App\Models\BannerCategory;

class BannerCategoriesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        if (BannerCategory::count() == 0) {

            BannerCategory::create([
                'name'           => 'home',
                'code'          => 'home',
            ]);
        }
    }
}
