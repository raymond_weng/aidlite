<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        if (User::count() == 0) {

            User::create([
                'name'           => 'wowyou',
                'email'          => 'wowyou@gmail.com',
                'password'       => bcrypt('54703040'),
                'remember_token' => str_random(60),
            ]);
            User::create([
                'name'           => 'aidlite',
                'email'          => 'aidlite@ms38.hinet.net',
                'password'       => bcrypt('89592267'),
                'remember_token' => str_random(60),
            ]);
        }
    }
}
