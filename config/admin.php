<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'Aidlite',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>A</b>idlite',

    'logo_mini' => '<b>A</b>L',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'blue',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'home',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        [
            'text'    => '產品',
            'icon'    => 'adjust',
            'submenu' => [
                [
                    'text' => '產品分類',
                    'url'  => 'admin/product/categories',
                ],
                [
                    'text' => '規格分類',
                    'url'  => 'admin/product/specification/categories',
                ],
                [
                    'text' => '產品規格',
                    'url'  => 'admin/product/specifications/list',
                ],
                [
                    'text' => '產品',
                    'url'  => 'admin/products',
                ],
            ],
        ],
        [
            'text'    => '消息',
            'icon'    => 'bell',
            'submenu' => [
                [
                    'text' => '消息分類',
                    'url'  => 'admin/news/categories',
                ],
                [
                    'text' => '消息',
                    'url'  => 'admin/news',
                ],
            ],
        ],
        [
            'text'    => '資訊',
            'icon'    => 'bell',
            'submenu' => [
                [
                    'text' => '資訊分類',
                    'url'  => 'admin/info/categories',
                ],
                [
                    'text' => '資訊',
                    'url'  => 'admin/info',
                ],
            ],
        ],
        [
            'text' => 'Banner',
            'icon'  => 'image',
            'submenu' => [
                [
                    'text' => 'Banner',
                    'url'  => 'admin/banners',
                ],
                [
                    'text' => 'SubBanner',
                    'url'  => 'admin/sub/banners',
                ],
            ],
        ],
        [
            'text'    => '關於我們',
            'url'  => 'admin/about',
            'icon'  => 'info',
        ],

    ],

];
