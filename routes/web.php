<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

//about
Route::get('/about', 'AboutController@index');

//Product
Route::get('/productCategories', 'ProductCategoryController@index');
Route::get('/productCategories/{id}', 'ProductCategoryController@show');
Route::get('/product/{id}', 'ProductController@index');
Route::get('/products/search', 'ProductCategoryController@search')->name('product.search');
Route::get('/products/specifications/search', 'ProductCategoryController@searchSpecifications')->name('product.specification.search');

//Info
Route::get('/infoCategories', 'InfoCategoryController@index');
Route::get('/infoCategories/{id}', 'InfoCategoryController@show');
Route::get('/info/{id}', 'InfoController@index');

//News
Route::get('/newsCategories', 'NewsCategoryController@index');
Route::get('/newsCategories/{id}', 'NewsCategoryController@show');
Route::get('/news/{id}', 'NewsController@index');

//Contact
Route::get('/contact', 'ContactController@index');

//Send Mail
Route::get('/sendemail', 'ContactController@send');

//inquiry car
Route::get('/inquiry_car', 'ContactController@inquiry');

//Send Mail
Route::get('/inquiry_send', 'ContactController@inquiry_send');
/*
Route::get('/', function () {
    return view('layouts.default');
});
*/

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin'], function () {

    Route::get('/', 'ProductsController@index')->name('home');
    Route::get('/home', 'ProductsController@index')->name('home.index');

    //Route::get('/news/categories', 'NewsCategoriesController@index')->name('admin.news.category.index');
    Route::resource('/news/categories', 'NewsCategoriesController', [
        'as' => 'news'
    ]);
    Route::resource('/news', 'NewsController');
    Route::resource('/info/categories', 'InfoCategoriesController', [
        'as' => 'info'
    ]);
    Route::resource('/info', 'InfoController');
    Route::resource('/banner/categories', 'BannersCategoriesController', [
        'as' => 'banner'
    ]);
    Route::resource('/banners', 'BannersController');
    Route::resource('/sub/banners', 'SubBannersController', [
        'as' => 'sub'
    ]);
    Route::resource('/product/categories', 'ProductCategoriesController', [
        'as' => 'product'
    ]);
    Route::resource('/product/specification/categories', 'SpecificationCategoriesController', [
        'as' => 'product.specification'
    ]);
    
    Route::get('/product/specifications/list', 'SpecificationsController@list')->name('product.specifications.list');
    Route::resource('/product/specifications', 'SpecificationsController', [
        'as' => 'product'
    ]);

    Route::get('/product/specifications_by_category/{id}', 'SpecificationsController@specificationsByCategoryID');

    Route::resource('/product/specification/items', 'SpecificationItemsController', [
        'as' => 'product.specification'
    ]);
    Route::resource('/products', 'ProductsController');
    Route::get('/products/{id}/copy', 'ProductsController@copy')->name('products.copy');

    Route::get('/about', 'PagesController@index')->name('page.index');
    Route::put('/about/{id}', 'PagesController@update')->name('page.update');
    Route::post('/about', 'PagesController@store')->name('page.store');

    Route::post('/upload/images', 'ImagesController@upload');
    Route::post('/upload/images/delete', 'ImagesController@destroy');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/admin/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('/admin/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
    // list all lfm routes here...
});

Auth::routes();
