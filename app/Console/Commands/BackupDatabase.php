<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class BackupDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup the database';

    protected $process;
    protected $db_name;
    protected $db_folder;

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $dt = Carbon::today();
        $this->db_name = "aidlite_db_".$dt->format('Ym').'.sql';

        $this->db_folder = 'db_backup';
        //判斷必須用絕對路徑
        $filepath = storage_path($this->db_folder); //this fails
        if(!File::exists($filepath)){
            File::makeDirectory($filepath, 0777, true, true);
        };
        //$this->info($db_folder.'/'.$db_name);
        $this->process = new Process(sprintf(
            'mysqldump -u%s -p%s %s > %s',
            config('database.connections.mysql.username'),
            config('database.connections.mysql.password'),
            config('database.connections.mysql.database'),
            //storage_path是storage的絕對路徑
            storage_path($this->db_folder.'/'.$this->db_name)
        ));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            //$this->info($this->process->getCommandLine());
            $this->process->mustRun();

            $data = array(
                'name'=> 'aidlite',
                'email'=> 'aidlite@gmail.com',
                'text'=> '資料庫定期備份',
                'company'=> 'aidlite',
                'date'=> Carbon::today()->format('Ym')
            );

            Mail::send('emails.database_backup', $data, function ($message) {

                $message->from(config('mail.from.address', 'aidlite@gmail.com'));
                $message->to(config('mail.to.address', 'aidlite@gmail.com'));
                $message->subject('資料庫定期備份');
                $message->attach(storage_path($this->db_folder.'/'.$this->db_name), [
                    'as' => $this->db_name,
                ]);
            });

            $this->info('The backup has been proceed successfully.');


        } catch (ProcessFailedException $exception) {
            $this->error('The backup process has been failed.');
        }
    }
}
