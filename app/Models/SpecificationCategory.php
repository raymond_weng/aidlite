<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpecificationCategory extends Model
{
    protected $fillable = ['name', 'sort'];


    public function specifications()
    {
        return $this->hasMany('App\Models\Specification', 'category_id', 'id');
    }
}
