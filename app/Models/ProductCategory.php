<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $fillable = ['name', 'parent_id', 'image', 'featured', 'sort', 'layer', 'sequence'];

    public function products()
    {
        return $this->hasMany('App\Models\Product', 'category_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'parent_id', 'id');
    }

    public function subCategories()
    {
        return $this->hasMany('App\Models\ProductCategory', 'parent_id', 'id');
    }

}
