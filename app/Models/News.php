<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = ['title', 'excerpt', 'seo_keyword', 'content', 'status', 'category_id', 'sort', 'post_date'];

    public function scopePublish($query)
    {
        return $query->where('status', 1);
    }

    public function category()
    {
        return $this->belongsTo('App\Models\NewsCategory', 'category_id', 'id');
    }
}
