<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Specification extends Model
{
    protected $fillable = ['name', 'category_id', 'sort'];

    public function category()
    {
        return $this->belongsTo('App\Models\SpecificationCategory', 'category_id', 'id');
    }

    public function items()
    {
        return $this->hasMany('App\Models\SpecificationItem', 'specification_id', 'id')->orderBy('sort');
    }
}
