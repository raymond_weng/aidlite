<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSpecification extends Model
{
    protected $table = 'products_specifications';

    protected $fillable = ['product_id', 'product_category_id', 'specification_category_id', 'specification_id', 'specification_item_id'];

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id')->with('images');
    }

    public function productCategory()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'product_category_id', 'id');
    }

    public function specificationCategory()
    {
        return $this->belongsTo('App\Models\SpecificationCategory', 'specification_category_id', 'id');
    }

    public function specification()
    {
        return $this->belongsTo('App\Models\Specification', 'specification_id', 'id');
    }

    public function specificationItem()
    {
        return $this->belongsTo('App\Models\SpecificationItem', 'specification_item_id', 'id');
    }
}
