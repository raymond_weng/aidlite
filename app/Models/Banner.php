<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = ['name', 'category_id', 'category_code', 'alt', 'url', 'target', 'status', 'embed', 'sort'];

    public function scopePublish($query)
    {
        return $query->where('status', 1);
    }

    public function bannerCategories()
    {
        return $this->belongsTo('App\Models\BannerCategory', 'category_id', 'id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\Image', 'relation_id', 'id')->where('type', 'banner')->orderBy('sort');
    }
}
