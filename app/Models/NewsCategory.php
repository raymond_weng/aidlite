<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsCategory extends Model
{
    protected $fillable = ['name', 'sort', 'status'];

    public function scopePublish($query)
    {
        return $query->where('status', 1);
    }

    public function news()
    {
        return $this->hasMany('App\Models\News', 'category_id', 'id');
    }
}
