<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpecificationItem extends Model
{
    protected $fillable = ['name', 'specification_id', 'sort'];

    public function specification()
    {
        return $this->belongsTo('App\Models\Specification', 'specification_id', 'id');
    }

}
