<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'category_id', 'model', 'featured', 'specifications', 'video', 'photo', 'additional_information', 'description', 'seo_keyword', 'seo_description'];

    public function scopeFeatured($query)
    {
        return $query->where('featured', 1);
    }

    public function productCategory()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'category_id', 'id');
    }

    public function productSpecifications()
    {
        return $this->hasMany('App\Models\ProductSpecification', 'product_id', 'id')
            ->with('specification')
            ->with('specificationItem');
    }

    public function images()
    {
        return $this->hasMany('App\Models\Image', 'relation_id', 'id')->where('type', 'product')->orderBy('sort');
    }

    public function productAccessories()
    {
        return $this->belongsToMany('App\Models\Product', 'products_accessories', 'id', 'product_id');
    }
}
