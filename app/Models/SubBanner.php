<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubBanner extends Model
{
    protected $fillable = ['name', 'alt', 'url', 'target', 'status', 'sort'];

    public function scopePublish($query)
    {
        return $query->where('status', 1);
    }

    public function images()
    {
        return $this->hasMany('App\Models\Image', 'relation_id', 'id')->where('type', 'subbanner')->orderBy('sort');
    }
}
