<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BannerCategory extends Model
{
    protected $fillable = ['name', 'code'];

    public function scopePublish($query)
    {
        return $query->where('status', 1);
    }

    public function banners()
    {
        return $this->hasMany('App\Models\Banner', 'category_id', 'id');
    }
}
