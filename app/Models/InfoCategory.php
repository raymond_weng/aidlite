<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfoCategory extends Model
{
    protected $fillable = ['name', 'sort', 'status'];


    public function scopePublish($query)
    {
        return $query->where('status', 1);
    }

    public function infos()
    {
        return $this->hasMany('App\Models\Info', 'category_id', 'id');
    }
}
