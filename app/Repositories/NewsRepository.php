<?php
/**
 * Created by PhpStorm.
 * User: raymondweng
 * Date: 2016/12/16
 * Time: 上午2:39
 */

namespace App\Repositories;

use App\Models\NewsCategory;
use App\Models\News;
use Exception;

class NewsRepository
{
    protected $news;

    /**
     * UserRepository constructor.
     * @param news $news
     */
    public function __construct(News $news)
    {
        $this->news = $news;
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return news
     *
     * role->news,product_m,logistics_m,finance
     */
    public function create(array $data)
    {

        $news = $this->news->create($data);

        //dd($news);
        return $news;
    }

    public function update(array $data, $id)
    {
        //dd($data);
        $news = $this->news->find($id);
        //dd($news);
        if ($news) {
            if ($data['title'])$news['title'] = $data['title'];
            if ($data['excerpt'])$news['excerpt'] = $data['excerpt'];
            if ($data['seo_keyword'])$info['seo_keyword'] = $data['seo_keyword'];
            if ($data['content'])$news['content'] = $data['content'];
            if ($data['status'])$news['status'] = $data['status'];
            if ($data['category_id'])$news['category_id'] = $data['category_id'];

            $news->save();
        }
        //dd($news);
        return $news;
    }

    public function news($id)
    {
        $news= $this->news->find($id);
        return $news;
    }
    
    public function newsList()
    {
        $news = $this->news->orderBy('post_date', 'desc')->paginate(20);
        //dd($news);
        return $news;
    }

    public function newsListByPublish()
    {
        $news = $this->news->publish()->orderBy('post_date', 'desc')->paginate(20);
        //dd($news);
        return $news;
    }

    public function newsWithCategory($id)
    {
        $news= $this->news->with('category')->orderBy('post_date', 'desc')->find($id);
        return $news;
    }

    public function newsListWithCategory()
    {
        $news = $this->news->with('category')->orderBy('post_date', 'desc')->paginate(20);
        //dd($news);
        return $news;
    }

    public function newsListWithCategoryByPublish()
    {
        $news = $this->news->with('category')->publish()->orderBy('post_date', 'desc')->paginate(20);
        //dd($news);
        return $news;
    }

    public function newsListByCategory($id)
    {
        $news = $this->news->where('category_id', '=', $id)->orderBy('post_date', 'desc')->paginate(20);
        return $news;
    }

    public function newsListByCategoryAndPublish($id)
    {
        $news = $this->news->where('category_id', '=', $id)->publish()->orderBy('post_date', 'desc')->paginate(20);
        return $news;
    }

    public function delete($id)
    {
        $news= $this->news->find($id);
        if ($news->delete()) {
            return true;
        }
        return false;
    }

}