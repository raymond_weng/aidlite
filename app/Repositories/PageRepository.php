<?php
/**
 * Created by PhpStorm.
 * User: raymondweng
 * Date: 2016/12/16
 * Time: 上午2:39
 */

namespace App\Repositories;

use App\Models\Page;
use Exception;

class PageRepository
{
    protected $page;

    /**
     * UserRepository constructor.
     * @param page $page
     */
    public function __construct(Page $page)
    {
        $this->page = $page;
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return page
     *
     * role->page,product_m,logistics_m,finance
     */
    public function create(array $data)
    {

        $page = $this->page->create($data);

        //dd($page);
        return $page;
    }

    public function update(array $data, $id)
    {
        //dd($data);
        $page = $this->page->find($id);
        //dd($page);
        if ($page) {
            if ($data['title'])$page['title'] = $data['title'];
            if ($data['excerpt'])$page['excerpt'] = $data['excerpt'];
            if ($data['content'])$page['content'] = $data['content'];
            if ($data['status'])$page['status'] = $data['status'];
            if ($data['type'])$page['type'] = $data['type'];

            $page->save();
        }
        //dd($page);
        return $page;
    }

    public function page($id)
    {
        $page = $this->page->find($id);
        return $page;
    }

    public function pageByType($type)
    {
        $page = $this->page->where('type', $type)->first();
        return $page;
    }

    public function pages()
    {
        $page = $this->page->get();
        //dd($page);
        return $page;
    }

    public function delete($id)
    {
        $page = $this->page->find($id);
        if ($page->delete()) {
            return true;
        }
        return false;
    }

}