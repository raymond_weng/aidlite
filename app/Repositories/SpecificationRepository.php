<?php
/**
 * Created by PhpStorm.
 * User: raymondweng
 * Date: 2016/12/16
 * Time: 上午2:39
 */

namespace App\Repositories;

use App\Models\Specification;
use Exception;

class SpecificationRepository
{
    protected $specification;

    /**
     * UserRepository constructor.
     * @param Specification $Specification
     */
    public function __construct(Specification $specification)
    {
        $this->specification = $specification;
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Specification
     *
     * role->Specification,product_m,logistics_m,finance
     */
    public function create(array $data)
    {

        $item = $this->specification->create($data);

        //dd($item);
        return $item;
    }

    public function update(array $data, $id)
    {
        //dd($data);
        $item = $this->specification->find($id);
        //dd($item);
        if ($item) {
            if ($data['name'])$item->name = $data['name'];
            if ($data['category_id'])$item->category_id = $data['category_id'];
            if ($data['sort'])$item->sort = $data['sort'];
            $item->save();
        }
        //dd($item);
        return $item;
    }

    public function specification($id)
    {
        $item= $this->specification->find($id);
        return $item;
    }

    public function specificationWithCategory($id)
    {
        $item= $this->specification->with('category')->find($id);
        return $item;
    }

    public function specifications()
    {
        $items = $this->specification->orderBy('sort')->get();
        //dd($items);
        return $items;
    }

    public function specificationsWithCategory()
    {
        $items = $this->specification->with('category')->orderBy('sort')->get();
        //dd($items);
        return $items;
    }

    public function specificationsWithPaging()
    {
        $items = $this->specification->orderBy('sort')->paginate(20);
        //dd($items);
        return $items;
    }

    public function specificationsWithCategoryAndPaging($id)
    {
        $items = $this->specification->with('category')->where('category_id', $id)->orderBy('sort')->paginate(20);
        //dd($items);
        return $items;
    }

    public function specificationsWithItems()
    {
        $items = $this->specification->with('items')->orderBy('sort')->get();
        //dd($items);
        return $items;
    }

    public function specificationsWithItemsByCategory($id)
    {
        $items = $this->specification->where('category_id', $id)->with('items')->orderBy('sort')->get();
        //dd($items);
        return $items;
    }

    public function delete($id)
    {
        $item= $this->specification->find($id);
        if ($item->delete()) {
            return true;
        }
        return false;
    }

}