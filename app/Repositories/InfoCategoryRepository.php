<?php
/**
 * Created by PhpStorm.
 * User: raymondweng
 * Date: 2016/12/16
 * Time: 上午2:39
 */

namespace App\Repositories;

use App\Models\InfoCategory;
use Exception;

class InfoCategoryRepository
{
    protected $infoCategory;

    /**
     * UserRepository constructor.
     * @param infoCategory $infoCategory
     */
    public function __construct(InfoCategory $infoCategory)
    {
        $this->infoCategory = $infoCategory;
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return infoCategory
     *
     * role->infoCategory,product_m,logistics_m,finance
     */
    public function create(array $data)
    {

        $infoCategory = $this->infoCategory->create($data);

        //dd($infoCategory);
        return $infoCategory;
    }

    public function update(array $data, $id)
    {
        //dd($data);
        $infoCategory = $this->infoCategory->find($id);
        //dd($infoCategory);
        if ($infoCategory) {
            if ($data['name'])$infoCategory['name'] = $data['name'];
            if ($data['sort'])$infoCategory['sort'] = $data['sort'];
            $infoCategory->save();
        }
        //dd($infoCategory);
        return $infoCategory;
    }

    public function infoCategory($id)
    {
        $infoCategory = $this->infoCategory->find($id);
        return $infoCategory;
    }

    public function infoCategoryFirst()
    {
        $infoCategory = $this->infoCategory->orderBy('sort')->first();
        return $infoCategory;
    }

    public function infoCategories()
    {
        $infoCategories = $this->infoCategory->orderBy('sort')->get();
        //dd($infoCategorys);
        return $infoCategories;
    }

    public function infoCategoriesByPublish()
    {
        $infoCategories = $this->infoCategory->publish()->orderBy('sort')->get();
        //dd($infoCategorys);
        return $infoCategories;
    }

    public function infoCategoriesWithPaging()
    {
        $infoCategories = $this->infoCategory->orderBy('sort')->paginate(20);
        //dd($infoCategorys);
        return $infoCategories;
    }

    public function infoCategoriesWithPagingByPublish()
    {
        $infoCategories = $this->infoCategory->publish()->orderBy('sort')->paginate(20);
        //dd($infoCategorys);
        return $infoCategories;
    }

    public function categoryWithinfo($id)
    {
        $infoCategory= $this->infoCategory->with('info')->find($id);
        return $infoCategory;
    }

    public function categoriesWithinfo()
    {
        $infoCategories = $this->infoCategory->with('info')->orderBy('sort')->paginate(20);
        //dd($infoCategorys);
        return $infoCategories;
    }

    public function categoriesWithinfoByPublish()
    {
        $infoCategories = $this->infoCategory->publish()->with('info')->orderBy('sort')->paginate(20);
        //dd($infoCategorys);
        return $infoCategories;
    }

    public function delete($id)
    {
        $infoCategory = $this->infoCategory->find($id);
        if ($infoCategory->delete()) {
            return true;
        }
        return false;
    }

}