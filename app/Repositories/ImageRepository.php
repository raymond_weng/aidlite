<?php
/**
 * Created by PhpStorm.
 * User: raymondweng
 * Date: 2016/12/16
 * Time: 上午2:39
 */

namespace App\Repositories;

use App\Models\Image;
use Exception;
use Illuminate\Support\Facades\Storage;

class ImageRepository
{
    protected $image;

    /**
     * UserRepository constructor.
     * @param image $image
     */
    public function __construct(Image $image)
    {
        $this->image = $image;
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return image
     *
     * role->image,product_m,logistics_m,finance
     */
    public function create(array $data)
    {

        $image = $this->image->create($data);

        //dd($image);
        return $image;
    }
    //['relation_id', 'type', 'image', 'is_temp', 'sort'];
    public function update(array $data, $id)
    {
        //dd($data);
        $image = $this->image->find($id);
        //dd($image);
        if ($image) {
            if ($data['image'])$image['image'] = $data['image'];
            if ($data['is_temp'])$image['is_temp'] = $data['is_temp'];
            if ($data['sort'])$image['sort'] = $data['sort'];
            $image->save();
        }
        //dd($image);
        return $image;
    }

    public function images($relation_id, $type)
    {
        //dd($data);
        $images = $this->image->where('relation_id', $relation_id)
            ->where('type', $type)
            ->orderBy('sort')
            ->get();
        //dd($images);
        return $images;
    }

    public function changeTempByRelationAndType($relation_id, $type, $temp)
    {
        //dd($data);
        $images = $this->image->where('relation_id', $relation_id)
            ->where('type', $type)
            ->update(['is_temp' => 0]);
        //dd($images);
        return $images;
    }

    public function image($id)
    {
        $image = $this->image->find($id);
        return $image;
    }

    public function delete($id)
    {
        $image = $this->image->find($id);
        if ($image->delete()) {
            $exists = Storage::exists($image->image);
            if ($exists) {
                Storage::delete($image->image);
            }
            return true;
        }
        return false;
    }

    public function deleteByRelationAndType($relation_id, $type)
    {
        $images = $this->image->where('relation_id', $relation_id)
            ->where('type', $type)
            ->get();
        if ($images) {
            foreach($images as $image) {
                $this->delete($image->id);
            }
            return true;
        }
        return false;
    }

    public function deleteByTemp()
    {
        $images = $this->image->where("is_temp", false)->get();
        if ($images) {
            foreach($images as $image) {
                $this->delete($image->id);
            }
            return true;
        }
        return false;
    }
}