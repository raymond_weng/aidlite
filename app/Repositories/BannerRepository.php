<?php
/**
 * Created by PhpStorm.
 * User: raymondweng
 * Date: 2016/12/16
 * Time: 上午2:39
 */

namespace App\Repositories;

use App\Models\Banner;
use Exception;

class BannerRepository
{
    protected $banner;

    /**
     * UserRepository constructor.
     * @param banner $banner
     */
    public function __construct(Banner $banner)
    {
        $this->banner = $banner;
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return banner
     *
     * role->banner,product_m,logistics_m,finance
     */
    public function create(array $data)
    {

        $banner = $this->banner->create($data);

        //dd($banner);
        return $banner;
    }

    public function update(array $data, $id)
    {
        //dd($data);
        $banner = $this->banner->find($id);
        //dd($banner);
        if ($banner) {
            if ($data['name'])$banner['name'] = $data['name'];
            if ($data['category_id'])$banner['category_id'] = $data['category_id'];
            if ($data['category_code'])$banner['category_code'] = $data['category_code'];
            if ($data['image'])$banner['image'] = $data['image'];
            if ($data['alt'])$banner['alt'] = $data['alt'];
            if ($data['url'])$banner['url'] = $data['url'];
            if ($data['target'])$banner['target'] = $data['target'];
            if ($data['status'])$banner['status'] = $data['status'];
            if ($data['embed'])$banner['embed'] = $data['embed'];
            if ($data['sort'])$banner['sort'] = $data['sort'];
            $banner->save();
        }
        //dd($banner);
        return $banner;
    }

    public function banner($id)
    {
        $banner = $this->banner->with('images')->find($id);
        return $banner;
    }
    
    public function banners()
    {
        $banners = $this->banner->with('images')->orderBy('sort')->get();
        //dd($banners);
        return $banners;
    }

    public function bannersWithPaging()
    {
        $banners = $this->banner->with('images')->orderBy('sort')->paginate(20);
        //dd($banners);
        return $banners;
    }

    public function bannersByCategoryID($id)
    {
        $banners = $this->banner->where('category_id', $id)->with('images')->orderBy('sort')->get();
        //dd($banners);
        return $banners;
    }

    public function bannersWithPagingByCategoryID($id)
    {
        $banners = $this->banner->where('category_id', $id)->with('images')->orderBy('sort')->paginate(20);
        //dd($banners);
        return $banners;
    }

    public function bannersByCategoryCode($code)
    {
        $banners = $this->banner->where('category_code', $code)->with('images')->orderBy('sort')->get();
        //dd($banners);
        return $banners;
    }

    public function bannersWithPagingByCategoryCode($code)
    {
        $banners = $this->banner->where('category_code', $code)->with('images')->orderBy('sort')->paginate(20);
        //dd($banners);
        return $banners;
    }

    public function bannersByPublish()
    {
        $banners = $this->banner->publish()->with('images')->orderBy('sort')->get();
        //dd($banners);
        return $banners;
    }

    public function bannersWithPagingByPublish()
    {
        $banners = $this->banner->publish()->with('images')->orderBy('sort')->paginate(20);
        //dd($banners);
        return $banners;
    }

    public function bannersByPublishAndCategoryID($id)
    {
        $banners = $this->banner->where('category_id', $id)->publish()->with('images')->orderBy('sort')->get();
        //dd($banners);
        return $banners;
    }

    public function bannersWithPagingByPublishAndCategoryID($id)
    {
        $banners = $this->banner->where('category_id', $id)->publish()->with('images')->orderBy('sort')->paginate(20);
        //dd($banners);
        return $banners;
    }

    public function bannersByPublishAndCategoryCode($code)
    {
        $banners = $this->banner->where('category_code', $code)->publish()->with('images')->orderBy('sort')->get();
        //dd($banners);
        return $banners;
    }

    public function bannersWithPagingByPublishAndCategoryCode($code)
    {
        $banners = $this->banner->where('category_code', $code)->publish()->with('images')->orderBy('sort')->paginate(20);
        //dd($banners);
        return $banners;
    }

    public function delete($id)
    {
        $banner = $this->banner->find($id);
        if ($banner->delete()) {
            return true;
        }
        return false;
    }

}