<?php
/**
 * Created by PhpStorm.
 * User: raymondweng
 * Date: 2016/12/16
 * Time: 上午2:39
 */

namespace App\Repositories;

use App\Models\InfoCategory;
use App\Models\Info;
use Exception;

class InfoRepository
{
    protected $info;

    /**
     * UserRepository constructor.
     * @param info $info
     */
    public function __construct(Info $info)
    {
        $this->info = $info;
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return info
     *
     * role->info,product_m,logistics_m,finance
     */
    public function create(array $data)
    {

        $info = $this->info->create($data);

        //dd($info);
        return $info;
    }

    public function update(array $data, $id)
    {
        //dd($data);
        $info = $this->info->find($id);
        //dd($info);
        if ($info) {
            if ($data['title'])$info['title'] = $data['title'];
            if ($data['excerpt'])$info['excerpt'] = $data['excerpt'];
            if ($data['seo_keyword'])$info['seo_keyword'] = $data['seo_keyword'];
            if ($data['content'])$info['content'] = $data['content'];
            if ($data['status'])$info['status'] = $data['status'];
            if ($data['category_id'])$info['category_id'] = $data['category_id'];

            $info->save();
        }
        //dd($info);
        return $info;
    }

    public function info($id)
    {
        $info = $this->info->find($id);
        return $info;
    }

    public function infoList()
    {
        $infoList = $this->info->orderBy('post_date', 'desc')->paginate(20);
        //dd($infoList);
        return $infoList;
    }

    public function infoListByPublish()
    {
        $infoList = $this->info->publish()->orderBy('post_date', 'desc')->paginate(20);
        //dd($infoList);
        return $infoList;
    }

    public function homeInfoListByPublish()
    {
        $infoList = $this->info->publish()->orderBy('post_date', 'desc')->limit(8)->get();
        //dd($infoList);
        return $infoList;
    }

    public function infoWithCategory($id)
    {
        $info= $this->info->with('category')->orderBy('post_date', 'desc')->find($id);
        return $info;
    }

    public function infoListWithCategory()
    {
        $infoList = $this->info->with('category')->orderBy('post_date', 'desc')->paginate(20);
        //dd($infoList);
        return $infoList;
    }

    public function infoListWithCategoryByPublish()
    {
        $infoList = $this->info->with('category')->publish()->orderBy('post_date', 'desc')->paginate(20);
        //dd($infoList);
        return $infoList;
    }

    public function infoListByCategory($id)
    {
        $infoList = $this->info->where('category_id', '=', $id)->orderBy('post_date', 'desc')->paginate(20);
        return $infoList;
    }

    public function infoListByCategoryAndPublish($id)
    {
        $infoList = $this->info->where('category_id', '=', $id)->publish()->orderBy('post_date', 'desc')->paginate(20);
        return $infoList;
    }

    public function delete($id)
    {
        $info = $this->info->find($id);
        if ($info->delete()) {
            return true;
        }
        return false;
    }

}