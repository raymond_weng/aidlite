<?php
/**
 * Created by PhpStorm.
 * User: raymondweng
 * Date: 2016/12/16
 * Time: 上午2:39
 */

namespace App\Repositories;

use App\Models\SubBanner;
use Exception;

class SubBannerRepository
{
    protected $banner;

    /**
     * UserRepository constructor.
     * @param banner $banner
     */
    public function __construct(SubBanner $banner)
    {
        $this->banner = $banner;
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return banner
     *
     * role->banner,product_m,logistics_m,finance
     */
    public function create(array $data)
    {

        $banner = $this->banner->create($data);

        //dd($banner);
        return $banner;
    }

    public function update(array $data, $id)
    {
        //dd($data);
        $banner = $this->banner->find($id);
        //dd($banner);
        if ($banner) {
            if ($data['name'])$banner['name'] = $data['name'];
            if ($data['alt'])$banner['alt'] = $data['alt'];
            if ($data['url'])$banner['url'] = $data['url'];
            if ($data['target'])$banner['target'] = $data['target'];
            if ($data['status'])$banner['status'] = $data['status'];
            if ($data['sort'])$banner['sort'] = $data['sort'];
            $banner->save();
        }
        //dd($banner);
        return $banner;
    }

    public function banner($id)
    {
        $banner = $this->banner->with('images')->find($id);
        return $banner;
    }
    
    public function banners()
    {
        $banners = $this->banner->with('images')->publish()->orderBy('sort')->get();
        //dd($banners);
        return $banners;
    }

    public function bannersWithPaging()
    {
        $banners = $this->banner->with('images')->orderBy('sort')->paginate(20);
        //dd($banners);
        return $banners;
    }

    public function bannersByPublish()
    {
        $banners = $this->banner->publish()->with('images')->orderBy('sort')->get();
        //dd($banners);
        return $banners;
    }

    public function bannersWithPagingByPublish()
    {
        $banners = $this->banner->publish()->with('images')->orderBy('sort')->paginate(20);
        //dd($banners);
        return $banners;
    }

    public function delete($id)
    {
        $banner = $this->banner->find($id);
        if ($banner->delete()) {
            return true;
        }
        return false;
    }

}