<?php
/**
 * Created by PhpStorm.
 * User: raymondweng
 * Date: 2016/12/16
 * Time: 上午2:39
 */

namespace App\Repositories;

use App\Models\ProductCategory;
use Exception;

class ProductCategoryRepository
{
    protected $category;

    /**
     * UserRepository constructor.
     * @param Category $category
     */
    public function __construct(ProductCategory $category)
    {
        $this->category = $category;
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return category
     *
     * role->category,product_m,logistics_m,finance
     */
    public function create(array $data)
    {

        $category = $this->category->create($data);

        //dd($category);
        return $category;
    }

    public function update(array $data, $id)
    {
        //dd($data);
        $category = $this->category->find($id);
        //dd($category);
        if ($category) {
            if ($data['name'])$category['name'] = $data['name'];
            if ($data['parent_id'])$category['parent_id'] = $data['parent_id'];
            if ($data['featured'])$category['featured'] = $data['featured'];
            if ($data['sort'])$category['sort'] = $data['sort'];
            if ($data['layer'])$category['layer'] = $data['layer'];
            if ($data['sequence'])$category['sequence'] = $data['sequence'];
            $category->save();
        }
        //dd($category);
        return $category;
    }

    function categoryUpdateForSequence($seq, $oldSeq, $id) {
        if ($seq > $oldSeq) {
    	    $categories = $this->category->where('sequence', '<=', $seq)->where('sequence', '>', $oldSeq)->where('id', '<>', $id)->orderBy('sequence', 'desc')->get();
    	    //dd($categories);
    	    if (count($categories) > 0) {
                foreach ($categories as $key => $value) {
                    $category = $this->category->find($value['id']);
                    $category['sequence'] = $value['sequence'] - 1;
                    //dd($category);
                    $category->save();
                }
            }
        }
        if ($seq < $oldSeq) {
            $categories = $this->category->where('sequence', '>=', $seq)->where('sequence', '<', $oldSeq)->where('id', '<>', $id)->orderBy('sequence', 'desc')->get();
            if (count($categories) > 0) {
                foreach ($categories as $key => $value) {
                	//dd($categories);
                    $category = $this->category->find($value['id']);
                    $category['sequence'] = $value['sequence'] + 1;
                    //dd($category);
                    $category->save();
                }
            }
        }
    	//dd($categories);
    	
        return true;
    }

    function categoryUpdateForNewSequence($seq) {
        $categories = $this->category->where('sequence', '>=', $seq)->orderBy('sequence', 'desc')->get();
        //dd($categories);
        if (count($categories) > 0) {
            foreach ($categories as $key => $value) {
            	//dd($categories);
                $category = $this->category->find($value['id']);
                $category['sequence'] = $value['sequence'] + 1;
                //dd($category);
                $category->save();
            }
        }
    	//dd($categories);
    	
        return true;
    }

    function categoryUpdateForDeleteSequence($seq) {
        $categories = $this->category->where('sequence', '>', $seq)->orderBy('sequence', 'asc')->get();
        //dd($categories);
        if (count($categories) > 0) {
            foreach ($categories as $key => $value) {
            	//dd($categories);
                $category = $this->category->find($value['id']);
                $category['sequence'] = $value['sequence'] - 1;
                //dd($category);
                $category->save();
            }
        }
    	//dd($categories);
    	
        return true;
    }

    public function category($id)
    {
        $category= $this->category->find($id);
        return $category;
    }
    
    public function categories()
    {
        $categories = $this->category->with('parent')->orderBy('sequence')->with('subCategories')->get();

        return $categories;
    }

    public function categoriesWithoutSelf($id)
    {
        $categories = $this->category->with('parent')->where('id', '<>', $id)->orderBy('sequence')->with('subCategories')->get();

        return $categories;
    }

    public function categoriesByRoot()
    {
        $categories = $this->category->where('parent_id', NULL)->orWhere('parent_id', 0)->with('parent')->orderBy('sequence')->with('subCategories')->get();

        return $categories;
    }

    public function categoriesByParent($parent_id, $id='')
    {
    	if ($id != '') {
            $categories = $this->category->where('parent_id', $parent_id)->where('id', '<>', $id)->with('parent')->orderBy('sequence', 'desc')->with('subCategories')->get();
        } else {
            $categories = $this->category->where('parent_id', $parent_id)->with('parent')->orderBy('sequence', 'desc')->with('subCategories')->get();
        }

        return $categories;
    }

    public function categoriesWithPaging()
    {
        $categories = $this->category->orderBy('sequence')->paginate(20);
        //dd($categorys);
        return $categories;
    }

    public function categoriesWithPagingByCategory($id)
    {
        $categories = $this->category->where('parent_id', '=', $id)->orderBy('sequence')->paginate(20);
        //dd($categories);
        return $categories;
    }

    public function categoryFirst()
    {
        $category = $this->category->with('products')->orderBy('sequence')->first();
        return $category;
    }

    public function categoryLastSequence()
    {
        $category = $this->category->with('products')->orderBy('sequence', 'desc')->first();
        return $category;
    }

    public function categoryWithProducts($id)
    {
        $category = $this->category->with('products')->find($id);
        return $category;
    }

    public function categoryWithProductsAndPaging($id)
    {
        $category = $this->category->with('products')->find($id)->paginate(20);
        return $category;
    }

    public function categoriesWithProducts()
    {
        $categories = $this->category->with('products')->orderBy('sequence')->get();
        //dd($categorys);
        return $categories;
    }

    public function categoriesWithProductsAndPaging()
    {
        $categories = $this->category->with('products')->orderBy('sequence')->paginate(20);
        //dd($categorys);
        return $categories;
    }

    public function categoriesByFeatured()
    {
        $categories = $this->category->where('featured', '=', '1')->orderBy('sequence')->get();
        //dd($categorys);
        return $categories;
    }

    public function categoryByParent($id)
    {
        $categories = $this->category->where('parent_id', '=', $id)->get();
        return $categories;
    }

    public function delete($id)
    {
        $category = $this->category->find($id);
        if ($category->delete()) {
            return true;
        }
        return false;
    }

}