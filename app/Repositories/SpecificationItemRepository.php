<?php
/**
 * Created by PhpStorm.
 * User: raymondweng
 * Date: 2016/12/16
 * Time: 上午2:39
 */

namespace App\Repositories;


use App\Models\SpecificationItem;
use Exception;

class SpecificationItemRepository
{
    protected $item;

    /**
     * UserRepository constructor.
     * @param item $item
     */
    public function __construct(SpecificationItem $item)
    {
        $this->item = $item;
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return item
     *
     * role->item,product_m,logistics_m,finance
     */
    public function create(array $data)
    {

        $item = $this->item->create($data);

        //dd($item);
        return $item;
    }

    public function update(array $data, $id)
    {
        //dd($data);
        $item = $this->item->find($id);
        //dd($item);
        if ($item) {
            if ($data['name'])$item->name = $data['name'];
            if ($data['specification_id'])$item->specification_id = $data['specification_id'];
            if ($data['sort'])$item->sort = $data['sort'];
            $item->save();
        }
        //dd($item);
        return $item;
    }

    public function item($id)
    {
        $item= $this->item->find($id);
        return $item;
    }

    public function itemWithSpecification($id)
    {
        $item= $this->item->with('specification')->find($id);
        return $item;
    }

    public function items()
    {
        $items = $this->item->orderBy('sort')->get();
        //dd($items);
        return $items;
    }

    public function itemsWithSpecification()
    {
        $items = $this->item->with('specification')->orderBy('sort')->get();
        //dd($items);
        return $items;
    }

    public function itemsWithPaging()
    {
        $items = $this->item->orderBy('sort')->paginate(20);
        //dd($items);
        return $items;
    }

    public function itemsWithSpecificationAndPaging($id)
    {
        $items = $this->item->with('specification')->where('specification_id', $id)->orderBy('sort')->paginate(20);
        //dd($items);
        return $items;
    }

    public function delete($id)
    {
        $item= $this->item->find($id);
        if ($item->delete()) {
            return true;
        }
        return false;
    }

    public function specificationItemCountBySpecification($id) {
        $item = $this->item->where('specification_id', $id)->count();
        return $item;
    }

}