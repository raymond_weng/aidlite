<?php
/**
 * Created by PhpStorm.
 * User: raymondweng
 * Date: 2016/12/16
 * Time: 上午2:39
 */

namespace App\Repositories;

use App\Models\SpecificationCategory;
use Exception;

class SpecificationCategoryRepository
{
    protected $category;

    /**
     * UserRepository constructor.
     * @param Category $category
     */
    public function __construct(SpecificationCategory $category)
    {
        $this->category = $category;
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return category
     *
     * role->category,product_m,logistics_m,finance
     */
    public function create(array $data)
    {

        $category = $this->category->create($data);

        //dd($category);
        return $category;
    }

    public function update(array $data, $id)
    {
        //dd($data);
        $category = $this->category->find($id);
        //dd($category);
        if ($category) {
            if ($data['name'])$$category->name = $data['name'];
            if ($data['sort'])$category->sort = $data['sort'];
            $category->save();
        }
        //dd($category);
        return $category;
    }

    public function category($id)
    {
        $category= $this->category->find($id);
        return $category;
    }

    public function categories()
    {
        $categories = $this->category->orderBy('sort')->get();
        //dd($categorys);
        return $categories;
    }

    public function categoriesByParentID($id)
    {
        $categories = $this->category->where('category_id', '=', $id)->orderBy('sort')->get();
        //dd($categorys);
        return $categories;
    }

    public function categoriesWithPaging()
    {
        $categories = $this->category->orderBy('sort')->paginate(20);
        //dd($categorys);
        return $categories;
    }

    public function categoryFirst()
    {
        $category = $this->category->orderBy('sort')->first();
        return $category;
    }

    public function categoryWithSpecifications($id)
    {
        $category = $this->category->with('specifications')->find($id);
        return $category;
    }

    public function categoriesWithSpecifications()
    {
        $categories = $this->category->with('specifications')->orderBy('sort')->get();
        //dd($categorys);
        return $categories;
    }

    public function categoriesWithSpecificationsAndPaging()
    {
        $categories = $this->category->with('specifications')->orderBy('sort')->paginate(20);
        //dd($categorys);
        return $categories;
    }

    public function delete($id)
    {
        $category = $this->category->find($id);
        if ($category->delete()) {
            return true;
        }
        return false;
    }

}