<?php
/**
 * Created by PhpStorm.
 * User: raymondweng
 * Date: 2016/12/16
 * Time: 上午2:39
 */

namespace App\Repositories;

use App\Models\BannerCategory;
use Exception;

class BannerCategoryRepository
{
    protected $category;

    /**
     * UserRepository constructor.
     * @param category $category
     */
    public function __construct(BannerCategory $category)
    {
        $this->category = $category;
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return category
     *
     * role->category,product_m,logistics_m,finance
     */
    public function create(array $data)
    {

        $category = $this->category->create($data);

        //dd($category);
        return $category;
    }

    public function update(array $data, $id)
    {
        //dd($data);
        $category = $this->category->find($id);
        //dd($category);
        if ($category) {
            if ($data['name'])$category['name'] = $data['name'];
            if ($data['code'])$category['code'] = $data['code'];
            $category->save();
        }
        //dd($category);
        return $category;
    }

    public function category($id)
    {
        $category = $this->category->find($id);
        return $category;
    }

    public function categoryFirstByPublish()
    {
        $category = $this->category->publish()->first();
        return $category;
    }

    public function categories()
    {
        $categories = $this->category->get();
        //dd($categorys);
        return $categories;
    }

    public function categoriesWithPaging()
    {
        $categories = $this->category->paginate(20);
        //dd($categorys);
        return $categories;
    }

    public function categoriesByPublish()
    {
        $categories = $this->category->publish()->get();
        //dd($categorys);
        return $categories;
    }

    public function categoriesWithPagingByPublish()
    {
        $categories = $this->category->publish()->paginate(20);
        //dd($categorys);
        return $categories;
    }

    public function categoryWithBanners($id)
    {
        $category= $this->category->with('banners')->find($id);
        return $category;
    }

    public function categoriesWithBannersByPublish()
    {
        $categories = $this->category->publish()->with('banners')->paginate(20);
        //dd($categorys);
        return $categories;
    }

    public function delete($id)
    {
        $category= $this->category->find($id);
        if ($category->delete()) {
            return true;
        }
        return false;
    }

}