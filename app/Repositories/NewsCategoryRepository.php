<?php
/**
 * Created by PhpStorm.
 * User: raymondweng
 * Date: 2016/12/16
 * Time: 上午2:39
 */

namespace App\Repositories;

use App\Models\NewsCategory;
use Exception;

class NewsCategoryRepository
{
    protected $newsCategory;

    /**
     * UserRepository constructor.
     * @param newsCategory $newsCategory
     */
    public function __construct(NewsCategory $newsCategory)
    {
        $this->newsCategory = $newsCategory;
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return newsCategory
     *
     * role->newsCategory,product_m,logistics_m,finance
     */
    public function create(array $data)
    {

        $newsCategory = $this->newsCategory->create($data);

        //dd($newsCategory);
        return $newsCategory;
    }

    public function update(array $data, $id)
    {
        //dd($data);
        $newsCategory = $this->newsCategory->find($id);
        //dd($newsCategory);
        if ($newsCategory) {
            if ($data['name'])$newsCategory['name'] = $data['name'];
            if ($data['sort'])$newsCategory['sort'] = $data['sort'];
            $newsCategory->save();
        }
        //dd($newsCategory);
        return $newsCategory;
    }

    public function newsCategory($id)
    {
        $newsCategory = $this->newsCategory->find($id);
        return $newsCategory;
    }

    public function newsCategoryFirst()
    {
        $newsCategory = $this->newsCategory->orderBy('sort')->first();
        return $newsCategory;
    }

    public function newsCategories()
    {
        $newsCategories = $this->newsCategory->orderBy('sort')->get();
        //dd($newsCategorys);
        return $newsCategories;
    }

    public function newsCategoriesByPublish()
    {
        $newsCategories = $this->newsCategory->publish()->orderBy('sort')->get();
        //dd($newsCategorys);
        return $newsCategories;
    }

    public function newsCategoriesWithPaging()
    {
        $newsCategories = $this->newsCategory->orderBy('sort')->paginate(20);
        //dd($newsCategorys);
        return $newsCategories;
    }

    public function newsCategoriesWithPagingByPublish()
    {
        $newsCategories = $this->newsCategory->publish()->orderBy('sort')->paginate(20);
        //dd($newsCategorys);
        return $newsCategories;
    }

    public function categoryWithNews($id)
    {
        $newsCategory= $this->newsCategory->with('news')->find($id);
        return $newsCategory;
    }

    public function categoriesWithNews()
    {
        $newsCategories = $this->newsCategory->with('news')->orderBy('sort')->paginate(20);
        //dd($newsCategorys);
        return $newsCategories;
    }

    public function categoriesWithNewsByPublish()
    {
        $newsCategories = $this->newsCategory->publish()->with('news')->orderBy('sort')->paginate(20);
        //dd($newsCategorys);
        return $newsCategories;
    }

    public function delete($id)
    {
        $newsCategory= $this->newsCategory->find($id);
        if ($newsCategory->delete()) {
            return true;
        }
        return false;
    }

}