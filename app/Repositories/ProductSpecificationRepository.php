<?php
/**
 * Created by PhpStorm.
 * User: raymondweng
 * Date: 2016/12/16
 * Time: 上午2:39
 */

namespace App\Repositories;

use App\Models\ProductSpecification;
use Exception;

class ProductSpecificationRepository
{
    protected $specification;

    /**
     * UserRepository constructor.
     * @param specification $specification
     */
    public function __construct(ProductSpecification $specification)
    {
        $this->specification = $specification;
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return specification
     *
     * role->specification,product_m,logistics_m,finance
     */
    public function create(array $data)
    {

        $specification = $this->specification->create($data);

        //dd($specification);
        return $specification;
    }
    //['product_id', 'product_category_id', 'specification_category_id', 'specification_id', 'specification_item_id']
    public function update(array $data, $id)
    {
        //dd($data);
        $specification = $this->specification->find($id);
        //dd($specification);
        if ($specification) {
            if ($data['product_id'])$specification->product_id = $data['product_id'];
            if ($data['product_category_id'])$specification->product_category_id = $data['product_category_id'];
            if ($data['specification_category_id'])$specification->specification_category_id = $data['specification_category_id'];
            if ($data['specification_id'])$specification->specification_id = $data['specification_id'];
            if ($data['specification_item_id'])$specification->specification_item_id = $data['specification_item_id'];
            $specification->save();
        }
        //dd($specification);
        return $specification;
    }

    public function productSpecification($id)
    {
        $specification = $this->specification->find($id);
        return $specification;
    }
    
    public function productSpecificationByProduct($id)
    {
        $items = $this->specification
            ->where('product_id', $id)
            ->withh('specificationCategory')
            ->with('specification')
            ->with('specificationItem')
            ->get();

        return $items;
    }

    public function productSpecificationByProductAndSpecification($id, $spec_id)
    {
        $item = $this->specification
            ->where('product_id', $id)
            ->where('specification_id', $spec_id)
            ->with('specificationCategory')
            ->with('specification')
            ->with('specificationItem')
            ->first();

        return $item;
    }

    public function changeSpecificationItemToTempByProduct($id)
    {
        $items = $this->specification->where('product_id', $id)
            ->update(['specification_item_id' => 0]);
        //dd($items);
        return $items;
    }

    public function searchSpecifications($id, $pArray)
    {
        $query = $this->specification->select('product_id')->where('specification_category_id', $id);
        if (isset($pArray) && count($pArray) > 0) {
            $query->where(function ($query) use ($pArray) {
                foreach ($pArray as $pid) {
                    $query->orWhere('product_id', $pid);
                }
            });
        }
        //dd($query->with('product')->groupBy('product_id')->toSql());
        $items = $query->with('product')->groupBy('product_id')->paginate(20);
        //d($items);
        return $items;
    }

    public function searchSpecificationsCount($id, $specifications)
    {
        $query = $this->specification->select('product_id', \DB::raw('count(product_id) as p'))->where('specification_category_id', $id);
        if (isset($specifications) && count($specifications) > 0) {
            $query->where(function ($query) use ($specifications) {
                foreach ($specifications as $specification) {
                    if ($specification != 0) {
                        $query->orWhere('specification_item_id', $specification);
                    }
                }
            });
        }
        //dd($query->groupBy('product_id')->get());
        $items = $query->groupBy('product_id')->get();
        //d($items);
        return $items;
    }

    public function getAccessoriesBySpecificationCategory($id)
    {
        $specification = $this->specification->where('product_id', $id)->first();
        $items = $this->specification
            ->where('product_id', '!=', $id)
            ->where('specification_category_id', $specification->specification_category_id)
            ->with('product')
            ->groupBy('product_id')
            ->limit(10)
            ->get();


        return $items;
    }

    public function delete($id)
    {
        $specification = $this->specification->find($id);
        if ($specification->delete()) {
            return true;
        }
        return false;
    }

    public function deleteByTemp()
    {
        $items = $this->specification->where("specification_item_id", 0)->delete();
        if ($items) {
            return true;
        }
        return false;
    }
}