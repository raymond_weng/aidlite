<?php
/**
 * Created by PhpStorm.
 * User: raymondweng
 * Date: 2016/12/16
 * Time: 上午2:39
 */

namespace App\Repositories;

use App\Models\Image;
use App\Models\ProductCategory;
use App\Models\Product;
use App\Models\ProductSpecification;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class productRepository
{
    protected $product;
    protected $images;
    protected $feature;

    /**
     * UserRepository constructor.
     * @param product $product
     */
    public function __construct(Product $product, Image $images)
    {
        $this->product = $product;
        $this->images = $images;
        //$this->feature = $feature;
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return product
     *
     * role->product,product_m,logistics_m,finance
     */
    public function create(array $data)
    {

        return $this->createWithImages($data, array());
    }

    public function createWithImages(array $data, array $images)
    {

        $product = $this->product->create($data);

        if ($images != null && isset($images) && count($images) > 0){
            foreach ($images as $image) {
                $pi = new Image();
                $pi->product_id = $data['product_id'];
                $pi->image = $image;
                $pi->save();
            }
        }

        //dd($product);
        return $product;
    }

    public function update(array $data, $id)
    {

        return $this->updateWithImages($data, array(), $id);
    }

    public function updateWithImages(array $data, array $images, $id)
    {
        //dd($data);
        $product = $this->product->find($id);
        //dd($product);
        if ($product) {
            if ($data['name'])$product['name'] = $data['name'];
            if ($data['image'])$product['image'] = $data['image'];
            if ($data['model'])$product['model'] = $data['model'];
            if ($data['specification'])$product['specification'] = $data['specification'];
            if ($data['use'])$product['use'] = $data['use'];
            if ($data['accessories'])$product['accessories'] = $data['accessories'];
            if ($data['video_description'])$product['video_description'] = $data['video_description'];
            if ($data['description'])$product['description'] = $data['description'];
            if ($data['category_id'])$product['category_id'] = $data['category_id'];
            if ($data['seo_keyword'])$info['seo_keyword'] = $data['seo_keyword'];
            if ($data['seo_description'])$info['seo_description'] = $data['seo_description'];
            $product->save();
        }
        //dd($product);
        return $product;
    }

    public function product($id)
    {
        $product= $this->product->with('images')->find($id);
        //dd($product);
        return $product;
    }
    
    public function products()
    {
        $product = $this->product->with('images')->paginate(20);
        //dd($product);
        return $product;
    }

    public function productWithCategoryAndSpecifications($id)
    {
        $product= $this->product->with('images')->with('productSpecifications')->with('productCategory')->find($id);
        return $product;
    }

    public function productsWithCategory()
    {
        $product = $this->product->with('images')->with('productCategory')->paginate(20);
        //dd($product);
        return $product;
    }

    public function productsWithCategoryByCategory($id)
    {
        $product = $this->product->where('category_id', '=', $id)->with('images')->with('productCategory')->paginate(20);
        //dd($product);
        return $product;
    }

    public function productsWithCategoryAndSpecifications()
    {
        $product = $this->product->with('images')->with('productSpecifications')->with('productCategory')->paginate(20);
        //dd($product);
        return $product;
    }

    public function productsWithFeatured()
    {
        $product = $this->product->featured()
            ->with('images')
            ->with('productSpecifications')
            ->with('productCategory')
            ->orderBy('updated_at', 'desc')
            ->limit(10)
            ->get();
        //dd($product);
        return $product;
    }

    public function productsWithRecently()
    {
        $product = $this->product
            ->with('images')
            ->with('productSpecifications')
            ->with('productCategory')
            ->orderBy('updated_at', 'desc')
            ->limit(10)
            ->get();
        //dd($product);
        return $product;
    }

    public function productsWithSearch($keyword)
    {
        //dd($keyword);
        $products = $this->product
            ->with('images')
            ->with('productSpecifications')
            ->with('productCategory')
            ->where('name', 'like', '%'.$keyword.'%')
            ->orWhere('model', 'like', '%'.$keyword.'%')
            ->orWhere('description', 'like', '%'.$keyword.'%')
            ->paginate(20);
        //dd($products);
        return $products;
    }

    public function productsByCategoryID($id)
    {
        $product = $this->product->where('category_id', '=', $id)->with('images')->with('productCategory')->paginate(20);
        //dd($product);
        return $product;
    }

    public function delete($id)
    {
        $product = $this->product->find($id);
        if ($product->delete()) {
            $deletedRows = ProductSpecification::where('product_id', $id)->delete();
            return true;
        }
        return false;
    }


}