<?php

namespace App\Http\Controllers;

use App\Models\InfoCategory;
use App\Repositories\InfoRepository;
use App\Repositories\InfoCategoryRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;


class InfoCategoryController extends Controller
{
	protected $repository;
    protected $categoryRepository;

    public function __construct(InfoRepository $repository, InfoCategoryRepository $categoryRepository)
    {
        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $infoCategoryFirst = $this->categoryRepository->infoCategoryFirst();
        if(isset($infoCategoryFirst)) {
            $id = $infoCategoryFirst->id;

            $infoList = $this->repository->infoListByCategoryAndPublish($id);
            $infoCategory = $this->categoryRepository->infoCategory($id);
        } else {
            $infoList = new Collection();
            $infoCategory = new InfoCategory();
        }

        $infoCategoryList = $this->categoryRepository->infoCategoriesByPublish();

//dd($infoList);
        return view('info.index', compact('infoList', 'infoCategoryList', 'infoCategory', 'id'));
    }

    public function show($id)
    {
        $infoList = $this->repository->infoListByCategoryAndPublish($id);
        $infoCategoryList = $this->categoryRepository->infoCategoriesByPublish();
        $infoCategory = $this->categoryRepository->infoCategory($id);

        return view('info.index', compact('infoList', 'infoCategoryList', 'infoCategory', 'id'));
    }

}
