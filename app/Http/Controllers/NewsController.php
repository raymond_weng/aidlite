<?php

namespace App\Http\Controllers;

use App\Repositories\NewsRepository;
use App\Repositories\NewsCategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class NewsController extends Controller
{
	protected $repository;
    protected $categoryRepository;

    public function __construct(NewsRepository $repository, NewsCategoryRepository $categoryRepository)
    {
        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $news = $this->repository->news($id);
        $newsCategory = $this->categoryRepository->newsCategory($news->category_id);
        //dd($newsCategory);
        $page_title = $news->title;
        $page_keyword = null;
        if (isset($news->seo_keyword)) {
            $page_keyword = $news->seo_keyword;
        }
        $page_description = null;
        if (isset($news->excerpt)) {
            $page_description = $news->excerpt;
        }
        return view('news.detail', compact('page_title', 'page_keyword', 'page_description', 'news', 'newsCategory'));
    }

}
