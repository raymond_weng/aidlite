<?php

namespace App\Http\Controllers;

use App\Models\ProductCategory;
use App\Repositories\ImageRepository;
use Illuminate\Http\Request;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductSpecificationRepository;
use App\Repositories\SpecificationCategoryRepository;
use App\Repositories\SpecificationRepository;
use App\Repositories\ProductRepository;
use Illuminate\Support\Facades\Config;

class ProductController extends Controller
{
    protected $repository;
    protected $category;
    protected $specificationCategory;
    protected $productSpecificationRepository;
    protected $imageRepository;

    public function __construct(ProductRepository $repository, ProductCategoryRepository $category, SpecificationCategoryRepository $specificationCategory, ProductSpecificationRepository $productSpecificationRepository, ImageRepository $imageRepository)
    {
        $this->repository = $repository;
        $this->category = $category;
        $this->specificationCategory = $specificationCategory;
        $this->productSpecificationRepository = $productSpecificationRepository;
        $this->imageRepository = $imageRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $categories = $this->category->categoriesByRoot();
        
        $product = $this->repository->productWithCategoryAndSpecifications($id);
        $currentCategory = $product->productCategory;
        $specificationCategories = $this->specificationCategory->categories();
        $accessories = $this->productSpecificationRepository->getAccessoriesBySpecificationCategory($id);

        $page_title = $product->name;
        $page_keyword = null;
        if (isset($product->seo_keyword)) {
            $page_keyword = $product->seo_keyword;
        }
        $page_description = null;
        if (isset($product->seo_description)) {
            $page_description = $product->seo_description;
        }
        //dd($products);
        return view('products.detail', compact('page_title', 'page_keyword', 'page_description', 'categories', 'currentCategory', 'product', 'specificationCategories', 'accessories'));
    }

    public function detail()
    {
    	$ProductFunction_Array[0]['Categories'] = "Headlight Bulbs";
    	$ProductFunction_Array[1]['Categories'] = "Fog Light Bulbs (DRL Bulbs)";
    	$ProductFunction_Array[2]['Categories'] = "";
    	$ProductFunction_Array[3]['Categories'] = "";

    	$ProductFunction_Array[0]['Volt'] = "6VDC";
    	$ProductFunction_Array[1]['Volt'] = "12VDC";
    	$ProductFunction_Array[2]['Volt'] = "";
    	$ProductFunction_Array[3]['Volt'] = "";

    	$ProductFunction_Array[0]['Color'] = "Red";
    	$ProductFunction_Array[1]['Color'] = "Amber";
    	$ProductFunction_Array[2]['Color'] = "Warm White";
    	$ProductFunction_Array[3]['Color'] = "Other";

    	$ProductFunction_Array[0]['BulbType'] = "HB1 (9004)";
    	$ProductFunction_Array[1]['BulbType'] = "7440";
    	$ProductFunction_Array[2]['BulbType'] = "";
    	$ProductFunction_Array[3]['BulbType'] = "";

    	$ProductFunction_Array[0]['BulbBase'] = "T25 Wedge";
    	$ProductFunction_Array[1]['BulbBase'] = "Neo-Wedge";
    	$ProductFunction_Array[2]['BulbBase'] = "";
    	$ProductFunction_Array[3]['BulbBase'] = "";

    	$ProductFunction_Array[0]['Function'] = "Switchback";
    	$ProductFunction_Array[1]['Function'] = "Strobe";
    	$ProductFunction_Array[2]['Function'] = "";
    	$ProductFunction_Array[3]['Function'] = "";

    	$ProductFunction_Array[0]['IP'] = "20";
    	$ProductFunction_Array[1]['IP'] = "65";
    	$ProductFunction_Array[2]['IP'] = "";
    	$ProductFunction_Array[3]['IP'] = "";

        //
        return view('products.detail', compact('ProductFunction_Array'));
    }


}
