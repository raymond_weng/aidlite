<?php

namespace App\Http\Controllers;

use App\Repositories\BannerRepository;
use App\Repositories\InfoRepository;
use App\Repositories\SubBannerRepository;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $bannerRepository;
    protected $subBannerRepository;
    protected $infoRepository;
    protected $productRepository;

    public function __construct(BannerRepository $bannerRepository, SubBannerRepository $subBannerRepository, ProductRepository $productRepository, InfoRepository $infoRepository)
    {
        $this->bannerRepository = $bannerRepository;
        $this->subBannerRepository = $subBannerRepository;
        $this->infoRepository = $infoRepository;
        $this->productRepository = $productRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = $this->bannerRepository->bannersByPublishAndCategoryCode('home');
        $subBanners = $this->subBannerRepository->banners();
        $infoList = $this->infoRepository->homeInfoListByPublish();
        $products = $this->productRepository->productsWithFeatured();
        $recentProducts = $this->productRepository->productsWithRecently();
        //dd($subBanners);
        return view('home.home', compact('banners', 'subBanners', 'infoList', 'products', 'recentProducts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
