<?php

namespace App\Http\Controllers;


use App\Models\ProductCategory;
use App\Repositories\ImageRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductSpecificationRepository;
use App\Repositories\SpecificationCategoryRepository;
use App\Repositories\SpecificationRepository;
use App\Repositories\ProductRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    protected $repository;
    protected $category;
    protected $specificationCategory;
    protected $productSpecificationRepository;
    protected $imageRepository;

    public function __construct(ProductRepository $repository, ProductCategoryRepository $category, SpecificationCategoryRepository $specificationCategory, ProductSpecificationRepository $productSpecificationRepository, ImageRepository $imageRepository)
    {
        $this->repository = $repository;
        $this->category = $category;
        $this->specificationCategory = $specificationCategory;
        $this->productSpecificationRepository = $productSpecificationRepository;
        $this->imageRepository = $imageRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->category->categoriesByRoot();
        //dd($categories);
        if (isset($categories) && is_countable($categories) && count($categories) > 0) {
            $currentCategory = $categories[0];
            if (isset($currentCategory->subCategories) && is_countable($currentCategory->subCategories) && count($currentCategory->subCategories) > 0) {
                $products = $this->repository->productsByCategoryID($currentCategory->subCategories[0]->id);
            } else {
                $products = new Collection();
            }
        } else {
            $currentCategory = new ProductCategory();
            $products = new Collection();
        }

        $specificationCategories = $this->specificationCategory->categories();

        //dd($specificationCategories);
        return view('products.index', compact('categories', 'currentCategory', 'products', 'specificationCategories'));
    }

    public function show($id)
    {
        $categories = $this->category->categoriesByRoot();

        $currentCategory = $this->category->category($id);
        $products = $this->repository->productsByCategoryID($id);

        $specificationCategories = $this->specificationCategory->categories();
        //dd($products);
        return view('products.index', compact('categories', 'currentCategory', 'products', 'specificationCategories'));
    }

    public function search(Request $request)
    {
        //dd($request->all());

        $products = $this->repository->productsWithSearch($request->input("productSearchName"));

        $currentCategory = new ProductCategory();
        $currentCategory->id = 0;
        $currentCategory->name = "Search";
        $categories = $this->category->categoriesByRoot();
        $specificationCategories = $this->specificationCategory->categories();

        //dd($products);
        return view('products.index', compact('categories', 'currentCategory', 'products', 'specificationCategories'));
    }

    public function searchSpecifications(Request $request)
    {
        //dd($request->all());
        $category = $request->input('specification_category');
        $specifications = $request->input('specifications');
        $specs = array();
        if (isset($specifications)) {
            foreach ($specifications as $key => $specification) {
                if ($specification != 0) {
                    $specs[$key] = $specification;
                }
            }
        }
        //dd(count($specs));
        $productsCount = $this->productSpecificationRepository->searchSpecificationsCount($category, $specifications);
        $pArray = array();
        //dd($productsCount);
        if (isset($productsCount)) {
            foreach ($productsCount as $productCount) {
                if (is_countable($specs) && count($specs) > 0) {
                    if (is_countable($specs) && $productCount->p == count($specs)) {
                        $pArray[] = $productCount->product_id;
                    }
                } else {
                    $pArray[] = $productCount->product_id;
                }
            }
        }
        //dd($pArray);
        if (isset($pArray) && is_countable($pArray) && count($pArray) > 0) {
            $products = $this->productSpecificationRepository->searchSpecifications($category, $pArray);
        } else {
            $products = new Collection();
        }

        $currentCategory = new ProductCategory();
        $currentCategory->id = 0;
        $currentCategory->name = "Search";
        $categories = $this->category->categoriesByRoot();
        $specificationCategories = $this->specificationCategory->categories();

        //dd($specificationCategories);
        return view('products.index', compact('categories', 'currentCategory', 'products', 'specificationCategories'));
    }
}
