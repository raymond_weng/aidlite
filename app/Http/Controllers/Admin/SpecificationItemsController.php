<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\SpecificationItemRepository;
use App\Repositories\SpecificationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class SpecificationItemsController extends Controller
{
    protected $repository;
    protected $specification;

    public function __construct(SpecificationItemRepository $repository, SpecificationRepository $specification)
    {
        $this->repository = $repository;
        $this->specification = $specification;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$category_id = $request->get('category_id');
    	$specification_id = $request->get('id');
        $items = $this->repository->itemsWithSpecificationAndPaging($specification_id);

        //dd($items);
        return view('admin.product.specification.item.index', compact('items', 'category_id', 'specification_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $specifications = $this->specification->specifications();
        $specification_id = $request->get('id');
        $category_id = $request->get('category_id');
        return view('admin.product.specification.item.create_edit', compact('specifications', 'specification_id', 'category_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'specification_id' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //dd($request->all());
        $data = array();
        $data['name'] = $request->get('name');
        $data['sort'] = $request->get('sort');
        $data['specification_id'] = $request->get('specification_id');

        //dd($data);
        $this->repository->create($data);

        return redirect()->route('admin.product.specification.items.index', ['id' => $data['specification_id'], 'category_id' => $request->get('category_id')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->repository->specification($id);

        //dd($specification);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $specifications = $this->specification->specifications();
        $item = $this->repository->item($id);
        $specification_id = $request->get('id');
        $category_id = $request->get('category_id');
        return view('admin.product.specification.item.create_edit', compact('specifications', 'item', 'specification_id', 'category_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //dd($request->all());
        $item = $this->repository->item($id);
        $item->name = $request->get('name');
        $item->sort = $request->get('sort');
        $item->specification_id = $request->get('specification_id');
//dd($item);
        $item->save();

        return redirect()->route('admin.product.specification.items.index', ['id' => $item->specification_id, 'category_id' => $request->get('category_id')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->repository->delete($id);

        return redirect()->route('admin.product.specification.items.index', ['id' => $request->get('specification_id'), 'category_id' => $request->get('category_id')]);
    }
}
