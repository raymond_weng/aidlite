<?php

namespace App\Http\Controllers\Admin;


use App\Repositories\SpecificationCategoryRepository;
use App\Repositories\SpecificationRepository;
use App\Repositories\SpecificationItemRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class SpecificationsController extends Controller
{
    protected $repository;
    protected $category;
    protected $specificationItem;

    public function __construct(SpecificationRepository $repository, SpecificationCategoryRepository $category, SpecificationItemRepository $specificationItem)
    {
        $this->repository = $repository;
        $this->category = $category;
        $this->specificationItem = $specificationItem;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        //$items = $this->repository->specificationsWithCategoryAndPaging();
        $categories = $this->category->categoriesWithPaging();

        //dd($items);
        return view('admin.product.specification.list', compact('categories'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category_id = $request->get('id');
        $items = $this->repository->specificationsWithCategoryAndPaging($category_id);
        //dd($items);
        return view('admin.product.specification.index', compact('items', 'category_id'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categories = $this->category->categories();
        $category_id = $request->get('category_id');
        return view('admin.product.specification.create_edit', compact('categories', 'category_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //dd($request->all());
        $data = array();
        $data['name'] = $request->get('name');
        $data['sort'] = $request->get('sort');
        $data['category_id'] = $request->get('category_id');

        //dd($data);
        $this->repository->create($data);

        return redirect()->route('admin.product.specifications.index', ['id' => $data['category_id']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->repository->specification($id);

        //dd($category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $categories = $this->category->categories();
        $item = $this->repository->specification($id);
        $category_id = $request->get('category_id');
        return view('admin.product.specification.create_edit', compact('categories', 'item', 'category_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'category_id' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //dd($request->all());
        $item = $this->repository->specification($id);
        $item->name = $request->get('name');
        $item->sort = $request->get('sort');
        $item->category_id = $request->get('category_id');

        $item->save();

        return redirect()->route('admin.product.specifications.index', ['id' => $request->get('category_id')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //dd($request->get('category_id'));
        $error = 0;
        $count = $this->specificationItem->specificationItemCountBySpecification($id);
        //dd($count);
        if ($count <= 0) {
            $this->repository->delete($id);
        } else {
            $error = 1;
        }

        return redirect()->route('admin.product.specifications.index', ['id' => $request->get('category_id')])->with('error', $error);
    }

    public function specificationsByCategoryID(Request $request, $id)
    {
        if (!isset($id)) {
            return response()->json([
                'message' => '無此分類',
                'status' => 'error'
            ]);
        }

        $specifications = $this->repository->specificationsWithItemsByCategory($id);

        return response()->json([
            'status' => 'success',
            'data' => $specifications
        ]);
    }

}
