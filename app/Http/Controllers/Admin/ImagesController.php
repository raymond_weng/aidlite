<?php

namespace App\Http\Controllers\Admin;


use App\Repositories\ImageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ImagesController extends Controller
{
    protected $repository;

    public function __construct(ImageRepository $repository)
    {
        $this->middleware('auth');
        $this->repository = $repository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'file' => 'required',
            'file.*' => 'image|mimes:jpeg,png,jpg|max:2048',
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();

            return response()->json([
                'message' => $messages,
                'state' => 'error'
            ]);
        }

        $data = array();

        $type = $request->get('type');
        $data['relation_id'] = -1;
        $data['sort'] = 1;
        $data['type'] = $type;
        $data['is_temp'] = false;

        $width = $request->get('width');
        if (!isset($width)){$width = 100;}
        $height = $request->get('height');
        if (!isset($height)){$height = 100;}

        $files = $request->file('file');
        foreach($files as $file) {
            if ($file) {
                $name = time();
                $imageName = $name . '.' . $file->getClientOriginalExtension();
                $exists = Storage::exists($type);
                if (!$exists) {
                    Storage::makeDirectory($type);
                }

                $img = Image::make($file);

                if (isset($width)){
                    $img->widen($width);
                } else if (isset($height)){
                    $img->heighten($height);
                }

                $img->save('storage/'.$type.'/'. $imageName);
                $data['image'] = $type.'/'.$imageName;
            }
        }

        $image = $this->repository->create($data);

        return response()->json([
            'state' => 'success',
            'data' => [
                'id' => strval($image->id),
                'relation_id' => strval($image->relation_id),
                'image' => Storage::url($image->image),
                'type' => $image->type,
            ]
        ]);

    }

    public function changeTempByRelationAndType($id, $type, $temp) {
        $images = $this->repository->changeTempByRelationAndType($id, $type, $temp);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = $this->repository->delete($id);

        return $status;
    }
}
