<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\ImageRepository;
use App\Repositories\SubBannerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class SubBannersController extends Controller
{
    protected $repository;
    protected $imageRepository;

    public function __construct(SubBannerRepository $repository, ImageRepository $imageRepository)
    {
        $this->middleware('auth');
        $this->repository = $repository;
        $this->imageRepository = $imageRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = $this->repository->bannersWithPaging();

        //dd($banners);
        return view('admin.subbanner.index', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.subbanner.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'images' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => $messages,
                ]
            ]);
        }

        //dd($request->all());
        $data = array();
        $data['name'] = $request->get('name');
        
        $url = $request->get('url');
        if (isset($url)) {
            $data['url'] = $url;
        } else {
            $data['url'] = null;
        }
        
        $data['status'] = $request->get('my-checkbox') === 'on'?1:0;
        $data['sort'] = $request->get('sort');

        //dd($data);
        $banner = $this->repository->create($data);

        $images = json_decode($request->get('images'), true);

        $this->imageRepository->changeTempByRelationAndType($banner->id, "subbanner", false);

        foreach($images as $key => $data) {
            $image = $this->imageRepository->image($data);
            if (isset($image)) {
                $image->relation_id = $banner->id;
                $image->sort = $key;
                $image->is_temp = true;
                $image->save();
            }
        }
        $this->imageRepository->deleteByTemp();

        return response()->json([
            'status' => 'success',
        ]);
        //return redirect()->route('admin.sub.banner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $banner = $this->repository->banner($id);

        //dd($news);
        return view('admin.subbanner.show', compact('banner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = $this->repository->banner($id);

        //dd($news);
        return view('admin.subbanner.create_edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

//        return response()->json([
//            'status' => 'success',
//            'data' => $request->all(),
//        ]);
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'images' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => $messages,
                ]
            ]);
        }

        //dd($request->all());
        $banner = $this->repository->banner($id);
        $banner->name = $request->get('name');
        

        $url = $request->get('url');
        if (isset($url)) {
            $banner->url = $url;
        } else {
            $banner->url = null;
        }
        
        $banner->status = $request->get('my-checkbox') === 'on'?1:0;
        $banner->sort = $request->get('sort');
        //dd($data);
        $banner->save();

        $images = json_decode($request->get('images'), true);

        $this->imageRepository->changeTempByRelationAndType($id, "subbanner", false);

        foreach($images as $key => $data) {
            $image = $this->imageRepository->image($data);
            if (isset($image)) {
                $image->relation_id = $banner->id;
                $image->sort = $key;
                $image->is_temp = true;
                $image->save();
            }
        }
        $this->imageRepository->deleteByTemp();

        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        $banner = $this->repository->banner($id);
        if ($banner) {
            $this->imageRepository->deleteByRelationAndType($banner->id, "subbanner");
            $this->repository->delete($id);
        }

        return redirect()->route('admin.sub.banners.index');
    }
}
