<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\InfoCategoryRepository;
use App\Repositories\InfoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Expr\Array_;

class infoController extends Controller
{
    protected $repository;
    protected $category;

    public function __construct(InfoRepository $repository, InfoCategoryRepository $category)
    {
        $this->middleware('auth');
        $this->repository = $repository;
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $infos = $this->repository->infoListWithCategory();

        //dd($infoList);
        return view('admin.info.index', compact('infos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->category->infoCategories();

        return view('admin.info.create_edit', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'content' => 'required',
            'category_id' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //dd($request->all());
        $data = array();
        $data['title'] = $request->get('title');
        $excerpt = $request->get('excerpt');
        if (isset($excerpt)) {
            $data['excerpt'] = $excerpt;
        }
        $seo_keyword = $request->get('seo_keyword');
        if (isset($seo_keyword)) {
            $data['seo_keyword'] = $seo_keyword;
        }
        $data['content'] = htmlentities($request->get('content'), ENT_COMPAT , "UTF-8");
        $data['category_id'] = $request->get('category_id');
        $data['status'] = $request->get('my-checkbox') === 'on'?1:0;
        $data['sort'] = $request->get('sort');
        $data['post_date'] = $request->get('post_date');

        $this->repository->create($data);

        return redirect()->route('admin.info.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info = $this->repository->infoWithCategory($id);

        //dd($info);
        return view('admin.info.show', compact('info'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info = $this->repository->infoWithCategory($id);
        $categories = $this->category->infoCategories();
        //dd($info);
        return view('admin.info.create_edit', compact('info', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($id);
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'content' => 'required',
            'category_id' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //dd($request->all());
        $info = $this->repository->info($id);
        //dd($info);
        $info->title = $request->get('title');
        $excerpt = $request->get('excerpt');
        if (isset($excerpt)) {
            $info->excerpt = $excerpt;
        }
        $seo_keyword = $request->get('seo_keyword');
        if (isset($seo_keyword)) {
            $info['seo_keyword'] = $seo_keyword;
        }
        $info->content = htmlentities($request->get('content'), ENT_COMPAT , "UTF-8");
        $info->category_id = $request->get('category_id');
        $info->status = $request->get('my-checkbox') === 'on'?1:0;
        $info->sort = $request->get('sort');
        $info->post_date = $request->get('post_date');

        $info->save();

        return redirect()->route('admin.info.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd('delete:'.$id);
        $this->repository->delete($id);

        return redirect()->route('admin.info.index');
    }
}
