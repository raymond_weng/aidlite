<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\NewsCategoryRepository;
use App\Repositories\NewsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Expr\Array_;

class NewsController extends Controller
{
    protected $repository;
    protected $category;

    public function __construct(NewsRepository $repository, NewsCategoryRepository $category)
    {
        $this->middleware('auth');
        $this->repository = $repository;
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newsList = $this->repository->newsListWithCategory();

        //dd($newsList);
        return view('admin.news.index', compact('newsList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->category->newsCategories();

        return view('admin.news.create_edit', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'content' => 'required',
            'category_id' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //dd($request->all());
        $data = array();
        $data['title'] = $request->get('title');
        $excerpt = $request->get('excerpt');
        if (isset($excerpt)) {
            $data['excerpt'] = $excerpt;
        }
        $seo_keyword = $request->get('seo_keyword');
        if (isset($seo_keyword)) {
            $data['seo_keyword'] = $seo_keyword;
        }
        $data['content'] = htmlentities($request->get('content'), ENT_COMPAT , "UTF-8");
        $data['category_id'] = $request->get('category_id');
        $data['status'] = $request->get('my-checkbox') === 'on'?1:0;
        $data['sort'] = $request->get('sort');
        $data['post_date'] = $request->get('post_date');

        $this->repository->create($data);

        return redirect()->route('admin.news.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = $this->repository->newsWithCategory($id);

        //dd($news);
        return view('admin.news.show', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = $this->repository->newsWithCategory($id);
        $categories = $this->category->newsCategories();
        //dd($news);
        return view('admin.news.create_edit', compact('news', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($id);
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'content' => 'required',
            'category_id' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //dd($request->all());
        $news = $this->repository->news($id);
        //dd($news);
        $news->title = $request->get('title');
        $excerpt = $request->get('excerpt');
        if (isset($excerpt)) {
            $news->excerpt = $excerpt;
        }
        $seo_keyword = $request->get('seo_keyword');
        if (isset($seo_keyword)) {
            $news['seo_keyword'] = $seo_keyword;
        }
        $news->content = htmlentities($request->get('content'), ENT_COMPAT , "UTF-8");
        $news->category_id = $request->get('category_id');
        $news->status = $request->get('my-checkbox') === 'on'?1:0;
        $news->sort = $request->get('sort');
        $news->post_date = $request->get('post_date');

        $news->save();

        return redirect()->route('admin.news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd('delete:'.$id);
        $this->repository->delete($id);

        return redirect()->route('admin.news.index');
    }
}
