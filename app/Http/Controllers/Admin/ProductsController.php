<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\ImageRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductSpecificationRepository;
use App\Repositories\SpecificationCategoryRepository;
use App\Repositories\SpecificationRepository;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use PhpParser\Node\Expr\Array_;

class ProductsController extends Controller
{
    protected $repository;
    protected $category;
    protected $specificationCategory;
    protected $productSpecificationRepository;
    protected $imageRepository;

    public function __construct(ProductRepository $repository, ProductCategoryRepository $category, SpecificationCategoryRepository $specificationCategory, ProductSpecificationRepository $productSpecificationRepository, ImageRepository $imageRepository)
    {
        $this->middleware('auth');
        $this->repository = $repository;
        $this->category = $category;
        $this->specificationCategory = $specificationCategory;
        $this->productSpecificationRepository = $productSpecificationRepository;
        $this->imageRepository = $imageRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category_id = $request->get('category_id');
        if ($category_id) {
            $products = $this->repository->productsWithCategoryByCategory($category_id);
        } else {
            $products = $this->repository->productsWithCategory();
        }
        $subCategories = $this->category->categories();

        //dd('test');
        return view('admin.product.index', compact('products', 'subCategories', 'category_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->category->categories();
        $specificationCategories = $this->specificationCategory->categories();
        $isCopy = false;
        return view('admin.product.create_edit', compact('categories', 'specificationCategories', 'isCopy'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'model' => 'required',
            'category_id' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);

            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => $messages,
                ]
            ]);
        }

        $images = json_decode($request->get('images'), true);
        if (!isset($images) || (is_countable($images) && count($images) <= 0)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => "至少要有一張照片!",
                ]
            ]);
        }

        //dd($request->all());
        $data = array();
        $data['name'] = $request->get('name');
        $data['model'] = $request->get('model');
        $data['category_id'] = $request->get('category_id');
        $data['description'] = $request->get('description');
        $data['featured'] = $request->get('my-checkbox') === 'on'?1:0;

        $seo_keyword = $request->get('seo_keyword');
        if (isset($seo_keyword)) {
            $data['seo_keyword'] = $seo_keyword;
        }
        $seo_description = $request->get('seo_description');
        if (isset($seo_description)) {
            $data['seo_description'] = $seo_description;
        }

        $specification = $request->get('specifications');
        if (isset($specification)) {
            $data['specifications'] = htmlentities($specification, ENT_COMPAT , "UTF-8");
        }
        $video = $request->get('video');
        if (isset($video)) {
            $data['video'] = htmlentities($video, ENT_COMPAT , "UTF-8");
        }
        $photo = $request->get('photo');
        if (isset($photo)) {
            $data['photo'] = htmlentities($photo, ENT_COMPAT , "UTF-8");
        }
        $additional_information = $request->get('additional_information');
        if (isset($additional_information)) {
            $data['additional_information'] = htmlentities($additional_information, ENT_COMPAT , "UTF-8");
        }

        $product = $this->repository->create($data);

        //$images = json_decode($request->get('images'), true);

        $this->imageRepository->changeTempByRelationAndType($product->id, "product", false);

        foreach($images as $key => $data) {
            $image = $this->imageRepository->image($data);
            if (isset($image)) {
                $image->relation_id = $product->id;
                $image->sort = $key;
                $image->is_temp = true;
                $image->save();
            }
        }
        $this->imageRepository->deleteByTemp();

        $productSpecifications = json_decode($request->get('product_specifications'), true);

        $this->productSpecificationRepository->changeSpecificationItemToTempByProduct($product->id);

        foreach($productSpecifications as $key => $value) {
            $item = $this->productSpecificationRepository->productSpecificationByProductAndSpecification($product->id, $value['specification_id']);
            if (isset($item)) {
                $item->specification_item_id = $value['specification_item_id'];
                $item->save();
            } else {
                $value['product_id'] = $product->id;
                $item = $this->productSpecificationRepository->create($value);
            }
        }
        $this->productSpecificationRepository->deleteByTemp();

        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->repository->productWithCategoryAndSpecifications($id);

        //dd($product);
        return view('admin.product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = $this->repository->productWithCategoryAndSpecifications($id);
        $categories = $this->category->categories();
        $specificationCategories = $this->specificationCategory->categories();
        $isCopy = false;
        return view('admin.product.create_edit', compact('product','categories', 'specificationCategories', 'isCopy'));
    }

    public function copy($id)
    {
        $product = $this->repository->productWithCategoryAndSpecifications($id);
        $categories = $this->category->categories();
        $specificationCategories = $this->specificationCategory->categories();
        $isCopy = true;
        return view('admin.product.create_edit', compact('product','categories', 'specificationCategories', 'isCopy'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->input());
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'model' => 'required',
            'category_id' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => $messages,
                ]
            ]);
        }

        $images = json_decode($request->get('images'), true);
        if (!isset($images) || (is_countable($images) && count($images) <= 0)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => "至少要有一張照片!",
                ]
            ]);
        }

        //dd($request->input());
        $product = $this->repository->product($id);
        $product->name = $request->get('name');
        $product->model = $request->get('model');
        $product->category_id = $request->get('category_id');
        $product->description = $request->get('description');
        $product->featured = $request->get('my-checkbox') === 'on'?1:0;

        $seo_keyword = $request->get('seo_keyword');
        if (isset($seo_keyword)) {
            $product['seo_keyword'] = $seo_keyword;
        }
        $seo_description = $request->get('seo_description');
        if (isset($seo_description)) {
            $product['seo_description'] = $seo_description;
        }

        $specifications = $request->get('specifications');
        if (isset($specifications)) {
            $product->specifications = htmlentities($specifications, ENT_COMPAT , "UTF-8");
        } else {
            $product->specifications = "";
        }
        $video = $request->get('video');
        if (isset($video)) {
            $product->video = htmlentities($video, ENT_COMPAT , "UTF-8");
        } else {
            $product->video = "";
        }
        $photo = $request->get('photo');
        if (isset($photo)) {
            $product->photo = htmlentities($photo, ENT_COMPAT , "UTF-8");
        } else {
            $product->photo = "";
        }
        $additional_information = $request->get('additional_information');
        if (isset($additional_information)) {
            $product->additional_information = htmlentities($additional_information, ENT_COMPAT , "UTF-8");
        } else {
            $product->additional_information = "";
        }

        $product->save();

        //$images = json_decode($request->get('images'), true);

        $this->imageRepository->changeTempByRelationAndType($product->id, "product", false);

        foreach($images as $key => $data) {
            $image = $this->imageRepository->image($data);
            if (isset($image)) {
                $image->relation_id = $product->id;
                $image->sort = $key;
                $image->is_temp = true;
                $image->save();
            }
        }
        $this->imageRepository->deleteByTemp();

        $productSpecifications = json_decode($request->get('product_specifications'), true);

        $this->productSpecificationRepository->changeSpecificationItemToTempByProduct($product->id);

        foreach($productSpecifications as $key => $data) {
            $item = $this->productSpecificationRepository->productSpecificationByProductAndSpecification($product->id, $data['specification_id']);
            if (isset($item)) {
                $item->specification_item_id = $data['specification_item_id'];
                $item->save();
            } else {
                $data['product_id'] = $product->id;
                $item = $this->productSpecificationRepository->create($data);
            }
        }
        $this->productSpecificationRepository->deleteByTemp();

        //dd($product);
        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd('delete:'.$id);
        $this->repository->delete($id);

        return redirect()->route('admin.products.index');
    }
}
