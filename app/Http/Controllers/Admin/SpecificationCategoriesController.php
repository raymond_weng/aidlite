<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\SpecificationCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class SpecificationCategoriesController extends Controller
{
    protected $repository;

    public function __construct(SpecificationCategoryRepository $repository)
    {
        $this->middleware('auth');
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories = $this->repository->categoriesWithPaging();
        //dd($categories);
        return view('admin.product.specification.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.specification.category.create_edit');//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(),[
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //dd($request->all());
        $data = array();
        $data['name'] = $request->get('name');
        $parent_id = $request->get('category_id');
        if(isset($parent_id)) {
            $data['parent_id'] = $parent_id;
        }
        $featured = $request->get('checkbox');
        if(isset($featured)) {
            $data['featured'] = $featured === 'on'?1:0;
        } else {
            $data['featured'] = 0;
        }

        $data['sort'] = $request->get('sort');

        //dd($data);
        $this->repository->create($data);

        return redirect()->route('admin.product.specification.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = $this->repository->category($id);

        //dd($category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->repository->category($id);
        $categories = $this->repository->categories();
        //dd($category);
        return view('admin.product.specification.category.create_edit', compact('category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //dd($request->all());
        $category = $this->repository->category($id);
        $category->name = $request->get('name');
        $category->sort = $request->get('sort');

        //dd($data);
        $category->save();

        return redirect()->route('admin.product.specification.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);

        return redirect()->route('admin.product.specification.categories.index');
    }
}
