<?php

namespace App\Http\Controllers\Admin;


use App\Repositories\ProductCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ProductCategoriesController extends Controller
{
    protected $repository;

    public function __construct(ProductCategoryRepository $repository)
    {
        $this->middleware('auth');
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(request $request)
    {
    	$category_id = $request->get('category_id');
    	$subCategories = $this->repository->categories();
    	//dd($category_id);
    	if ($category_id) {
    	    $categories = $this->repository->categoriesWithPagingByCategory($category_id);
    	} else {
            $categories = $this->repository->categoriesWithPaging();
        }
        //dd($categories);
        return view('admin.product.category.index', compact('categories', 'category_id', 'subCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$category = null;
        $categories = $this->repository->categories();
        //dd($categories);
        return view('admin.product.category.create_edit', compact('categories', 'category'));//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(),[
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //dd($request->all());
        $data = array();
        $data['name'] = $request->get('name');
        $parent_id = $request->get('category_id');
        if($parent_id) {
            $data['parent_id'] = $parent_id;
            //Layer
            $c = $this->repository->categoryWithProducts($parent_id);
            $parent = $this->repository->categoriesByParent($parent_id);
            if (is_countable($parent) && count($parent) > 0) {
                //變更層級
                $data['layer'] = $parent[0]->parent->layer + 1;
                //變更順序
                $data['sequence'] = $parent[0]->sequence + 1;
            } else {
                //變更層級
                $data['layer'] = $c->layer + 1;
                //變更順序
                $data['sequence'] = $c->sequence + 1;
            }
            //dd($category);
            //變更其他資料順序
            $this->repository->categoryUpdateForNewSequence($data['sequence']);
        } else {
        	$c = $this->repository->categoryLastSequence();
            //變更層級
            $data['layer'] = 0;
            //變更順序
            if (isset($c)) {
                $data['sequence'] = $c->sequence + 1;
            } else {
                $data['sequence'] = 0;
            }
            
        }

        $featured = $request->get('checkbox');
        if(isset($featured)) {
            $data['featured'] = $featured === 'on'?1:0;
        } else {
            $data['featured'] = 0;
        }

        $data['sort'] = $request->get('sort') ? $request->get('sort') : null;

        //dd($data);
        $this->repository->create($data);

        return redirect()->route('admin.product.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = $this->repository->category($id);

        //dd($category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->repository->category($id);
        $categories = $this->repository->categories();
        //dd($category);
        return view('admin.product.category.create_edit', compact('category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //dd($request->all());
        $category = $this->repository->category($id);
        $category->name = $request->get('name');
        $parent_id = $request->get('category_id');
        $old_sequence = $category->sequence;

        if (isset($parent_id) && $category->layer == 0) {
            $parent = $this->repository->categoriesByParent($id);
            if (is_countable($parent) && count($parent) > 0) {
                return Redirect::back()
                ->with('msg', '此分類有子分類，不可移動。');
            }
        }

        if(isset($parent_id)) {
            $category->parent_id = $parent_id;
            //Layer
            //$parent = $this->repository->categoryWithProducts($parent_id);
            $c = $this->repository->categoryWithProducts($parent_id);
            $parent = $this->repository->categoriesByParent($parent_id, $category->id);
            //dd($c);
            if (is_countable($parent) && count($parent) > 0) {
                //變更層級
                $category->layer = $parent[0]->parent->layer + 1;
                //變更順序
                if ($parent[0]->sequence > $category->sequence) 
                    $category->sequence = $parent[0]->sequence;
                else
                    $category->sequence = $parent[0]->sequence + 1;
            } else {
                //變更層級
                $category->layer = $c->layer + 1;
                //變更順序
                if ($c->sequence > $category->sequence)
                    $category->sequence = $c->sequence;
                else
                    $category->sequence = $c->sequence + 1;
            }
            //dd($category);
            //變更其他資料順序
            $this->repository->categoryUpdateForSequence($category->sequence, $old_sequence, $category->id);
        } else if ($category->layer == 0 && !isset($parent_id)) {
            //
        } else {
        	$c = $this->repository->categoryLastSequence();
            //變更層級
            $category->layer = 0;
            //變更順序
            $category->sequence = $c->sequence + 1;
            //清除父類
            $category->parent_id = null;
        }
        //dd($category);

        $featured = $request->get('checkbox');
        if(isset($featured)) {
            $category->featured = $featured === 'on'?1:0;
        } else {
            $category->featured = 0;
        }

        $category->sort = $request->get('sort') ? $request->get('sort') : null;

        //dd($category);
        $category->save();

        return redirect()->route('admin.product.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$error = 0;
        $category = $this->repository->category($id);
        $parent = $this->repository->category($category->parent_id);
        $categoryWithProducts = $this->repository->categoryWithProducts($id);
        $categoryByParent = $this->repository->categoryByParent($id);
        //dd(count($categoryWithProducts->products));
        if (is_countable($categoryWithProducts->products) && count($categoryWithProducts->products) > 0) {
            $error = 1;
        } else if (is_countable($categoryByParent) && count($categoryByParent) > 0) {
            $error = 2;
        }
        //dd($error);
        if ($error == 0) {
            $this->repository->categoryUpdateForDeleteSequence($category->sequence);
            $this->repository->delete($id);
        }

        return redirect()->route('admin.product.categories.index')->with('error', $error);
    }
}
