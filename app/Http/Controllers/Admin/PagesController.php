<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\PageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class PagesController extends Controller
{
    protected $repository;

    public function __construct(PageRepository $repository)
    {
        $this->middleware('auth');
        $this->repository = $repository;
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $pages = $this->repository->pages();
//        //dd($pages);
//        return view('admin.page.index', compact('pages'));

        $page = $this->repository->pageByType('about');
        //dd($page);
        if (isset($page)) {
            return view('admin.page.create_edit', compact('page'));
        } else {
            return view('admin.page.create_edit');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.page.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(),[
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //dd($request->all());
        $data = array();
        $data['title'] = "關於我們";
        $data['content'] = htmlentities($request->get('content'), ENT_COMPAT , "UTF-8");
        $data['type'] = 'about';
        $data['status'] = true;

        $this->repository->create($data);

        return redirect()->route('admin.page.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = $this->repository->page($id);

        //dd($page);
        return view('admin.page.show', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = $this->repository->page($id);
        //dd($page);
        return view('admin.page.create_edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($id);
        $validator = Validator::make($request->all(),[
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
            //dd($messages);
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //dd($request->all());
        $page = $this->repository->page($id);
        //dd($page);
        $page->title = $request->get('title');
        $excerpt = $request->get('excerpt');
        if (isset($excerpt)) {
            $page->excerpt = $excerpt;
        }
        $page->content = htmlentities($request->get('content'), ENT_COMPAT , "UTF-8");
        //$page->type = $request->get('type');

        $page->save();

        return redirect()->route('admin.page.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd('delete:'.$id);
        $this->repository->delete($id);

        return redirect()->route('admin.page.index');
    }

}
