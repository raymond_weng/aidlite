<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Repositories\PageRepository;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $pageRepository;

    public function __construct(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    public function index()
    {
        $about = $this->pageRepository->pageByType('about');
        if (!isset($about)) {
            $about = new Page();
        }
        //dd($about);
        return view('about.index', compact('about'));
    }

}
