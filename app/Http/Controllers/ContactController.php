<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $error = 0;
        return view('contact.index', compact('error'));
    }

    public function send(Request $request)
    {
    	$error = 0;
    	$data = $request->all();
        //$messageBody = $this->getMessageBody($data);
        $to = config('mail.to');

        Mail::send('emails.contact', $data, function ($message) use ($data, $to) {
            $message->from($data['email'], $data['fullName']);
            $message->to($to);
            $message->subject('Web inquiry');
        });

        //判斷mail是否寄送失敗
        if (Mail::failures()) {
        	$error = 1;
            
        } else {
        	$error = 2;
        }

        // otherwise everything is okay ...
        return view('contact.index', compact('error'));
    }

    public function inquiry()
    {
        $error = 0;
        return view('contact.inquiry', compact('error'));
    }

    public function inquiry_send(Request $request)
    {
    	$error = 0;
    	$data = $request->all();
        //$messageBody = $this->getMessageBody($data);
        $to = config('mail.to');

        Mail::send('emails.inquiry', $data, function ($message) use ($data, $to) {
            $message->from($data['email'], $data['fullName']);
            $message->to($to);
            $message->subject('Web inquiry');
        });

        //判斷mail是否寄送失敗
        if (Mail::failures()) {
        	$error = 1;
            // otherwise everything is okay ...
            return view('contact.inquiry', compact('error'));
        } else {
        	//$error = 2;
        	return redirect('/')->with('error', 2);
        }

        
    }

}
