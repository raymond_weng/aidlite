<?php

namespace App\Http\Controllers;

use App\Models\NewsCategory;
use App\Repositories\NewsRepository;
use App\Repositories\NewsCategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;

class NewsCategoryController extends Controller
{
	protected $repository;
    protected $categoryRepository;

    public function __construct(NewsRepository $repository, NewsCategoryRepository $categoryRepository)
    {
        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newsCategoryFirst = $this->categoryRepository->newsCategoryFirst();
        if(isset($newsCategoryFirst)) {
            $id = $newsCategoryFirst->id;
            $newsList = $this->repository->newsListByCategoryAndPublish($id);
            $newsCategory = $this->categoryRepository->newsCategory($id);
        } else {
            $newsList = new Collection();
            $newsCategory = new NewsCategory();
        }

        $newsCategoryList = $this->categoryRepository->newsCategoriesByPublish();

        //dd($newsCategory);
        return view('news.index', compact('newsList', 'newsCategoryList', 'newsCategory', 'id'));
    }

    public function show($id)
    {
        $newsList = $this->repository->newsListByCategoryAndPublish($id);
        $newsCategoryList = $this->categoryRepository->newsCategoriesByPublish();
        $newsCategory = $this->categoryRepository->newsCategory($id);

        return view('news.index', compact('newsList', 'newsCategoryList', 'newsCategory', 'id'));
    }

}
