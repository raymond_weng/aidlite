<?php

namespace App\Http\Controllers;

use App\Repositories\InfoRepository;
use App\Repositories\InfoCategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class InfoController extends Controller
{
	protected $repository;
    protected $categoryRepository;

    public function __construct(InfoRepository $repository, InfoCategoryRepository $categoryRepository)
    {
        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $info = $this->repository->info($id);
        $infoCategory = $this->categoryRepository->infoCategory($info->category_id);
//dd($infoCategory);
        $page_title = $info->title;
        $page_keyword = null;
        if (isset($info->seo_keyword)) {
            $page_keyword = $info->seo_keyword;
        }
        $page_description = null;
        if (isset($info->excerpt)) {
            $page_description = $info->excerpt;
        }
        return view('info.detail', compact('page_title', 'page_keyword', 'page_description', 'info', 'infoCategory'));
    }

}
