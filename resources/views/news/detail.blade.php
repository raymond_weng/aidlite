@extends('layouts.default')

@section('css')
<style>
@media (max-width: 768px) {
    .envor-page-title-1 {
        display: none;
    }
}
</style>
@stop

@section('content')
      <!--

      Page Title start

      //-->
      <section class="envor-page-title-1" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9">
              <h1>News</h1>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">&nbsp;</div>
          </div>
        </div>
      <!--

      Page Title end

      //-->
      </section>
      <!--

      Desktop breadscrubs start

      //-->
      <section class="envor-desktop-breadscrubs">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="envor-desktop-breadscrubs-inner">
                <a href="{{ url('/') }}">Home</a><i class="fa fa-angle-double-right"></i><a href="{{ url('newsCategories/'.$newsCategory->id) }}">{{ $newsCategory->name }}</a><i class="fa fa-angle-double-right"></i>{{ $news->title }}
              </div>
            </div>
          </div>
        </div>
      <!--

      Desktop breadscrubs end

      //-->
      </section>
      <!--

      Mobile breadscrubs start

      //-->
      <section class="envor-mobile-breadscrubs">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <a href="{{ url('/') }}">Home</a><i class="fa fa-angle-double-right"></i><a href="{{ url('newsCategories/'.$newsCategory->id) }}">{{ $newsCategory->name }}</a><i class="fa fa-angle-double-right"></i>{{ $news->title }}
            </div>
          </div>
        </div>
      <!--

      Mobile breadscrubs end

      //-->
      </section>
      <!--

      Main Content start

      //-->
      <section class="envor-section">
        <div class="container">
          <div class="row">

            <!--

            Right Sidebar start

            //-->
            <div class="col-lg-12 col-md-12">
              <h3 class="align-left" style="margin-top: 0px;"><strong>{{ $news->title }}</strong></h3>
              <article style="display: block;">
                {!! html_entity_decode($news->content) !!}
                <!--p style="font-size: 20px; color: black;"><img src="img/img2.png" style="float: left; margin: 0px 10px 10px 10px;">NewsNews</p-->
              </article>
            </div>
          </div>
        </div>
      <!--

      Main Content start

      //-->
      </section>
      
      
@stop
