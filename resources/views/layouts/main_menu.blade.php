      <div class="envor-header-bg" id="envor-header-menu">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="envor-relative">
                <!--

                Site Logo start

                //-->
                <a href="{{ url('/') }}">
                <div class="envor-logo">
                  <img src="{{ asset('img/logo300-2.png') }}" alt="Envor Logo" style="width: 200px;">
                  
                </div>
                </a>
                <!--

                Site Logo end

                //-->
                
                <!--

                Desktop Menu start

                //-->
                @php
                  $productCategoryRepository = new \App\Repositories\ProductCategoryRepository(new \App\Models\ProductCategory());
                  $categories = $productCategoryRepository->categoriesByRoot();
                @endphp
                <nav>
                  <ul>
                    <li class="home" style="cursor:pointer; width: 90px; text-align: center;" onclick="location.href='{{ url('/') }}';">
                      <a href="{{ url('/') }}">Home</a>
                      <span class="hover" style="display: none;"></span>
                    </li>
                    <li class="products" style="width: 90px; text-align: center;" >
                      <a href="{{ url('productCategories') }}">Products</a>
                      <span class="hover" style="display: none;"></span>
                      <ul>
                        @foreach($categories as $category)
                          @if(isset($category->subCategories) && count($category->subCategories) > 0)
                          <li style="cursor: pointer;" onclick="location.href='{{ url('productCategories/'.$category->subCategories[0]->id) }}';">
                            <a href="{{ url('productCategories/'.$category->subCategories[0]->id) }}">{{ $category->name }}</a>
                          </li>
                          @else
                          <li>
                            <a style="pointer-events: none;">{{ $category->name }}</a>
                          </li>
                          @endif
                        @endforeach
                      </ul>
                    </li>
                    <li class="about" style="cursor:pointer; width: 90px; text-align: center;" onclick="location.href='{{ url('about') }}';">
                      <a href="{{ url('about') }}">About</a>
                      <span class="hover" style="display: none;"></span>
                    </li>
                    <li class="news" style="cursor:pointer; width: 90px; text-align: center;" onclick="location.href='{{ url('newsCategories') }}';">
                      <a href="{{ url('newsCategories') }}">News</a>
                      <span class="hover" style="display: none;"></span>
                    </li>
                    <li class="knowledge" style="cursor:pointer; width: 90px; text-align: center;" onclick="location.href='{{ url('infoCategories') }}';">
                      <a href="{{ url('infoCategories') }}">Info.</a>
                      <span class="hover" style="display: none;"></span>
                    </li>
                    <li class="contact" style="cursor:pointer; width: 90px; text-align: center;" onclick="location.href='{{ url('contact') }}';">
                      <a href="{{ url('contact') }}">Contact</a>
                      <span class="hover" style="display: none;"></span>
                    </li>
                  </ul>
                <!--

                Desktop Menu end

                //-->
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>