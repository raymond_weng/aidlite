<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131201112-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-131201112-1');
    </script>
    @if( isset($page_title) )
        <title>{{ $page_title }}</title>
    @else
        <title>"{{ config('app.page_title', 'Aidlite') }}"</title>
    @endif
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if( isset($page_keyword) )
        <meta name="keyword" content="{{ $page_keyword }}" >
    @else
        <meta name="keyword" content="{{ config('app.page_keyword', 'Aidlite') }}">
    @endif
    @if( isset($page_description) )
        <meta name="description" content="{{ $page_description }}" >
    @else
        <meta name="description" content="{{ config('app.page_description', 'Aidlite') }}">
    @endif

    <link rel="shortcut icon" href="favicon.ico">


    <!--
    * Google Fonts
    //-->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/jquery.mCustomScrollbar.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/colorbox-skins/5/colorbox.css') }}" type="text/css">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css">
    

    <link href="{{ asset('css/header/h1.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/color1.css') }}" rel="stylesheet" type="text/css" id="envor-site-color">
    <link href="{{ asset('css/rivathemes.css') }}" rel="stylesheet" type="text/css">

    <!-- LayerSlider styles -->
    <link rel="stylesheet" href="{{ asset('css/layerslider/css/layerslider.css') }}" type="text/css">

    <link rel="stylesheet" href="{{ asset('css/flexslider.css') }}" type="text/css">
    @yield('css')

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="js/vendor/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="{{ asset('js/vendor/modernizr-2.6.2-respond-1.1.0.min.js') }}"></script>
    <style type="text/css">
    	nav > ul > li.home, nav > ul > li.home > span.hover {
    		background: url({{ asset('img/home.png') }}) 50% 30% no-repeat #fff;
    	}
    	nav > ul > li.products, nav > ul > li.products > span.hover {
    		background: url({{ asset('img/products.png') }}) 50% 30% no-repeat #fff;
    	}
    	nav > ul > li.about, nav > ul > li.about > span.hover {
    		background: url({{ asset('img/about.png') }}) 50% 30% no-repeat #fff;
    	}
    	nav > ul > li.news, nav > ul > li.news > span.hover {
    		background: url({{ asset('img/news.png') }}) 50% 30% no-repeat #fff;
    	}
    	nav > ul > li.knowledge, nav > ul > li.knowledge > span.hover {
    		background: url({{ asset('img/knowledge.png') }}) 50% 30% no-repeat #fff;
    	}
    	nav > ul > li.contact, nav > ul > li.contact > span.hover {
    		background: url({{ asset('img/contact.png') }}) 50% 30% no-repeat #fff;
    	}

    	@media (max-width: 480px) {
    	    ul.search_bar {
    	        display: inline-block;
    	        text-align: center;
    	        margin: 0 auto;
    	        padding-inline-start: 0px;
    	    }

    	    #envor-header-top > div.container {
    	        text-align: center;
    	    }

    	    #envor-mobile-menu-btn {
    	        top: 80px;
    	    }

    	    #envor-history-btn {
	            top: 80px;
            }

            #envor-header-menu {
                top: 84px;
                z-index: 2;
                height: 85px;
            }

            .envor-logo {
                position: absolute;
                left: 0px;
                top: 0px;
            }
    	}

    	@media (max-width: 768px) and (min-width: 480px) {
    	    ul.search_bar {
    	        display: inline-block;
    	        text-align: center;
    	        margin: 0 auto;
    	        padding-inline-start: 0px;
    	    }

    	    #envor-header-top > div.container {
    	        text-align: center;
    	    }

    	    #envor-mobile-menu-btn {
    	        top: 80px;
    	    }
    	    
    	    #envor-history-btn {
	            top: 80px;
            }

            #envor-header-menu {
                top: 84px;
                z-index: 2;
                height: 85px;
            }

            .envor-logo {
                position: absolute;
                left: 50%;
                transform: translateX(-50%);
            }
    	}

    	@media (min-width: 768px) {
    	    ul.search_bar {
    	        display: none;
    	        margin: 0 auto;
    	    }

            .envor-logo {
                position: absolute;
                left: 0px;
                top: 0px;
            }
    	}

    	@media (max-width: 991px) and (min-width: 768px) {
    	    #envor-header-menu {
                top: 80px;
                z-index: 2;
            }
    	}

    	#functionTable tr > th {background: #b2b2b2;}
    	#functionTable tr:nth-child(even) {background: #fff;}
        #functionTable tr:nth-child(odd) {background: #ccc;}

    	footer.envor-footer {
			color: black;
			background: -webkit-linear-gradient(#C0C0C0,#696969);
			background: -o-linear-gradient(#C0C0C0,#696969);
			background: -moz-linear-gradient(#C0C0C0,#696969);
			background: linear-gradient(#C0C0C0,#696969);
		}
    </style>

  </head>