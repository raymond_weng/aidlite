    <i class="glyphicon glyphicon-align-justify" id="envor-mobile-menu-btn"></i>
    <div class="envor-mobile-menu" id="envor-mobile-menu">
      <h3>Menu</h3>
      <nav>
        <ul>
          <li style="cursor:pointer;" onclick="location.href='{{ url('/') }}';">
            <a href="{{ url('/') }}">Home</a>
          </li>
          <li>
            <a href="#">Products</a>
            <ul>
              @php
                $productCategoryRepository = new \App\Repositories\ProductCategoryRepository(new \App\Models\ProductCategory());
                $categories = $productCategoryRepository->categoriesByRoot();
              @endphp
              @foreach($categories as $category)
              <li>
                <a>{{ $category->name }}</a>
                @if(isset($category->subCategories) && count($category->subCategories) > 0)
                  <ul>
                  @foreach($category->subCategories as $subCategory)
                    <li style="cursor:pointer;" onclick="location.href='/productCategories/{{ $subCategory->id }}';"><a href="/productCategories/{{ $subCategory->id }}">{{ $subCategory->name }}</a></li>
                  @endforeach
                  </ul>
                @endif
              </li>
              @endforeach
            </ul>
          </li>
          <li style="cursor:pointer;" onclick="location.href='{{ url('about') }}';">
            <a href="{{ url('about') }}">About us</a>
          </li>
          <li>
            <a href="#">News</a>
            <ul>
              @php
                $newsRepository = new \App\Repositories\NewsCategoryRepository(new \App\Models\NewsCategory());
                $categories = $newsRepository->newsCategories();
              @endphp
              @foreach($categories as $category)
              <li style="cursor:pointer;" onclick="location.href='/newsCategories/{{ $category->id }}';"><a href="/newsCategories/{{ $category->id }}">{{ $category->name }}</a></li>
              @endforeach
            </ul>
          </li>
          <li>
            <a href="#">Info.</a>
            <ul>
              @php
                $infoRepository = new \App\Repositories\InfoCategoryRepository(new \App\Models\InfoCategory());
                $categories = $infoRepository->infoCategories();
              @endphp
              @foreach($categories as $category)
              <li style="cursor:pointer;" onclick="location.href='/infoCategories/{{ $category->id }}';"><a href="/infoCategories/{{ $category->id }}">{{ $category->name }}</a></li>
              @endforeach
            </ul>
          </li>
          <li style="cursor:pointer;" onclick="location.href='{{ url('contact') }}';">
            <a href="{{ url('contact') }}">contact us</a>
          </li>
        </ul>
      </nav>
      <!--

      Mobile Menu end

      //-->
    </div>