    <footer class="envor-footer envor-footer-1">
      <div class="container">
        <div class="row">
          <!--

          Footer About Widget start

          //-->
          <div class="col-lg-3 col-md-3">
            <div class="envor-widget">
              <h3 style="color: black;">About</h3>
              <div class="envor-widget-inner">
                <p>Aidlite Co., Ltd, founded in 1995, is dedicated to development and manufacture of excellent quality & high efficiency LED lighting technology. We combine several decades of expertise & experience allows us to be a vertically integrated manufacturer from initial design to finished products. Aidlite Co., Ltd is therefore one of the pioneers of the LED lighting industry in Taiwan.<br><a style="float: right;" href="{{ url('about') }}">Read More <i class="fa fa-arrow-circle-right"></i></a></p>
                
                
              </div>
            </div>
          </div>
          <!--

          Footer About Widget end

          //-->
          
          <!--

          Footer Contacts Widget start

          //-->
          <div class="col-lg-3 col-md-3">
            <div class="envor-widget envor-contacts-widget">
              <h3 style="color: black;">Contacts</h3>
              <div class="envor-widget-inner">
                <p>
                  <i class="fa fa-skype"><span style="vertical-align: middle; font-size: 14px; color: #000; position: absolute; left: 35px; width: 215px;"><a href="skype:pub@aidlite.com.tw?call">AIDLITE-LED Expert</a></span></i>
                </p>
                <p>
                  <i class="fa fa-phone"><span style="vertical-align: middle; font-size: 14px; color: #000; position: absolute; left: 35px; width: 215px; line-height: 20px;">TEL : <a href="tel:886-2-27359569">886-2-27359569</a> <br>FAX : 886-2-27359621</span></i>
                </p>
                <p>
                  <i class="fa fa-envelope"><span style="vertical-align: middle; font-size: 14px; position: absolute; left: 35px;"><a href="mailto:aidlite@ms38.hinet.net">aidlite@ms38.hinet.net</a></span></i>
                </p>
                <p>
                  <i class="fa fa-map-marker"><span style="vertical-align: middle; font-size: 14px; color: #000; position: absolute; left: 35px; width: 215px; line-height: 20px;"><a class="iframe cboxElement" href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3615.199176597968!2d121.5534472147589!3d25.027313844714048!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442aa34bf914529%3A0x44e27571a96ecea4!2zMTA25Y-w5YyX5biC5aSn5a6J5Y2A5Z-66ZqG6Lev5LqM5q61MTY06Jmf!5e0!3m2!1szh-TW!2stw!4v1520319525136">5F-4, NO. 164, Sec.2, Keelung Road, Taipei 10675 Taiwan.</a></span></i>
                  
                </p>
              </div>
            </div>
          </div>
          <!--

          Footer Contacts Widget end

          //-->

          <!--

          Footer News Widget start

          //-->
          <div class="col-lg-3 col-md-3">
            <div class="envor-widget envor-contacts-widget">
              <h3 style="color: black;">Social media</h3>
              <div class="envor-widget-inner">
                <p>
                  <i class="fa fa-facebook"><span style="vertical-align: middle; font-size: 14px; color: #000; position: absolute; left: 35px; width: 215px;"><a href="https://www.facebook.com/aidlite">Facebook</a></span></i>
                  <!--a href="https://www.facebook.com/aidlite" target="_blank"><i class="fa fa-facebook"></i></a-->
                </p>
                <p>
                  <i class="fa fa-twitter"><span style="vertical-align: middle; font-size: 14px; color: #000; position: absolute; left: 35px; width: 215px;"><a href="https://twitter.com/aidlite1">Twitter</a></span></i>
                </p>
                <p>
                  <i class="fa fa-youtube"><span style="vertical-align: middle; font-size: 14px; color: #000; position: absolute; left: 35px; width: 215px;"><a href="https://www.youtube.com/aidlite">YouTube</a></span></i>
                </p>
                <p>
                  <i class="fa fa-instagram"><span style="vertical-align: middle; font-size: 14px; color: #000; position: absolute; left: 35px; width: 215px;"><a href="https://www.instagram.com/aidliteled">Instagram</a></span></i>
                </p>
              </div>
            </div>
          </div>
          <!--

          Footer News Widget end

          //-->

          <!--

          Footer Flickr Widget start

          //-->
          <div class="col-lg-3 col-md-3">
            <div class="envor-widget envor-flick-widget">
              <h3 style="color: black;">Map</h3>
              <div class="envor-widget-inner">
                <p><a class="iframe cboxElement" href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3615.199176597968!2d121.5534472147589!3d25.027313844714048!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442aa34bf914529%3A0x44e27571a96ecea4!2zMTA25Y-w5YyX5biC5aSn5a6J5Y2A5Z-66ZqG6Lev5LqM5q61MTY06Jmf!5e0!3m2!1szh-TW!2stw!4v1520319525136"><img src="/img/map.jpg" width="300" height="230"></a></p>
              </div>
            </div>
          </div>
          <!--

          Footer Flickr Widget end

          //-->
          

          
          <!--

          Footer Copyright start

          //-->
          <div class="col-lg-12">
            <div class="envor-widget envor-copyright-widget">
              <div class="envor-widget-inner">
                <p style="width: 80%;">Copyright © 2014 Aidlite Co., Ltd. All rights reserved | <a href="https://privacypolicies.com/privacy/view/65bbb7d3d7d226e236addf03e18f7b82" target="_blank">Privacy</a></p>
              </div>
            </div>
          <!--

          Footer Copyright end

          //-->
          </div>
        </div>
      </div>
    </footer>