@include('layouts.main_head')

    <body>
  

    <!--[if lt IE 7]>
    <p class="chromeframe">請<a href="http://browsehappy.com/">更新</a>您目前使用的瀏覽器。或者是下載<a href="http://www.google.com/chromeframe/?redirect=true">Google Chrome 。</p>
    <![endif]-->
    <!--

    Scroll to the top

    //-->
    <div id="to-the-top"><i class="fa fa-chevron-up"></i></div>
    <!--

    Image Preload

    //-->
    <!--div id="envor-preload">
      <span>Now loading.<br>Please wait.</span>
      <i class="fa fa-cog fa-spin"></i>
      <p></p>
    </div-->
    <!--

    Envor mobile menu start

    //-->
    @include('layouts.main_mobile_menu')
    @include('layouts.main_history')
    <!--

    Envor mobile menu end

    //-->
    
    <!--

    Envor mobile shopping cart start

    //-->
    <!--i class="glyphicon glyphicon-shopping-cart" id="envor-mobile-cart-btn"></i>
    <div class="envor-mobile-menu" id="envor-mobile-cart">
      <h3>shopping cart</h3>
      <div class="envor-mobile-cart-list">
        <p><a href="">product title</a> x2 <span class="price">$299.00</span></p>
        <p><a href="">product title</a> x2 <span class="price">$299.00</span></p>
        <p><a href="">product title</a> x2 <span class="price">$299.00</span></p>
        <p><a href="">product title</a> x2 <span class="price">$299.00</span></p>
        <p><a href="">product title</a> x2 <span class="price">$299.00</span></p>
        <p><a href="">product title</a> x2 <span class="price">$299.00</span></p>
        <p><a href="">product title</a> x2 <span class="price">$299.00</span></p>
      </div>
    </div-->
    <!--

    Envor mobile shopping cart end

    //-->
    
    <!--

    Envor header start

    //-->
    <header class="envor-header envor-header-1">
      <!--

      Top bar start

      //-->
      @include('layouts.main_top')
      <!--

      Top bar end

      //-->
      
      <!--

      Logo & Menu start

      //-->
      @include('layouts.main_menu')
      <!--

      Logo & Menu end

      //-->
      
    <!--

    Envor header end

    //-->
    </header>
    <!--

    Envor site content start

    //-->
    <div class="envor-content">
      @yield('content')
    </div>
    <!--

    Envor site content end

    //-->
    
    <!--

    Envor footer start

    //-->
    @include('layouts.main_down')
    <!--

    Envor footer end

    //-->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('js/vendor/jquery-1.11.0.min.js') }}"></script>

    <script src="{{ asset('js/vendor/core-1.0.5.js') }}"></script>

    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery.mCustomScrollbar.min.js') }}"></script>
    <script src="{{ asset('js/jquery.mousewheel.min.js') }}"></script>
    <script src="{{ asset('js/jquery.colorbox-min.js') }}"></script>
    <script src="{{ asset('js/preloadCssImages.jQuery_v5.js') }}"></script>
    <script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
    <!--
    * jQuery with jQuery Easing, and jQuery Transit JS
    //-->
    <script src="{{ asset('js/layerslider/jquery-easing-1.3.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/layerslider/jquery-transit-modified.js') }}" type="text/javascript"></script>
    <!--
    * LayerSlider from Kreatura Media with Transitions
    -->
    <script src="{{ asset('js/layerslider/layerslider.transitions.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/layerslider/layerslider.kreaturamedia.jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.flexslider-min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.rivathemes.js') }}"></script>
    <script type="text/javascript">
      $('document').ready(function() {
          $('.carousel').carousel();
          /*

          New & Featured products Slider

          */
          $('#latest-projects').rivaSlider({
            visible : 4,
            selector : 'envor-project'
          });
          /*

          History Slider

          */
          $('#history-projects').rivaSlider({
            visible : 4,
            selector : 'envor-project2'
          });

          $(".inline").colorbox({inline:true, width:"20%"});
          $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});

          if (document.documentElement.clientHeight > document.documentElement.clientWidth) {

              $(".img1").colorbox({rel:'group1', transition: "fade", innerWidth: "80%"});
          } else {

              $(".img1").colorbox({rel:'group1', transition: "fade", innerHeight: "80%"});
          }

          $(document).bind('cbox_open', function(){
              $('body').css({overflow:'hidden'});
          }).bind('cbox_closed', function(){
              $('body').css({overflow:'auto'});
              $('#carousel').flexslider.update();
          });
          /*

          Sorting

          */
          $('#faq-sorting').rivaSorting({
            showAll : 0
          });

          var mySlider;
          var myCarousel;

          $('#carousel').flexslider({
		    animation: "slide",
		    controlNav: false,
		    animationLoop: false,
		    slideshow: false,
		    itemWidth: 160,
		    itemMargin: 5,
		    asNavFor: '#slider',
              start: function(slider){
                  mySlider=slider;
              }
          });

		  $('#slider').flexslider({
		    animation: "slide",
		    controlNav: false,
		    animationLoop: false,
		    slideshow: false,
		    sync: "#carousel",
              start: function(slider){
                  myCarousel=slider;
              }
  		  });

      });

      var i = 1;
  	  $('#toggle_' + i).addClass('open');

    </script>
    @yield('js')
    <script src="{{ asset('js/envor.js') }}"></script>
    <script type="text/javascript">
      $('document').ready(function() {
          /*

          Preload Images

          */
          var imgs = new Array(), $imgs = $('img');
          for (var i = 0; i < $imgs.length; i++) {
            imgs[i] = new Image();
            imgs[i].src = $imgs.eq(i).attr('src');
          }
          Core.preloader.queue(imgs).preload(function() {
            var images = $('a').map(function() {
                    return $(this).attr('href');
            }).get();
            Core.preloader.queue(images).preload(function() {
                  $.preloadCssImages();
            })
          })
          $('#envor-preload').hide();

          showProductHistory();
          showInquiryCar();
      });
      //顯示產品記錄10筆
      function showProductHistory() {
          //console.log('---------histories-----------');
          var histories = jQuery.parseJSON(localStorage.getItem ('view_histories'));
          //var history = $('#envor-history')
          //console.log($('#envor-history nav ul'));
          histories.forEach(function(value, index, arr) {
              $("#envor-history nav ul")
                  .append($('<li>')
                      .append($('<a>')
                          .attr('href', '/product/' + value.id)
                          .append($('<img>')
                              .attr('src', "/storage/" + value.images[0].image))
                          .attr('width', '180px;')
                      )
                      .append($('<br>'))
                      .append($('<a>')
                          .attr('href', '/product/' + value.id)
                          .append(value.name)
                      )
                  )

          });
      }

      //顯示產品記錄10筆
      function showInquiryCar() {
          //console.log('---------Inquiry-----------');
          var products = jQuery.parseJSON(localStorage.getItem ('inquiry_car'));
          console.log('---------Inquiry:'+products.length+'-----------');
          $('#topbarcart').html('');
          $('#shopping-cart-count span').text(products.length);
          if (products.length > 0) {
              $('#shopping-cart-count').show();
              //console.log($('#topbarcart'));
              products.forEach(function(value, index, arr) {
                  //console.log(value);
                  $("#topbarcart")
                      .append($('<p>')
                          /*.append($('<a>')
                              .attr('href', '/product/' + value.id)
                              .append(value.model)
                          )
                          .append($('<br>'))*/
                              .append($('<a>')
                                  .attr('href', '/product/' + value.id)
                                  .attr('style', 'float: left;')
                                  .append(value.name)
                              )
                              .append($('<a>')
                                  .attr('onclick', 'delInquiryCart(' + value.id + ');')
                                  .attr('style', 'cursor: pointer; float: right;')
                                  .append('x')
                              )
                      )

              });
          } else {
              $('#shopping-cart-count').hide();
          }
      }

      function delInquiryCart(id) {
          //alert(id);
          var products = jQuery.parseJSON(localStorage.getItem ('inquiry_car'));
          products.forEach(function(value, index, arr) {
              if (id == value.id) {
                products.splice(index, 1);
              }
          });
          localStorage.setItem('inquiry_car', JSON.stringify(products));
          alert('remove the inquiry cart!');
          showInquiryCar();
      }
      /*

      Google Analytics Code

      */
      var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
      (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
      g.src='//www.google-analytics.com/ga.js';
      s.parentNode.insertBefore(g,s)}(document,'script'));
      /*

      Windows Phone 8 и Internet Explorer 10

      */
      if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement("style")
        msViewportStyle.appendChild(
          document.createTextNode(
            "@-ms-viewport{width:auto!important}"
          )
        )
        document.getElementsByTagName("head")[0].appendChild(msViewportStyle)
      }
    </script>
  </body>
</html>