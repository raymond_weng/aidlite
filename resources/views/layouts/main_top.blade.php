      <div class="envor-top-bar" id="envor-header-top">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <!--

              Contact information start

              //-->
              <p class="contacts"><i class="fa fa-phone"></i> <a href="tel:886-2-27359569">886-2-27359569</a></p>
              <p class="contacts"><i class="fa fa-envelope"></i> <a href="mailto:aidlite@ms38.hinet.net">aidlite@ms38.hinet.net</a></p>

              <!--

              Contact information end

              //-->
              
              <!--

              Social Buttons start

              //-->
              <ul class="social-btns">
                <li><a href="https://www.facebook.com/aidlite" style="font-size: 20px;" target="_blank"><i class="fa fa-facebook" style="font-size: :20px;"></i></a></li>
                <li><a href="https://twitter.com/aidlite1" style="font-size: 20px;" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://www.youtube.com/user/aidlite" style="font-size: 20px;" target="_blank"><i class="fa fa-youtube"></i></a></li>
                <li><a href="https://www.instagram.com/aidliteled" style="font-size: 20px;" target="_blank"><i class="fa fa-instagram"></i></a></li>
                <!--li><a class="inline cboxElement" href="#inline_login" style="font-size: 20px;"><i class="fa fa-users"></i></a></li-->
                <!--

                Shopping cart start

                //-->
                <div class="shopping-cart" style="float: left;">
                  <span style="position: relative;">
                    <span id="shopping-cart-count" hidden style="position:absolute; z-index:1; border-radius:50%; height:20px; width:20px; background:#238ff9;">
                      <span style="display:block; color:#FFFFFF; height:20px; line-height:20px; text-align:center">0</span>
                    </span>
                    <i class="fa fa-shopping-cart" style="font-size: 20px;"></i>
                  </span>

                  <div class="cart">
                    <p class="title" style="text-transform:capitalize;">Inquiry Cart</p>
                    <div class="cart-entry" id="topbarcart"></div>
                    <p class="cart-btns"><a href="/inquiry_car" class="envor-btn envor-btn-small envor-btn-primary" style="text-transform:capitalize;">View Cart</a></p>
                  </div>
                </div>
                <!--

                Shopping cart end

                //-->
                <form class="search" style="float: left; padding: 2px 0px 0px 2px;" name="productSearch" action="{{ route('product.search') }}" method="get">
                  <span style="line-height: 35px; color: #333;">
                    <input style="height: 36px; border-radius: 4px 0px 0px 4px;" type="text" name="productSearchName" placeholder="product name, type, description or part #" />
                    <button type="submit" style="border-radius: 0px 4px 4px 0px; padding-bottom: 34px; background: #2b2b2b;"><i class="fa fa-search" style="color: #fff;"></i></button>
                  </span>
                </form>
              </ul>
              <!--

              Social Buttons end

              //-->

              <!--

              Search Bar

              //-->
              <ul class="search_bar">
                <form class="search" style="float: left; padding: 2px 0px 0px 2px;" name="productSearch" action="{{ route('product.search') }}" method="get">
                  <span style="line-height: 35px; color: #333;">
                    <input style="height: 36px; border-radius: 4px 0px 0px 4px;" type="text" name="productSearchName" placeholder="product name, type, description or part #" />
                    <button type="submit" style="border-radius: 0px 4px 4px 0px; padding-bottom: 34px; background: #2b2b2b;"><i class="fa fa-search" style="color: #fff;"></i></button>
                  </span>
                </form>
              </ul>
              
              <!--div style="display:none">
			    <div id="inline_login" style="padding:10px; background:#fff;">
			      <p><img src="img/facebook_login.png" width="200" /></p>
			      <p>or</p>
			      <p><img src="img/twitter_login.png" width="200" /></p>
			      <p>or</p>
			      <p><span style="margin-right: 10px;">UserName</span><span><input style="float:none;" type="text" name="loginName" /></span></p>
			      <p><span style="margin-right: 10px;">Password</span><span><input style="float:none;" type="password" name="password" /></span></p>
			      <p><span><input type="submit" name="loginName" value="Login" /></span></p>
			    </div>
		      </div-->

            </div>
          </div>
        </div>
      </div>