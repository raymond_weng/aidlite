@extends('layouts.default')

@section('css')
<style>
label.error {
	color: red;
	width: 100%;
}

@media (max-width: 768px) {
    .envor-page-title-1 {
        display: none;
    }
}
</style>
@stop

@section('content')
      <!--

      Page Title start

      //-->
      <section class="envor-page-title-1" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9">
              <h1>Contact</h1>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">&nbsp;</div>
          </div>
        </div>
      <!--

      Page Title end

      //-->
      </section>
      <!--

      Desktop breadscrubs start

      //-->
      <section class="envor-desktop-breadscrubs">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="envor-desktop-breadscrubs-inner">
                <a href="{{ url('/') }}">Home</a><i class="fa fa-angle-double-right"></i>Contact
              </div>
            </div>
          </div>
        </div>
      <!--

      Desktop breadscrubs end

      //-->
      </section>
      <!--

      Mobile breadscrubs start

      //-->
      <section class="envor-mobile-breadscrubs">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <a href="{{ url('/') }}">Home</a><i class="fa fa-angle-double-right"></i>Contact
            </div>
          </div>
        </div>
      <!--

      Mobile breadscrubs end

      //-->
      </section>
      <!--

      Main Content start

      //-->
      <section class="envor-section">
        <div class="container">
          <div class="row">
            <!--

            Contact Form start

            //-->
            <div class="col-lg-12 col-md-12">
              @if ($error == 1)
              <div class="envor-msg envor-msg-error">
                <header>
                  Error
                  <i class="fa fa-times"></i>
                </header>
                <p>Email can't send！</p>
              </div>
              <div>&nbsp;</div>
              @elseif ($error == 2)
              <div class="envor-msg envor-msg-success">
                <header>
                  Success
                  <i class="fa fa-times"></i>
                </header>
                <p>Email send success！</p>
              </div>
              <div>&nbsp;</div>
              @endif
              <h4 style="text-transform:initial;">Want more information?<br>Pls provide us with your details for below list and our team will be happy to come back to you very soon.</h4>
              <p style="color: red; font-size: 16px;">* Required items</p>
              <form data-toggle="validator" role="form" class="envor-f1" name="contactForm" id="contactForm" action="sendemail" method="get">
                <p>
                  <label for="drop-company" style="width: 50%;"> <b style="color: red; font-size: 16px;">* </b>Company Name</label>
                  <input type="text" style="width: 100%;" name="companyName" id="drop-company" placeholder="Company Name...">
                </p>
                <p>
                  <label for="drop-name" style="width: 50%;"> <b style="color: red; font-size: 16px;">* </b>Name</label>
                  <input type="text" style="width: 100%;" name="fullName" id="drop-name" placeholder="Name...">
                </p>
                <p>
                  <label for="drop-country" style="width: 50%;"><b style="color: red; font-size: 16px;">* </b>Country</label>
                  <input type="text" style="width: 100%;" name="country" id="drop-country" placeholder="Country...">
                </p>
                <p>
                  <label for="drop-phone" style="width: 50%;">Telephone </label>
                  <input type="text" style="width: 100%;" id="drop-phone" name="phone" placeholder="Telephone...">
                </p>
                <p>Prefer Contact Way/Account</p>

                <p><label for="drop-email" style="width: 50%;">
                  <b style="color: red; font-size: 16px;">* </b>Email</label>
                  <input type="email" style="width: 100%;" id="drop-email" name="email" placeholder="Email...">
                </p>
                <p>
                  <label for="drop-skype" style="width: 50%;">Skype </label>
                  <input type="text" style="width: 100%;" id="drop-skype" name="skype" placeholder="Skype...">
                </p>
                <p>
                  <label for="drop-whatsapp" style="width: 50%;">Whatsapp </label>
                  <input type="text" style="width: 100%;" id="drop-whatsapp" name="whatsapp" placeholder="Whatsapp...">
                </p>
                <p>
                  <label for="drop-other" style="width: 50%;">Other </label>
                  <input type="text" style="width: 100%;" id="drop-other" name="other" placeholder="Other...">
                </p>
                <p>
                  <label for="drop-message" style="width: 50%;"><b style="color: red; font-size: 16px;">* </b>Message </label>
                  <textarea style="width: 100%; min-width: 100%;" name="content" id="drop-message" placeholder="Message..."></textarea>
                </p>
                <p style="text-align: right;"><button type="submit" class="envor-btn envor-btn-normal envor-btn-primary riva-prev-tab margin-left-0" style="text-transform: initial;">Submit</button></p>
              </form>
            <!--

            Contact Form end

            //-->
            </div>
          </div>
        </div>
      <!--

      Main Content start

      //-->
      </section>
      
      
@stop

@section('js')
<script src="{{ asset('js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#contactForm").validate({
			rules: {
				companyName: "required",
				fullName: "required",
				country: "required",
				email: {
					required: true,
					email: true
				},
				content: "required"
			},
			messages: {
				companyName: "Please fill in company name.",
				fullName: "Please fill in fullname.",
				country: "Please fill in country name.",
				email: "Please fill in email.",
				content: "Please fill in message."
			}
		});
    });


</script>

@stop