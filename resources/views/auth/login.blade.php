<!DOCTYPE html>
<html>

<head>
    <title>@yield('title', config('admin.title', 'Aidlite'))</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- global level css -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <!-- end of global level css -->
    <link href="{{ asset('assets/vendors/iCheck/css/square/blue.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet" />
    <!-- page level css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/login.css') }}" />
    <!-- end of page level css -->
</head>

<body>
<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-sm-6 col-sm-offset-3  col-md-5 col-md-offset-4 col-lg-4 col-lg-offset-4">
            <div id="container">
                <div id="wrapper">
                    <div id="login" class="animate form">
                        <form action="{{ route('login') }}" id="authentication" autocomplete="on" method="post">
                            {{ csrf_field() }}
                            <h3>
                                <img src="img/logo300-2.png" alt="josh logo">
                            </h3>
                            <div class="form-group ">
                                <label style="margin-bottom:0;" for="email" class="uname control-label"> <i class="livicon" data-name="mail" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i> E-mail
                                </label>
                                <input id="email" name="email" placeholder="E-mail" value="" />
                                <div class="col-sm-12">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label style="margin-bottom:0;" for="password" class="passwd"> <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i> Password
                                </label>
                                <input type="password" id="password" name="password" placeholder="輸入密碼" />
                                <div class="col-sm-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" name="remember" id="remember-me" value="remember-me" class="square-blue" />&nbsp;&nbsp;記住我
                                </label>
                            </div>
                            <p class="login button">
                                <input type="submit" value="Log In" class="btn btn-success" />
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- global js -->
<script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script>
<!-- end of global js -->
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/iCheck/js/icheck.js" type="text/javascript') }}"></script>
<script src="{{ asset('assets/js/pages/login.js" type="text/javascript') }}"></script>
</body>

</html>
