<!DOCTYPE html>
<html lang="zh-TW">
<head>
    <meta charset="utf-8">
</head>
<body>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:480px;border-collapse:collapse!important">
        <tr>
            <td bgcolor="#ffffff" align="left" style="padding-top:20px;padding-bottom:40px;padding-right:0px;padding-left:0px;color:#666666;font-family:'Lato',sans-serif;font-size:13px;font-weight:400;line-height:1.2">
            <table style="width:100%">
                <tr>
                    <td style="border:1px solid #eeeeee;background-color:#fbfbfb;padding:7px 0">
                    <table style="width:100%">
                        <tr>
                            <td width="10" style="padding:7px 0">&nbsp;</td>
                            <td style="padding:7px 0">
                                <font style="color:inherit;font-family:inherit;font-size:inherit">
                                    <p style="margin:3px 0 7px;text-transform:uppercase;font-weight:500;font-size:18px;">光甫詢價資訊</p>
                                </font>
                            </td>
                        </tr>
                    </table></td>
                </tr>
                <tr>
                  <td style="padding:7px 0"><font style="color:inherit;font-family:inherit;font-size:inherit">
                  <table bgcolor="#ffffff" style="width:100%;border-collapse:collapse">
                    <tr>
		              <th style="border:1px solid #d6d4d4">Model</th>
		              <th style="border:1px solid #d6d4d4">Name</th>
		              <th style="border:1px solid #d6d4d4">Quantity</th>
		            </tr>
                    @foreach ($product as $key => $p)
		            <tr>
		              <td style="border:1px solid #d6d4d4">{{ $p['product_model'] }}</td>
		              <td style="border:1px solid #d6d4d4">{{ $p['product_name'] }}</td>
		              <td style="border:1px solid #d6d4d4">
		                @if ($p['product_quantity'] == 1)
		                1-99
		                @elseif ($p['product_quantity'] == 2) 
		                100-499
		                @elseif ($p['product_quantity'] == 3)
		                500-999
		                @elseif ($p['product_quantity'] == 4)
		                >1000
		                @endif
		              </td>
		            </tr>
                    @endforeach
                  </table></font></td>
                </tr>
                <tr>
                    <td style="padding:7px 0"><font style="color:inherit;font-family:inherit;font-size:inherit">
                    <table bgcolor="#ffffff" style="width:100%;border-collapse:collapse">
                        <tr>
                            <td colspan="5" style="border:1px solid #d6d4d4;text-align:center;color:#777;padding:7px 0">&nbsp;&nbsp;</td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454">Company Name</font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
	                        </table></td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454"><strong>{{ $companyName }}</strong></font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
			                </table></td>
                        </tr>
                        <tr>
                            <td colspan="5" style="border:1px solid #d6d4d4;text-align:center;color:#777;padding:7px 0">&nbsp;&nbsp;</td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454">Name</font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
	                        </table></td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454"><strong>{{ $fullName }}</strong></font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
			                </table></td>
                        </tr>
                        <tr>
                            <td colspan="5" style="border:1px solid #d6d4d4;text-align:center;color:#777;padding:7px 0">&nbsp;&nbsp;</td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454">Country</font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
	                        </table></td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454"><strong>{{ $country }}</strong></font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
			                </table></td>
                        </tr>
                        <tr>
                            <td colspan="5" style="border:1px solid #d6d4d4;text-align:center;color:#777;padding:7px 0">&nbsp;&nbsp;</td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454">Telephone</font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
	                        </table></td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454"><strong>{{ $phone }}</strong></font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
			                </table></td>
                        </tr>
                        <tr>
                            <td colspan="5" style="border:1px solid #d6d4d4;text-align:center;color:#777;padding:7px 0">&nbsp;&nbsp;</td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454">Email</font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
	                        </table></td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454"><strong>{{ $email }}</strong></font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
			                </table></td>
                        </tr>
                        <tr>
                            <td colspan="5" style="border:1px solid #d6d4d4;text-align:center;color:#777;padding:7px 0">&nbsp;&nbsp;</td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454">Skype</font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
	                        </table></td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454"><strong>{{ $skype }}</strong></font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
			                </table></td>
                        </tr>
                        <tr>
                            <td colspan="5" style="border:1px solid #d6d4d4;text-align:center;color:#777;padding:7px 0">&nbsp;&nbsp;</td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454">Whatsapp</font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
	                        </table></td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454"><strong>{{ $whatsapp }}</strong></font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
			                </table></td>
                        </tr>
                        <tr>
                            <td colspan="5" style="border:1px solid #d6d4d4;text-align:center;color:#777;padding:7px 0">&nbsp;&nbsp;</td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454">Other</font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
	                        </table></td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454"><strong>{{ $other }}</strong></font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
			                </table></td>
                        </tr>
                        <tr>
                            <td colspan="5" style="border:1px solid #d6d4d4;text-align:center;color:#777;padding:7px 0">&nbsp;&nbsp;</td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454">Message</font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
	                        </table></td>
	                        <td style="border:1px solid #d6d4d4"><table>
	                            <tr>
				                    <td width="10">&nbsp;</td>
				                    <td><font size="2" face="Open-sans, sans-serif" color="#555454"><strong>{!! nl2br($content) !!}</strong></font></td>
				                    <td width="10">&nbsp;</td>
			                    </tr>
			                </table></td>
                        </tr>
                    </table></font></td>
                </tr>
            </table></td>
        </tr>
    </table>
</body>
</html>