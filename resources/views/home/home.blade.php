@extends('layouts.default')

@section('css')
<style type="text/css">

    .carousel-control.left, .carousel-control.right {
        background-image:none !important;
        filter:none !important;
    }
</style>
@stop


@section('content')
  <!--

      LayerSlider start

      //-->
  <section class="envor-section envor-home-slider">
    <div id="envor-home-images" class="carousel slide" data-ride="carousel">

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        @foreach ($banners as $banner)
        @if ($loop->first)
        <div class="item active">
        @else
        <div class="item">
        @endif
          @if ($banner->embed)
          {!! html_entity_decode($banner->embed) !!}
          @else

              <a href="{{ $banner->url }}" target="_blank">
                <img src="{{ Storage::url($banner->images[0]->image) }}" alt="{{ $banner->name }}" title="{{ $banner->name }}">
              </a>

          @endif
        </div>
        @endforeach
      </div>
      <!-- Controls -->
      <a class="left carousel-control" href="#envor-home-images" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a class="right carousel-control" href="#envor-home-images" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a>
    </div>
  </section>
  <!--

  LayerSlider end

  //-->
  <section class="envor-section envor-soc-buttons-list">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
        @foreach($subBanners as $subBanner)
          @php
            $img = $subBanner->images[0]->image;
            if(count($subBanner->images) > 1) {
              $hover_img = $subBanner->images[1]->image;
            }else {
              $hover_img = $subBanner->images[0]->image;
            }
          @endphp
          <!--

          Social Button Item start

          //-->
          <div class="envor-social-button-2">
            <div class="esb-main">
              <a href="{{ $subBanner->url }}"><img style="cursor: pointer;" alt="{{ $subBanner->name }}" title="{{ $subBanner->name }}" src="{{ Storage::url($img) }}" onmouseover="this.src='{{ Storage::url($hover_img) }}';" onmouseout="this.src='{{ Storage::url($img) }}';" width="60" /></a>
            </div>
            <!--

            Social Button Item end
              
            //-->
          </div>
        @endforeach
        </div>
      </div>
    </div>
    <!--

    Social Buttons List end

    //-->
  </section>
  
  <!--

  New & Featured products start
  Info start
  History start

  //-->
  <section class="envor-section" style="padding-top: 0px;">
    <div class="container">
      <div class="row">
        <div class="col-lg-9">
          <h2 style=" margin-bottom: 0px;"><strong>New & Featured products</strong></h2>
          <div class="envor-relative" id="latest-projects" style="z-index: 1;">
            <!--

            Projects Item start

            //-->
            @foreach($products as $product)
              <article class="envor-project envor-padding-left-30">
                <div class="envor-project-inner">
                  @if(isset($product->images) && count($product->images) > 0)
                    <figure>
                      <a href="/product/{{ $product->id }}">
                        <img src="{{ Storage::url($product->images[0]->image) }}" alt="{{ Storage::url($product->images[0]->alt) }}">
                      </a>
                    </figure>
                  @endif
                  <div class="envor-project-details" style="height: 80px;">
                    <p class="link" style="margin-top: 10px;"><a href="/product/{{ $product->id }}">{{ $product->name }}</a></p>
                  </div>
                </div>
              </article>
          @endforeach

          <!--

                Projects Navigation start

                //-->
            <div class="envor-navigation rivaslider-navigation">
              <a href="" class="back"><i class="glyphicon glyphicon-chevron-left"></i></a>
              <a href="" class="forward"><i class="glyphicon glyphicon-chevron-right"></i></a>
              <!--

              Projects Navigation end

              //-->
            </div>
          </div>
        </div>
        <!--

            Info

        //-->
        <div class="col-lg-3" style="padding-top: 0px;">
          <h2 style=" margin-bottom: 7px;"><strong>Info.</strong></h2>
          @foreach ($infoList as $infos)
            <p><a href="{{ url('info/'.$infos->id) }}">{{ $infos->title }}</a></p>
          @endforeach

          <p><a style="float: right;" href="{{ url('infoCategories') }}">Details <i class="fa fa-arrow-circle-right"></i></a></p>
        </div>

        <!--

            History start

            //-->
        <div class="col-lg-9" style="padding-top: 0px;">
          <h2 style="text-transform:capitalize; margin-bottom: 0px;"><strong>recently viewed</strong></h2>
          <div class="envor-relative" id="history-projects" style="z-index: 1;">

            <div class="envor-navigation rivaslider-navigation">
              <a href="" class="back"><i class="glyphicon glyphicon-chevron-left"></i></a>
              <a href="" class="forward"><i class="glyphicon glyphicon-chevron-right"></i></a>
            </div>
          </div>
        </div>
        <!--

            History end

            //-->
        <div class="col-lg-3"></div>
      </div>
    </div>
  </section>
  <!--

  New & Featured products end
  Info end
  History end

  //-->


@stop
@section('js')
  <script type="text/javascript">
  	  var w = $(document).width();
      var h = w * 0.3;
  	  $(window).resize(function () {
  		  var width = $(document).width();
  	      var height = width * 0.3;
  	      $('iframe').height(height);
          $('iframe').parents(".item").height(height);
  	      //console.log('width:'+width+',height:'+height)
  	  });
  	  
      $('document').ready(function() {
        @if (session('error') == 2)
          localStorage.removeItem('inquiry_car');
        @endif


      });

      //console.log('---------histories-----------');
      var histories = jQuery.parseJSON(localStorage.getItem ('view_histories'));
      histories.forEach(function(value, index, arr) {
          $("#history-projects")
              .prepend($('<article>')
                  .attr('class', 'envor-project2 envor-padding-left-30')
                  .append($('<div>')
                      .attr('class', 'envor-project-inner')
                      .append($('<figure>')
                          .append($('<a>')
                              .attr('href', '/product/' + value.id)
                              .append($('<img>')
                                  .attr('src', "/storage/" + value.images[0].image)
                              )
                          )
                      )
                      .append($('<div>')
                          .attr('class', 'envor-project-details')
                          .attr('style', 'height: 80px;')
                          .append($('<p>')
                              .attr('class', 'link')
                              .attr('style', 'margin-top: 10px;')
                              .append($('<a>')
                                  .attr('href', '/product/' + value.id)
                                  .append(value.name)
                              )
                          )
                      )
                  )
              )

      });

  </script>
@stop
