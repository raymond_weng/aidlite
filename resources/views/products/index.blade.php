@extends('layouts.default')

@section('css')
<style>
/*@media (max-width: 991px) {*/
/*    #col-md-3 {*/
/*        display: none;*/
/*    }*/
/*}*/

/*@media (max-width: 768px) {*/
/*    .envor-page-title-1 {*/
/*        display: none;*/
/*    }*/
/*}*/
@media (max-width: 767px) {
    .envor-page-title-1 {
        display: none;
    }
    figure {
        height: 360px;
    }
}
@media (min-width: 768px) and (max-width: 979px) {
    #col-md-3 {
        display: none;
    }

    figure {
        height: 320px;
    }
}
@media (min-width: 980px) and (max-width: 1199px) {
    figure {
        height: 150px;
    }
}
@media (min-width: 1200px) {
    figure {
        height: 200px;
    }
}
</style>
@stop

@section('content')
      <!--

      Page Title start

      //-->
      <section class="envor-page-title-1" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9">
              <h1>Products</h1>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">&nbsp;</div>
          </div>
        </div>
      <!--

      Page Title end

      //-->
      </section>
      <!--

      Desktop breadscrubs start

      //-->
      <section class="envor-desktop-breadscrubs">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="envor-desktop-breadscrubs-inner">
                <a href="{{ url('/') }}">Home</a><i class="fa fa-angle-double-right"></i>{{ $currentCategory->name }}
              </div>
            </div>
          </div>
        </div>
      <!--

      Desktop breadscrubs end

      //-->
      </section>
      <!--

      Mobile breadscrubs start

      //-->
      <section class="envor-mobile-breadscrubs">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <a href="{{ url('/') }}">Home</a><i class="fa fa-angle-double-right"></i>{{ $currentCategory->name }}
            </div>
          </div>
        </div>
      <!--

      Mobile breadscrubs end

      //-->
      </section>
      <!--

      Main Content start

      //-->
      <section class="envor-section">
        <div class="container">
          <div class="row">

            <!--

            Right Sidebar start

            //-->
            <div class="col-lg-3 col-md-3" id="col-md-3">
              <div class="envor-toggle">
                <!--

                Toggle Item

                //-->
                @foreach($categories as $category)
                  <article id="toggle_{{ $category->id }}">
                    @if(isset($category->subCategories) && count($category->subCategories) > 0)
                      <header>{{ $category->name }}<i class="fa fa-plus"></i></header>
                      @foreach($category->subCategories as $subCategory)
                        <a href="/productCategories/{{ $subCategory->id }}"><p style="border: 1px solid #e5e5e5; margin: 0 auto;">{{ $subCategory->name }}</p></a>
                      @endforeach
                    @else
                      <header>{{ $category->name }}<i class="fa fa-plus"></i></header>
                      <a href="/productCategories/{{ $category->id }}"><p style="border: 1px solid #e5e5e5; margin: 0 auto;">{{ $category->name }}</p></a>
                    @endif
                  </article>
                @endforeach
              </div>


              <form style="border: 1px solid #e5e5e5; padding: 10px;" id="specification_form" class="form-horizontal form-bordered" action="{{ route('product.specification.search') }}" method="GET">
                <div>
                    <span style="width: 100%;float: left; margin-top: 10px; font-size: 20px;">Filter</span>
                    <span style="width: 100%;float: left; margin-top: 10px;">Step 1: Select Category</span>
                    <span style="width: 100%; margin-top: 10px;">
                        <select id="specification_category" name="specification_category" style="margin-top: 10px; min-width: 190px; width: 100%;">
                        <!--option value="0">All</option-->
                        @foreach($specificationCategories as $specCategory)
                        <option value="{{ $specCategory->id }}">{{ $specCategory->name }}</option>
                        @endforeach
                        </select>
                    </span>
                    <span>&nbsp</span>
                    <span>&nbsp</span>
                    <span style="width: 100%;float: left; margin-top: 10px;">Step 2: Select Spec.</span>
                    <div id="specification_container">

                    </div>

                </div>
                <div style="text-align: right;"><button style="margin-top: 10px;" type="submit">Submit</button></div>
              </form>
            </div>

            <div class="col-lg-9 col-md-9">
              <div class="col-lg-12">
                <h2 style="margin-top: 0px;"><strong>@if(isset($currentCategory)){{ $currentCategory->name }}@endif</strong></h2>

                <div class="envor-projects-listing envor-projects-listing-4-cols">
                  @if (count($products) <= 0)
                  <img src="/img/Results_found.png" width="100%" alt="0 Results found" />
                  @endif
                  <!--

                  Project Item

                  //-->
                  @foreach($products as $product)
                      @php
                      //如果是search，要做轉換規格->產品
                            if (isset($product->product)) {
                                $product = $product->product;
                            }
                      @endphp
                    <article class="envor-project envor-padding-bottom-30 envor-padding-left-30 javascript html envor-sorting-item envor-listing-item">
                      <div class="envor-project-inner">
                            @if(isset($product->images) && count($product->images) > 0)
                        <figure>
                            <a href="/product/{{ $product->id }}">
                                <img src="{{ Storage::url($product->images[0]->image) }}" alt="{{ Storage::url($product->images[0]->alt) }}">
                            </a>

                        </figure>
                            @endif
                        <div class="envor-project-details" style="height: 80px;">
                            <p class="link" style="margin-top: 10px;"><a href="/product/{{ $product->id }}">{{ $product->name }}</a></p>
                        </div>
                      </div>
                    </article>
                  @endforeach
                </div>
              </div>
                <div class="col-lg-12 align-center">
                @if(isset($products) && count($products) > 0)
                    {!! $products->appends(Request::all())->links() !!}
                @endif
                </div>
            </div>
          </div>
        </div>


      <!--

      Main Content start

      //-->
      </section>


@stop
@section('silder_javascript')
    <script type="text/javascript">
      $('document').ready(function() {
          $('.carousel').carousel()
          /*

          New & Featured products Slider

          */
          $('#latest-projects').rivaSlider({
            visible : 4,
            selector : 'envor-project'
          });
          /*

          History Slider

          */
          $('#history-projects').rivaSlider({
            visible : 4,
            selector : 'envor-project2'
          });
          /*

          Our Partners Slider

          */
          $('#our-partners').rivaCarousel({
            visible : 5,
            selector : 'envor-partner-logo',
            mobile_visible : 1
          });
          /*

          Footer News Slider

          */
          $('#footer-news').rivaSlider({
            visible : 1,
            selector : 'envor-post-preview'
          });
          /*

          Testimonials #1 Carousel

          */
          $('#clients-testimonials').rivaCarousel({
            visible : 1,
            selector : 'envor-testimonials-1',
            mobile_visible : 1
          });

      });
    </script>
@stop
@section('js')
    <script>

        $( function() {
            //console.log('---start---');
            @php
                $specificationCategoryID = 0;
                if (isset($specificationCategories) && count($specificationCategories) > 0) {
                    $specificationCategoryID = $specificationCategories[0]->id;
                }

                $currentCategoryID = 0;
                if (isset($currentCategory)) {
                    if (isset($currentCategory->parent_id)) {
                        $currentCategoryID = $currentCategory->parent_id;
                    } else {
                        $currentCategoryID = $currentCategory->id;
                    }
                }

            @endphp

            var current_category_id = <?php echo $currentCategoryID; ?>;
            console.log('current_category_id:'+current_category_id);
            $("#toggle_" + current_category_id+" header").click();


            var specification_category_id = <?php echo $specificationCategoryID; ?>;
            console.log('specification_category_id:'+specification_category_id);
            if (specification_category_id !== 0) {
                //console.log('specification_category_id:' + specification_category_id);
                //$("#specification_category").val(specification_category_id).change();
                updateSpecification(specification_category_id);
            }

            $("#specification_category").on('change', function ()  {
                var category = $("#specification_category").val();
                //console.log(category);
                if(typeof category === 'undefined' || category.length == 0) {
                    //console.log('category === undefined');
                    return false;
                } else {
                    //console.log('/admin/product/specifications_by_category/'+ selected);
                    updateSpecification(category);
                    return false;
                }
            });
        });

        function updateSpecification(category) {
            $.ajax({
                url: '/admin/product/specifications_by_category/'+ category,
                method: 'GET',
                success: function (response) {
                    //alert('Form Submitted!');
                    //console.log(response);
                    var status = response.status;
                    if(status === "success") {
                        $("#specification_container span").remove();
                        var list = jQuery.parseJSON(JSON.stringify(response.data));
                        //console.log(list);
                        list.forEach(function(value, index, arr) {
                            var select_options = $('<select name="specifications[]" style="margin-top: 10px; min-width: 190px; width: 100%;"></select>');
                            var items = value.items;
                            var specification_id = value.id;
                            //console.log('specification_id:'+specification_id);
//console.log('index:'+index);
                            select_options.append($('<option>')
                                .attr('value', 0)
                                .append("All " + value.name)
                            );
                            items.forEach(function(value, i, arr) {
                                //console.log(value);
                                select_options.append($('<option>')
                                    .attr('value', value.id)
                                    .append("&nbsp; " + value.name)
                                )
                            });

                            $("#specification_container")
                                .append($('<span>')
                                    .attr('style', 'width: 100%; margin-top: 10px;')
                                    .append(select_options)
                                )
                        });
                    } else {
                        alert("輸入資料有誤");
                    }

                },
                error: function(){
                    alert("上傳資料失敗");
                }
            });
        }
    </script>
@stop

