@extends('layouts.default')

@section('css')
<style>
@media (max-width: 991px) {
    #col-md-3 {
        display: none;
    }
}

@media (max-width: 768px) {
    .envor-page-title-1 {
        display: none;
    }
}
.video-container {
    position:relative;
    padding-bottom:56.25%;
    padding-top:30px;
    height:0;
    overflow:hidden;
}

.video-container iframe, .video-container object, .video-container embed {
    position:absolute;
    top:0;
    left:0;
    width:100%;
    height:100%;
}
</style>
@stop

@section('content')
      <!--

      Page Title start

      //-->
      <section class="envor-page-title-1" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9">
              <h1>Product Detail</h1>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">&nbsp;</div>
          </div>
        </div>
      <!--

      Page Title end

      //-->
      </section>
      <!--

      Desktop breadscrubs start

      //-->
      <section class="envor-desktop-breadscrubs">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="envor-desktop-breadscrubs-inner">
                <a href="{{ url('/') }}">Home</a><i class="fa fa-angle-double-right"></i><a href="{{ url('productCategories/'.$product->productCategory->id) }}">{{ $product->productCategory->name }}</a><i class="fa fa-angle-double-right"></i>{{ $product->name }}
              </div>
            </div>
          </div>
        </div>
      <!--

      Desktop breadscrubs end

      //-->
      </section>
      <!--

      Mobile breadscrubs start

      //-->
      <section class="envor-mobile-breadscrubs">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <a href="{{ url('/') }}">Home</a><i class="fa fa-angle-double-right"></i><a href="{{ url('productCategories/'.$product->productCategory->id) }}">{{ $product->productCategory->name }}</a><i class="fa fa-angle-double-right"></i>{{ $product->name }}
            </div>
          </div>
        </div>
      <!--

      Mobile breadscrubs end

      //-->
      </section>
      <!--

      Main Content start

      //-->
      <section class="envor-section">
        <div class="container">
          <div class="row">

            <!--

            Right Sidebar start

            //-->
            <div class="col-lg-3 col-md-3" id="col-md-3">
              <div class="envor-toggle">
                <!--

                Toggle Item

                //-->
                @foreach($categories as $category)
                  <article id="toggle_{{ $category->id }}">
                    @if(isset($category->subCategories) && count($category->subCategories) > 0)
                      <header >{{ $category->name }}<i class="fa fa-plus"></i></header>
                      @foreach($category->subCategories as $subCategory)
                        <a href="/productCategories/{{ $subCategory->id }}"><p style="border: 1px solid #e5e5e5; margin: 0 auto;">{{ $subCategory->name }}</p></a>
                      @endforeach
                    @else
                      <header >{{ $category->name }}<i class="fa fa-plus"></i></header>
                      <a href="/productCategories/{{ $category->id }}"><p style="border: 1px solid #e5e5e5; margin: 0 auto;">{{ $category->name }}</p></a>
                    @endif
                  </article>
                @endforeach
              </div>


              <form style="border: 1px solid #e5e5e5; padding: 10px;" id="specification_form" class="form-horizontal form-bordered" action="{{ route('product.specification.search') }}" method="GET">
                {{ csrf_field() }}
                <div>
                    <span style="width: 100%;float: left; margin-top: 10px; font-size: 20px;">Filter</span>
                    <span style="width: 100%;float: left; margin-top: 10px;">Step 1: Select Category</span>
                  <span style="width: 100%; margin-top: 10px;">
                        <select id="specification_category" name="specification_category" style="margin-top: 10px; min-width: 190px; width: 100%;">
                        <!--option value="0">All</option-->
                          @foreach($specificationCategories as $specCategory)
                            <option value="{{ $specCategory->id }}">{{ $specCategory->name }}</option>
                          @endforeach
                        </select>
                    </span>
                    <span>&nbsp</span>
                    <span>&nbsp</span>
                  <span style="width: 100%;float: left; margin-top: 10px;">Step 2: Select Spec.</span>
                  <div id="specification_container">

                  </div>

                </div>
                <div style="text-align: right;"><button style="margin-top: 10px;" type="submit">Submit</button></div>
              </form>
            </div>

            <div class="col-lg-9 col-md-9">
              <div class="col-lg-6">
                <div id="slider" class="flexslider" style="width: 100%; ">
                    <div id="zoom" style="position: absolute; bottom: 5px; right: 5px; z-index: 2; font-size: 32px;"><i class="fa fa-search-plus" style="top: 0px;"></i></div>
				    <ul class="slides">
                      @foreach($product->images as $image)
                        <li>
                          <a class="img1 cboxElement" href="{{ Storage::url($image->image) }}">
                              <img src="{{ Storage::url($image->image) }}" />
                          </a>
                        </li>
                      @endforeach
				      <!-- items mirrored twice, total of 12 -->
				    </ul>
				</div>
				<div id="carousel" class="flexslider">
				    <ul class="slides">
                      @foreach($product->images as $image)
                        <li style="width: 160px; height: 120px; margin-right: 5px;">
                          <img src="{{ Storage::url($image->image) }}" style="width: 120px; height: 120px;" />
                        </li>
                      @endforeach
				      <!-- items mirrored twice, total of 12 -->
				    </ul>
				</div>
		      </div>
			  <div class="col-lg-6">
                <p>Share：
                      <a href="https://www.facebook.com/sharer/sharer.php?u={{ URL::current() }}" target="_blank"><img src="/img/fb.png" /></a>
                      <a href="https://twitter.com/intent/tweet?text={{ $product->name }}&amp;url={{ URL::current() }}" target="_blank"><img src="/img/twitter.png" /></a>
                      <a href="https://plus.google.com/share?url={{ URL::current() }}" target="_blank"><img src="/img/googleplus.png" /></a>
                </p>
			  	<h4>{{ $product->model }}</h4>
				<h3>{{ $product->name }}</h3>
				<p>{{ $product->description }}</p>
                <p><button id="inquiry" onclick="addToInquiry()">Add to inquiry</button>
			  </div>
              <div class="envor-sorting" id="faq-sorting">

                <div id="tabs" class="envor-tabs">
                  <header>
                    @if ($product->specifications)<span>Specifications</span>@endif
                        <span>Description</span>
                    @if ($product->video)<span>Video</span>@endif
                    @if ($product->photo)<span>Photo</span>@endif
                    @if ($product->additional_information)<span>Additional Information</span>@endif
                  </header>

                    @if ($product->specifications)
                    <article style="color: #333; padding-bottom: 30px;">{!! html_entity_decode($product->specifications) !!}</article>
                    @endif
                  <article style="color: #333; padding-bottom: 30px;">
                    <table border="1" cellspacing="1" cellpadding="5" width="100%" style="width:100.0%; color: #2b2b2b; border: 1px solid #dddddd;">

                      @foreach($product->productSpecifications as $specification)
                        <tr>
                          <td style="background: #f5f5f5; width: 20%;">{{ $specification->specification->name }}</td>
                          <td>{{ $specification->specificationItem->name }}</td>
                        </tr>
                      @endforeach
                    </table>
                    
                  </article>
                  @if ($product->video)
                  <article style="color: #333;">{!! html_entity_decode($product->video) !!}</article>
                  @endif
                  @if ($product->photo)
                  <article style="color: #333;">{!! html_entity_decode($product->photo) !!}</article>
                  @endif
                  @if ($product->additional_information)
                  <article style="color: #333;">{!! html_entity_decode($product->additional_information) !!}</article>
                  @endif
                </div>
              </div>
            </div>
          </div>
              
        </div>
      <!--

      Main Content start

      //-->
      </section>
      
      
@stop
@section('silder_javascript')
    <script type="text/javascript">
      $('document').ready(function() {
          $('.carousel').carousel()
          /*

          New & Featured products Slider
          
          */
          $('#latest-projects').rivaSlider({
            visible : 4,
            selector : 'envor-project'
          });
          /*

          History Slider
          
          */
          $('#history-projects').rivaSlider({
            visible : 4,
            selector : 'envor-project2'
          });
          /*

          Our Partners Slider

          */
          $('#our-partners').rivaCarousel({
            visible : 5,
            selector : 'envor-partner-logo',
            mobile_visible : 1
          });
          /*

          Footer News Slider

          */
          $('#footer-news').rivaSlider({
            visible : 1,
            selector : 'envor-post-preview'
          });
          /*

          Testimonials #1 Carousel

          */
          $('#clients-testimonials').rivaCarousel({
            visible : 1,
            selector : 'envor-testimonials-1',
            mobile_visible : 1
          });
      });
    </script>
@stop
@section('js')
    <script src="{{ asset('js/share.js') }}"></script>
  <script>

      $( function() {
          //console.log('---start---');
                @php
                  $specificationCategoryID = 0;
                  if (isset($specificationCategories) && count($specificationCategories) > 0) {
                      $specificationCategoryID = $specificationCategories[0]->id;
                  }

                    $currentCategoryID = 0;
                    if (isset($currentCategory) && isset($currentCategory->parent_id)) {
                        $currentCategoryID = $currentCategory->parent_id;
                    } else {
                        $currentCategoryID = $currentCategory->id;
                    }
                @endphp

          var current_category_id = <?php echo $currentCategoryID; ?>;
          $("#toggle_" + current_category_id+" header").click();

          var specification_category_id = <?php echo $specificationCategoryID; ?>;
          //console.log('specification_category_id:'+specification_category_id);
          if (specification_category_id !== 0) {
              //console.log('specification_category_id:' + specification_category_id);
              //$("#specification_category").val(specification_category_id).change();
              updateSpecification(specification_category_id);
          }

          $("#specification_category").on('change', function ()  {
              var category = $("#specification_category").val();
              //console.log(category);
              if(typeof category === 'undefined' || category.length == 0) {
                  //console.log('category === undefined');
                  return false;
              } else {
                  //console.log('/admin/product/specifications_by_category/'+ selected);
                  updateSpecification(category);
                  return false;
              }
          });

          var w = $(window).width();
          if (w < 768) {
              $('#inquiry').attr('style', 'display: none;');
          } else {
              $('#inquiry').removeAttr('style');
          }

          saveProductToHistory();
      });

      function saveProductToHistory() {
          //console.log('---------histories-----------');
          var histories = jQuery.parseJSON(localStorage.getItem ('view_histories'));
          var product = jQuery.parseJSON(JSON.stringify(<?php echo json_encode($product); ?>));
//          var oldProduct = histories.find(function(item, index, array){
//              return item.id == product.id;
//          });

          //console.log(product);
          var index = -1;
          if (histories !== null && histories.length > 0) {
              index = histories.findIndex(function(item) {
                  //console.log(item.id +'=='+ product.id);
                  return item.id == product.id;
              });
              //console.log('index:'+index);
          } else {
              histories = [];
          }

          if(index !== -1) {
              histories.splice(index, 1);
          }

          histories.push(product);

          if (histories.length > 10) {
              histories.splice(0, histories.length - 10);
          }

          localStorage.setItem ('view_histories',  JSON.stringify(histories)) ;
          //localStorage.clear();
          //console.log(histories);
      }

      function addToInquiry() {
          //console.log('---------addToInquiry-----------');
          var products = jQuery.parseJSON(localStorage.getItem ('inquiry_car'));
          var product = jQuery.parseJSON(JSON.stringify(<?php echo json_encode($product); ?>));
          //console.log(product);
          var index = -1;
          if (products === null || products.length == 0) {
              products = [];
          } else {
              index = products.findIndex(function(item) {
                  //console.log(item.id +'=='+ product.id);
                  return item.id == product.id;
              });
              //console.log('index:'+index);
          }

          if(index === -1) {
              products.push(product);
              localStorage.setItem ('inquiry_car',  JSON.stringify(products)) ;
          }
          alert('The product has joined the inquiry cart!');
          showInquiryCar();
      }

      function updateSpecification(category) {
          $.ajax({
              url: '/admin/product/specifications_by_category/'+ category,
              method: 'GET',
              success: function (response) {
                  //alert('Form Submitted!');
                  //console.log(response);
                  var status = response.status;
                  if(status === "success") {
                      $("#specification_container span").remove();
                      var list = jQuery.parseJSON(JSON.stringify(response.data));
                      //console.log(list);
                      list.forEach(function(value, index, arr) {
                          var select_options = $('<select name="specifications[]" style="margin-top: 10px; min-width: 190px; width: 100%;"></select>');
                          var items = value.items;
                          var specification_id = value.id;
                          //console.log('specification_id:'+specification_id);

                          select_options.append($('<option>')
                              .attr('value', 0)
                              .append("All " + value.name)
                          )
                          items.forEach(function(value, index, arr) {
                              //console.log(value);
                              select_options.append($('<option>')
                                  .attr('value', value.id)
                                  .append("&nbsp; " + value.name)
                              )
                          });

                          $("#specification_container")
                              .append($('<span>')
                                  .attr('style', 'width: 100%; margin-top: 10px;')
                                  .append(select_options)
                              )
                      });
                  } else {
                      alert("輸入資料有誤");
                  }

              },
              error: function(){
                  alert("上傳資料失敗");
              }
          });
      }

      $(window).resize( function() {
        var w = $(window).width();
        if (w < 768) {
            $('#inquiry').attr('style', 'display: none;');
        } else {
            $('#inquiry').removeAttr('style');
        }
      });
  </script>
@stop