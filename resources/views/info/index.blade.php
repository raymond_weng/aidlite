@extends('layouts.default')

@section('css')
<style>
@media (max-width: 991px) {
    #col-md-3 {
        display: none;
    }
}

@media (max-width: 768px) {
    .envor-page-title-1 {
        display: none;
    }
}
</style>
@stop

@section('content')
      <!--

      Page Title start

      //-->
      <section class="envor-page-title-1" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9">
              <h1>Info.</h1>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">&nbsp;</div>
          </div>
        </div>
      <!--

      Page Title end

      //-->
      </section>
      <!--

      Desktop breadscrubs start

      //-->
      <section class="envor-desktop-breadscrubs">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="envor-desktop-breadscrubs-inner">
                <a href="{{ url('/') }}">Home</a><i class="fa fa-angle-double-right"></i>{{ $infoCategory->name }}
              </div>
            </div>
          </div>
        </div>
      <!--

      Desktop breadscrubs end

      //-->
      </section>
      <!--

      Mobile breadscrubs start

      //-->
      <section class="envor-mobile-breadscrubs">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <a href="{{ url('/') }}">Home</a><i class="fa fa-angle-double-right"></i>{{ $infoCategory->name }}
            </div>
          </div>
        </div>
      <!--

      Mobile breadscrubs end

      //-->
      </section>
      <!--

      Main Content start

      //-->
      <section class="envor-section">
        <div class="container">
          <div class="row">

            <!--

            Right Sidebar start

            //-->
            <div class="col-lg-3 col-md-3" id="col-md-3">
              <div class="envor-toggle">
                <!--

                Toggle Item

                //-->
                @foreach ($infoCategoryList as $i)
                <a href="{{ url('infoCategories/'.$i->id) }}">
                <article @if (($loop->first && $id == "") || $id == $i->id)class="open"@endif>
                  <header @if (($loop->first && $id == "") || $id == $i->id)class="active"@endif>{{ $i->name }}</header>
                </article>
                </a>
                @endforeach
                
              </div>
            </div>

            <div class="col-lg-9 col-md-9">
              <div class="envor-widget envor-category-widget">
                <div class="envor-widget-inner">
                  <h2>{{ $infoCategory->name }}</h2>
                  <ul>
                    @foreach ($infoList as $info)
                    <li><p style="float: left; width: 70%;"><a href="{{ url('info/'.$info->id) }}">{{ $info->title }}</a></p><p style="float:right; width: 30%; text-align: right;">{{ \Carbon\Carbon::parse($info->post_date)->format('Y-m-d') }}</p></li>
                    @endforeach
                  </ul>
                </div>
              </div>
                <div class="col-lg-12 align-center">
                    @if(isset($infoList) && count($infoList) > 0)
                        {!! $infoList->appends(Request::all())->links() !!}
                    @endif
                </div>
            </div>
          </div>
        </div>
      <!--

      Main Content start

      //-->
      </section>
      
      
@stop