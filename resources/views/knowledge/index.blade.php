@extends('layouts.default')

@section('content')
      <!--

      Page Title start

      //-->
      <section class="envor-page-title-1" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9">
              <h1>Knowledge</h1>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">&nbsp;</div>
          </div>
        </div>
      <!--

      Page Title end

      //-->
      </section>
      <!--

      Desktop breadscrubs start

      //-->
      <section class="envor-desktop-breadscrubs">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="envor-desktop-breadscrubs-inner">
                <a href="{{ url('/') }}">Home</a><i class="fa fa-angle-double-right"></i>Knowledge
              </div>
            </div>
          </div>
        </div>
      <!--

      Desktop breadscrubs end

      //-->
      </section>
      <!--

      Mobile breadscrubs start

      //-->
      <section class="envor-mobile-breadscrubs">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <a href="{{ url('/') }}">Home</a><i class="fa fa-angle-double-right"></i>Knowledge
            </div>
          </div>
        </div>
      <!--

      Mobile breadscrubs end

      //-->
      </section>
      <!--

      Main Content start

      //-->
      <section class="envor-section">
        <div class="container">
          <div class="row">

            <!--

            Right Sidebar start

            //-->
            <div class="col-lg-3 col-md-3">
              <div class="envor-toggle">
                <!--

                Toggle Item

                //-->
                <article id="toggle_1">
                  <header>Knowledge</header>
                </article>
                <!--

                Toggle Item
                
                //-->
                <article id="toggle_2">
                  <header>Content</header>
                </article>
                
              </div>
            </div>

            <div class="col-lg-9 col-md-9">
              <div class="envor-widget envor-category-widget">
                <div class="envor-widget-inner">
                  <h2>Knowledge</h2>
                  <ul>
                    <li><p style="float: left; width: 70%;"><a href="{{ url('knowledgeDetail') }}">2018-TEST01</a></p><p style="float:right; width: 30%; text-align: right;">2018-03-05</p></li>
                    <li><p style="float: left; width: 70%;"><a href="{{ url('knowledgeDetail') }}">2018-TEST02</a></p><p style="float:right; width: 30%; text-align: right;">2018-03-05</p></li>
                    <li><p style="float: left; width: 70%;"><a href="{{ url('knowledgeDetail') }}">2018-TEST03</a></p><p style="float:right; width: 30%; text-align: right;">2018-03-05</p></li>
                    <li><p style="float: left; width: 70%;"><a href="{{ url('knowledgeDetail') }}">2018-TEST04</a></p><p style="float:right; width: 30%; text-align: right;">2018-03-05</p></li>
                    <li><p style="float: left; width: 70%;"><a href="{{ url('knowledgeDetail') }}">2018-TEST05</a></p><p style="float:right; width: 30%; text-align: right;">2018-03-05</p></li>
                  </ul>
                </div>
              </div>



            </div>
          </div>
        </div>
      <!--

      Main Content start

      //-->
      </section>
      
      
@stop
@section('silder_javascript')
    <script type="text/javascript">
      $('document').ready(function() {
          $('.carousel').carousel()
          /*

          New & Featured products Slider
          
          */
          $('#latest-projects').rivaSlider({
            visible : 4,
            selector : 'envor-project'
          });
          /*

          History Slider
          
          */
          $('#history-projects').rivaSlider({
            visible : 4,
            selector : 'envor-project2'
          });
          /*

          Our Partners Slider

          */
          $('#our-partners').rivaCarousel({
            visible : 5,
            selector : 'envor-partner-logo',
            mobile_visible : 1
          });
          /*

          Footer News Slider

          */
          $('#footer-news').rivaSlider({
            visible : 1,
            selector : 'envor-post-preview'
          });
          /*

          Testimonials #1 Carousel

          */
          $('#clients-testimonials').rivaCarousel({
            visible : 1,
            selector : 'envor-testimonials-1',
            mobile_visible : 1
          });
      });
    </script>
@stop