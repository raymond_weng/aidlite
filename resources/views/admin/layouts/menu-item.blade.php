@if (is_string($item))
    <li class="header">{{ $item }}</li>
@else
    <li @if(isset($item['submenu']))
            @foreach($item['submenu'] as $submenu)
                @if(Request::is($submenu['url'])) class="active" @endif
            @endforeach
        @elseif(isset($item['url']))
            @if(Request::is($item['url'])) class="active" @endif
        @endif>
        <a @if (isset($item['url'])) href="{{ "/".$item['url'] }}" @endif>
            @if(isset($item['icon']))
                <i class="livicon" data-name="{{ $item['icon'] }}" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
            @else
                <i class="fa fa-angle-double-right"></i>
            @endif
                <span class="title">{{ $item['text'] }}</span>
            @if(isset($item['submenu']))
                <span class="fa arrow"></span>
            @endif
        </a>
        @if (isset($item['submenu']))
            <ul class="sub-menu">
                @each('admin.layouts.menu-item', $item['submenu'], 'item')
            </ul>
        @endif
    </li>
@endif

