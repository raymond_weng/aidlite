@extends('admin.layouts.master')

@section('admin_css')
    @stack('css')
    @yield('css')
@stop

@section('body')
    <header class="header">
        <a href="/admin/home" class="logo navbar-brand">
            {!! config('admin.logo', '<b>Admin</b>LTE') !!}
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <div>
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <div class="responsive_nav"></div>
                </a>
            </div>
            <div class="navbar-right">

                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a class="dropdown-toggle" href="{{ url('/logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="livicon pull-left" data-name="sign-out"  data-c="#fff" data-hc="#fff"></i>
                            <div class="riot">
                                <div>登出<span>&nbsp;&nbsp;&nbsp;&nbsp;</span></div>
                            </div>
                        </a>
                        <form id="logout-form" action="{{ url(config('admin.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                            @if(config('admin.logout_method'))
                                {{ method_field(config('admin.logout_method')) }}
                            @endif
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar ">
                <div class="page-sidebar  sidebar-nav">
                    <!-- Sidebar Menu -->
                    <ul id="menu" class="page-sidebar-menu">
                        @each('admin.layouts.menu-item', Config::get('admin.menu'), 'item')
                    </ul>
                    <!-- /.sidebar-menu -->
                </div>
            </section>
            <!-- /.sidebar -->
        </aside>
        <aside class="right-side">
            <section class="content-header">
                @yield('content_header')
            </section>
            <section class="content">
                @yield('content')
            </section>
        </aside>
        <!-- right-side -->
    </div>
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
        <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
    </a>
    <!-- ./wrapper -->
@stop

@section('admin_js')
    @stack('js')
    @yield('js')
@stop
