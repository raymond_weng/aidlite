@extends('admin.layouts.page')

@section('title', 'Aidlite')

@section('content_header')
    <h1>產品分類列表</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.home') }}">
                <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li class="active">
            產品分類列表
        </li>

    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable">
                <div class="panel-heading clearfix">
                    <div class="col-lg-3 pull-left" style="display: inline-block;">
                        <form name="productCategorySearch" method="get" >
                            <label class="select-inline mar-left5">
                                <select name="category_id" class="form-control select2" >
                                    <option value="">請選擇</option>
                                    @foreach($subCategories as $subCategory)
                                    @if ($subCategory->layer == 0)
                                    <option value="{{ $subCategory->id }}" @if ($category_id == $subCategory->id) selected @endif >{{ $subCategory->name }}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </label>
                            <label class="input-inline">
                                <button type="submit" style="margin-bottom: 3px;" class="btn btn-effect-ripple btn-primary">搜尋</button>
                            </label>
                        </form>
                    </div>
                    <div class="pull-right">
                        <a href="{{ route('admin.product.categories.create') }}" class="btn btn-primary btn-sm">
                            <span><i class="fa fa-plus" id="addButton"></i>&nbsp;&nbsp;新增產品分類</span>
                        </a>
                    </div>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table" id="table3">
                        <thead>
                        <tr>
                            <th>名稱</th>
                            <th>父分類</th>
                            <!--th>排序</th-->
                            <th>時間</th>
                            <th>動作</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>
                                    @if ($category->layer > 0)
                                    @for ($i = 0; $i <= $category->layer; $i++)
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    @endfor
                                    @endif
                                    {{ $category->name }}
                                </td>
                                <td>
                                    @if (isset($category->parent))
                                        {{ $category->parent->name }}
                                    @endif
                                </td>
                                <!--td>{{ $category->sort }}</td-->
                                <td>{{ $category->updated_at }}</td>
                                <td>
                                    <a href="{{ route('admin.product.categories.edit', ['id' => $category->id]) }}">
                                        <span class="btn btn-success"><i class="fa fa-edit"></i></span>
                                    </a>
                                    <a href="#modal-danger" data-toggle="modal" data-target="#modal-danger" data-variable="{{ route('admin.product.categories.destroy', ['id' => $category->id]) }}">
                                        <span class="btn btn-danger"><i class="fa fa-trash"></i></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {!! $categories->appends(request()->all())->links() !!}
        </div>
    </div>

    <!--window-->
    <div class="modal fade" id="modal-danger" role="document">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4><i class="icon fa fa-ban"></i> 提醒</h4>
                </div>
                <div class="modal-body">
                    <p>確定要刪除嗎？</p>
                </div>
                <div class="modal-footer">

                    <form action="" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <button type="button" data-dismiss="modal" class="btn btn-default">取消</button>
                        <input type="submit" class="btn btn-primary" value="確定">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--window-->
@stop

@section('js')
    <script>

        $('#modal-danger').on('show.bs.modal', function(e) {
            var $target = $(e.relatedTarget);
            var variable = $target.attr('data-variable');
            console.log(variable);
            $('#delete_form').attr('action', variable);
//            var $modal = $(this);
//            var esseyId = e.relatedTarget.id;
//            $modal.find('.modal-body').html(esseyId);
        })

        $('document').ready(function() {
            @if (session('error') == 1)
            alert('請先刪除所有產品，再刪除此分類');
            @elseif (session('error') == 2)
            alert('請先刪除所有子分類，再刪除此分類');
            @endif
        });
  </script>

    </script>
@stop