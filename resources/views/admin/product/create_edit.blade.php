@extends('admin.layouts.page')

@section('css')
    <link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen" />
    <link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/all.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/switchery/css/switchery.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/radio_checkbox.css') }}">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('assets/css/pages/tab.css') }}" />
    <style type="text/css">
        #sortable{
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            /* 也可以是：display: inline-flex; 此為「行內 Flex」 */
            padding:0;
            margin: 0 auto;
        }
        #sortable > li{
            list-style: none;
            width: 160px;
            height: 160px;
            margin: 5px;
            text-align: center;
        }

        .sortable-ghost {
            opacity: .4;
        }

        i#plus > svg {
            top: 5px;
        }
    </style>
@stop

@section('title', 'Aidlite')

@section('content_header')
    <h1>@if(isset($product->id))編輯@else新增@endif產品</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.home') }}">
                <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="{{ route('admin.products.index') }}">產品列表</a>
        </li>
        <li class="active">@if(isset($product->id))編輯@else新增@endif產品</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                @php
                    $url = route('admin.products.store');
                    if(isset($product->id) && !$isCopy) {
                        $url = route('admin.products.update', ['id' => $product->id]);
                    }
                @endphp
                <div class="panel-heading">
                    <i class="livicon" data-name="adjust" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                </div>
                <div class="panel-body border">
                    <form id="tryitForm" class="form-horizontal form-bordered" role="form" action="{{ $url }}" method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(isset($product->id) && !$isCopy)
                            {{ method_field("PUT") }}
                        @endif
                        {{ csrf_field() }}
                        <!-- text input -->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input">名稱</label>
                            <div class="col-md-5">
                                <input type="text" id="form-text-input" name="name" class="form-control" placeholder="名稱" required
                                       value="@if(isset($product->name)){{ old('name', $product->name) }}@else{{ old('name') }}@endif">
                            </div>
                            <label class="col-md-1 control-label" for="form-text-input">分類</label>
                            <div class="col-md-2">
                                <select id="select" name="category_id" class="form-control select2" required>
                                    <option value="">請選擇...</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" @if((isset($product->category_id) && $product->category_id == $category->id) || old('category_id') == $category->id){{ 'selected="selected"' }}@endif @if ($category->layer == 0) disabled @endif>
                                        @if ($category->layer > 0)
                                        @for ($i = 0; $i <= $category->layer; $i++)
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        @endfor
                                        @endif
                                        {{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            @if ($errors->has('name'))
                                <div class="col-md-8 col-md-offset-2">
                                    <span class="text-danger">標題未填或格式錯誤</span>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-textarea-input">型號</label>
                            <div class="col-md-5">
                                <input type="text" id="form-text-input" name="model" class="form-control" placeholder="型號"required
                                       value="@if(isset($product->model)){{ old('model', $product->model) }}@else{{ old('model') }}@endif">
                            </div>
                            <label class="col-md-1 control-label" for="form-text-input">推薦</label>
                            <div class="col-md-2">
                                <input type="checkbox" name="my-checkbox" @if(isset($product->featured) && $product->featured == 1) checked @endif data-on-color="primary" data-off-color="info">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-textarea-input">關鍵字</label>
                            <div class="col-md-8">
                                <input type="text" id="form-text-input" name="seo_keyword" class="form-control"
                                       value="@if(isset($product->seo_keyword)){{ old('seo_keyword', $product->seo_keyword) }}@else{{ old('seo_keyword') }}@endif">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-textarea-input">摘要</label>
                            <div class="col-md-8">
                                <textarea id="form-textarea-input" name="seo_description" rows="3" class="form-control resize_vertical">@if (isset($product->seo_description)){{ old('seo_description', $product->seo_description) }}@else{{ old('seo_description') }}@endif</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-textarea-input">說明</label>
                            <div class="col-md-8">
                                <textarea id="form-textarea-input" name="description" rows="6" class="form-control resize_vertical" required>@if (isset($product->description)){{ old('description', $product->description) }}@else{{ old('description') }}@endif</textarea>
                            </div>
                            @if ($errors->has('description'))
                                <div class="col-md-8 col-md-offset-2">
                                    <span class="text-danger">說明未填或格式錯誤</span>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input"></label>
                            <div class="col-md-10"><p style="color: red; font-size: 16px;">* 圖的尺寸：寬600高600，檔案大小:2M以下</p></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input">圖</label>
                            <div class="col-md-8">
                                <div style="display:flex;">
                                    <div class="btn_add" id="btn_file_upload" style="text-align: center; border-width:3px;border-style:dashed;border-color:#FFAC55;padding:5px;margin:5px;width: 60px; height: 60px;display: inline-block;">
                                        <i id="plus" class="livicon" style="text-align: center;" data-name="plus" data-size="30" data-c="#FFAC55" data-hc="#FFAC55" data-loop="true"></i>
                                        <!--input id="fileUpload" name="image" type="file" style="display:none" /-->
                                        <input id="file_upload" name="file" type="file" style="display: none;" />
                                    </div>
                                    <div style="position:relative; border-width:3px;border-style:dashed;border-color:#FF0000;margin:5px;width: 60px; height: 60px;">
                                        <i id="trash" class="fa fa-fw fa-trash-o" style="position:absolute; font-size:30px; color:#FF0000; padding: 13px;"></i>
                                        <div class="trash_container" id="trash_container" style="width: 60px; height: 60px;"></div>
                                    </div>
                                </div>

                                <ul id="sortable">

                                </ul>
                            </div>
                            @if ($errors->has('image'))
                                <div class="col-md-8 col-md-offset-2">
                                    <span class="text-danger">未上傳圖檔</span>
                                </div>
                            @endif
                        </div>




                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2 bs-example">
                                <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                                    <li class="active">
                                        <a href="#specifications" data-toggle="tab">Specifications</a>
                                    </li>
                                    <li>
                                        <a href="#description" data-toggle="tab">Description</a>
                                    </li>
                                    <li>
                                        <a href="#video" data-toggle="tab">Video</a>
                                    </li>
                                    <li>
                                        <a href="#photo" data-toggle="tab">Photo</a>
                                    </li>
                                    <li>
                                        <a href="#additional_information" data-toggle="tab">Additional Information</a>
                                    </li>
                                </ul>
                                <div id="productTabContent" class="tab-content">
                                    <div class="tab-pane fade active in" id="specifications">
                                        <div class="bootstrap-admin-panel-content">
                                            <textarea class="form-control my-editor" name="specifications">@if (isset($product->specifications)){{ old('specification', $product->specifications) }}@else{{ old('specifications') }}@endif</textarea>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="description">
                                        <div class="form-group">
                                            <label class="col-md-1 control-label" for="form-text-input">分類</label>
                                            <div class="col-md-3">
                                                <select id="specification_category" name="specification_category_id" class="form-control select2" required>
                                                    <option value="">請選擇...</option>
                                                    @foreach($specificationCategories as $category)
                                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered" id="table_specification">
                                            <thead>
                                            <tr>
                                                <th>規格</th>
                                                <th>說明</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="video">
                                        <div class="bootstrap-admin-panel-content">
                                            <textarea class="form-control my-editor" name="video">@if (isset($product->video)){{ old('video', $product->video) }}@else{{ old('video') }}@endif</textarea>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="photo">
                                        <div class="bootstrap-admin-panel-content">
                                            <textarea class="form-control my-editor" name="photo">@if (isset($product->photo)){{ old('photo', $product->photo) }}@else{{ old('photo') }}@endif</textarea>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="additional_information">
                                        <div class="bootstrap-admin-panel-content">
                                            <textarea class="form-control my-editor" name="additional_information">@if (isset($product->additional_information)){{ old('additional_information', $product->additional_information) }}@else{{ old('additional_information') }}@endif</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-actions">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-effect-ripple btn-primary">送出</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('assets/vendors/tinymce/tinymce.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/config.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/switchery/js/switchery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/card/lib/js/jquery.card.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/radio_checkbox.js') }}"></script>
    <script src="{{ asset('assets/js/pages/tabs_accordions.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/Sortable/js/Sortable.js') }}" type="text/javascript"></script>
    <script>

        $( function() {
            initView();
            initEvent();
            initSortable();
            initData();
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var specification_list = [];

        function initData() {
            @php
                $new_images = array();
                if(!$isCopy) {
                    if(isset($product)) {
                        $images = $product->images;
                        foreach ($images as $image) {
                            array_push($new_images, ["id"=>$image->id, "image"=>Storage::url($image->image)]);
                        }
                    }
                }


                $specification_list = array();
                if (isset($product)) {
                    $productSpecifications = $product->productSpecifications;
                } else {
                    $productSpecifications = new \Illuminate\Database\Eloquent\Collection();
                }

                $specificationCategoryID = 0;
                if (isset($productSpecifications) && count($productSpecifications) > 0) {
                    $specificationCategoryID = $product->productSpecifications[0]->specification_category_id;
                    foreach ($productSpecifications as $specification) {
                        array_push($specification_list, $specification->specification_item_id);
                    }
                }

            @endphp

            //console.log(<?php echo json_encode($new_images); ?>);
            //console.log('specificationCategoryID:' + <?php echo $specificationCategoryID; ?>);

            var images = jQuery.parseJSON(JSON.stringify(<?php echo json_encode($new_images); ?>));
            if(images.length > 0) {
                console.log(images);
                images.forEach(function(image) {
                    console.log(image);
                    addImage(image);
                });
            }

            var specification_category_id = <?php echo $specificationCategoryID; ?>;
            specification_list = jQuery.parseJSON(JSON.stringify(<?php echo json_encode($specification_list); ?>));
            if (specification_category_id !== 0) {
                //console.log('specification_category_id:' + specification_category_id);
                $("#specification_category").val(specification_category_id).change();
            }

        }

        function initView() {
            $('input[type="checkbox"].custom-checkbox, input[type="radio"].custom-radio').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue',
                increaseArea: '20%'
            });
        }

        function initEvent() {
            var fileSelect = document.getElementById("btn_file_upload"),
                fileElem = document.getElementById("file_upload");

            fileSelect.addEventListener("click", function (e) {

                if (fileElem) {
                    //console.log("click");
                    fileElem.click();
                }
                //e.preventDefault(); // prevent navigation to "#"
            }, false);

            $("#file_upload").on('change', function () {
                //Get count of selected files
                if($(this)[0].files.length > 0) {
                    console.log("uploadImages");
                    uploadImages($(this)[0].files);
                }

            });

            $("#specification_category").on('change', function ()  {
                var category = $("#specification_category").val();
                //console.log("#specification_category:" + category);
                if(typeof category === 'undefined' || category.length == 0) {
                    //console.log('category === undefined');
                    return false;
                } else {
                    //console.log('/admin/product/specifications_by_category/'+ selected);
                    $.ajax({
                        url: '/admin/product/specifications_by_category/'+ category,
                        method: 'GET',
                        success: function (response) {
                            //alert('Form Submitted!');
                            //console.log(response);
                            var status = response.status;
                            if(status === "success") {
                                $("#table_specification tr").remove();

                                var list = jQuery.parseJSON(JSON.stringify(response.data));
                                //console.log(list);
                                list.forEach(function(value, index, arr) {
                                    var div_items = $('<div></div>');
                                    var items = value.items;
                                    var specification_id = value.id;
                                    console.log('specification_id:'+specification_id);
                                    var isExist = false;
                                    items.forEach(function(value, index, arr) {
                                        //console.log(value);
                                        div_items.append($('<label>')
                                            .attr('class', 'radio-inline')
                                            .append($('<input>')
                                                .attr('type', 'radio')
                                                .attr('class', 'custom-radio')
                                                .attr('name', 'optionsRadiosInline' + specification_id)
                                                .attr('id', 'specification_item_' + value.id)
                                                .attr('value', value.id)
                                            )
                                            .append("&nbsp; " + value.name)
                                        )

                                        if (jQuery.inArray( value.id, specification_list ) !== -1) {
                                            isExist = true;
                                            console.log('--'+value.id+'--');
                                            div_items.find("#specification_item_" + value.id).prop("checked", true);
                                        }
                                    });

                                    $("#table_specification")
                                        .find('tbody')
                                        .append($('<tr>')
                                            .append($('<td>')
                                                .append($('<div>')
                                                    .attr('class', 'checkbox')
                                                    .append($('<label>')
                                                        .append($('<input>')
                                                            .attr('id', value.id)
                                                            .attr('type', 'checkbox')
                                                            .attr('class', 'custom-checkbox')
                                                        )
                                                        .append("&nbsp; " + value.name)
                                                    )
                                                )
                                            )
                                            .append($('<td>')
                                                .append(div_items)
                                            )

                                        )

                                    if (isExist) {
                                        $("#table_specification").find("#" + value.id).prop("checked", true);
                                    }

                                });
                                $('input[type="checkbox"].custom-checkbox, input[type="radio"].custom-radio').iCheck({
                                    checkboxClass: 'icheckbox_minimal-blue',
                                    radioClass: 'iradio_minimal-blue',
                                    increaseArea: '20%'
                                });
                            } else {
                                alert("輸入資料有誤");
                            }

                        },
                        error: function(){
                            alert("上傳資料失敗");
                        }
                    });
                    return false;
                }
            });

            //$("#tryitForm").submit(formSubmit());

            $("#tryitForm").on('submit', function () {
                //console.log('count:'+$('#sortable li').length);
                if ( $('#sortable li').length <= 0 ) {
                    alert("請新增產品照片!");
                    return false;
                }
                var formData = $("#tryitForm").serialize();
                var list = [];

                $('#sortable li').each(function(e){
                    //console.log($(this).attr('data-id'));
                    list.push($(this).attr('data-id'));
                });

                formData += '&images=' + JSON.stringify(list);

                var category = $("#specification_category").val();
                list = [];
                $('#table_specification > tbody  > tr').each(function() {
                    //console.log($(this).children('td').eq(0).find("input").is(":checked"));
                    var isChecked = $(this).children('td').eq(0).find("input").is(":checked");
                    if(isChecked) {
                        console.log('specification_category:'+specification_category);
                        console.log($(this).children('td').eq(0).find("input").attr('id'));
                        var specification = $(this).children('td').eq(0).find("input").attr('id');
                        console.log($(this).children('td').eq(1).find("input[name=optionsRadiosInline"+specification+"]:checked").val());
                        var item = $(this).children('td').eq(1).find("input[name=optionsRadiosInline"+specification+"]:checked").val();
                        if(typeof item !== 'undefined') {
                            list.push({'specification_category_id':category, 'specification_id':specification, 'specification_item_id':item});
                        }

                    }

                });
                formData += '&product_specifications=' + JSON.stringify(list);
                console.log(formData);

                var method = 'POST';
                @if(isset($banner->id) && !$isCopy)
                    method = 'PUT';
                @endif
                $.ajax({
                    url: "{{ $url }}",
                    method: method,
                    data: formData,
                    success: function (data) {
                        //alert('Form Submitted!');
                        console.log(data);
                        var obj = jQuery.parseJSON(JSON.stringify(data));
                        var status = obj.status;
                        if(obj.status === "success") {
                            window.location.replace("{{ route('admin.products.index') }}");
                        } else {
                            var data = obj.data;
                            var message = data.message;
                            if(!message || message == '') {
                                message = "輸入資料有誤";
                            }
                            alert(message);
                        }

                    },
                    error: function(){
                        alert("上傳資料失敗");
                    }
                });

                return false;
            });

        }



        function initSortable() {
            //var el = document.getElementById('sortable');
            var sortable = Sortable.create($("#sortable")[0], {
                group: "items",  // or { name: "...", pull: [true, false, clone], put: [true, false, array] }
                animation: 150,  // ms, animation speed moving items when sorting, `0` — without animation
                filter: ".btnAdd",
                ghostClass: "sortable-ghost",  // Class name for the drop placeholder
                chosenClass: "sortable-chosen",  // Class name for the chosen item
                forceFallback: false,  // ignore the HTML5 DnD behaviour and force the fallback to kick in
            });

            Sortable.create($("#trash_container")[0], {
                group: "items",
                onAdd: function (evt) {
                    //console.log(this.el);
                    this.el.removeChild(evt.item);
                }
            });
        }

        function uploadImages(files){
            var fileLimit = 0; //限制檔案數量 0: 不限制 1以上就限制數值上所寫之數量
            var fileSize = 2048000; //限制檔案大小 0: 不限制 1以上就限制數值上所寫之數量
            //var total = files.length + $('#sortable li').length;

            if (fileLimit != 0) {
                if (files.length + $('#sortable li').length > fileLimit) {
                    alert('上傳圖檔超過 ' + fileLimit + ' 個檔案');
                    return false;
                }
            }

            var form_data = new FormData();

            for (var i = 0; i < files.length; i++) {
                //判斷是否為圖片
                /*if (!files.type.match('image.*')) {
                    continue;
                }*/

                //判斷上傳檔案大小(MAX 100kBytes)
                if (fileSize != 0) {
                    if (files[i].size <= fileSize) {
                        //continue;
                        form_data.append('file[]', files[i]);
                    } else {

                    }
                }
            }

            //console.log('call ajax');
            form_data.append('type', 'product');
            form_data.append('height', 600);
            form_data.append('width', 600);

            $.ajax({
                url: '/admin/upload/images', // point to server-side PHP script
                dataType: 'json',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(data){
                    console.log(data);
                    var obj = jQuery.parseJSON(JSON.stringify(data));
                    console.log('successful:'+ obj.data.id);

                    addImage(obj.data);

                },
                error: function (error) {
                    console.log('error');
                },
                complete: function (xhr) {
                    console.log('complete');
                }
            });

        }

        function addImage(data) {

            $("#sortable").append(
                $('<li>')
                    .attr('data-id', data.id)
                    .append(
                        $('<img>')
                            .attr('width', '160')
                            .attr('height', '160')
                            .attr('src', data.image)
                            .attr('class', 'thumb-image')
                    )
            );

        }

        var editor_config = {
            path_absolute : "/admin/",
            selector: "textarea.my-editor",
            language:'zh_TW',
            setup: function (editor) {
                editor.on('init change', function () {
                    editor.save();
                });
            },
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak autosave",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);

    </script>
@stop
