@extends('admin.layouts.page')

@section('title', 'Aidlite')

@section('content_header')
    <h1>產品規格列表</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.home') }}">
                <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="{{ route('admin.product.specifications.list') }}">產品規格分類列表</a>
        </li>
        <li class="active">
            產品規格列表
        </li>

    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable">
                <div class="panel-heading clearfix">
                    <div class="pull-left">
                        <i class="livicon" data-name="adjust" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                    </div>
                    <div class="pull-right">
                        <a href="{{ route('admin.product.specifications.list') }}" class="btn btn-primary btn-sm">
                            <span>回上一層</span>
                        </a>
                        <a href="{{ route('admin.product.specifications.create', ['category_id' => $category_id]) }}" class="btn btn-primary btn-sm">
                            <span><i class="fa fa-plus" id="addButton"></i>&nbsp;&nbsp;新增產品規格</span>
                        </a>
                    </div>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table" id="table3">
                        <thead>
                        <tr>
                            <th>名稱</th>
                            <th>分類</th>
                            <th>排序</th>
                            <th>時間</th>
                            <th>動作</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr>
                                <td>{{ $item->name }}</td>
                                <td>
                                    @if (isset($item->category))
                                        {{ $item->category->name }}
                                    @endif
                                </td>
                                <td>{{ $item->sort }}</td>
                                <td>{{ $item->updated_at }}</td>
                                <td>
                                    <a href="{{ route('admin.product.specification.items.index', ['id' => $item->id, 'category_id' => $category_id]) }}">
                                        <span class="btn btn-info"><i class="fa fa-eye"></i></span>
                                    </a>
                                    <a href="{{ route('admin.product.specifications.edit', ['id' => $item->id, 'category_id' => $category_id]) }}">
                                        <span class="btn btn-success"><i class="fa fa-edit"></i></span>
                                    </a>
                                    <a href="#modal-danger" data-toggle="modal" data-target="#modal-danger" data-variable="{{ route('admin.product.specifications.destroy', ['id' => $item->id, 'category_id' => $category_id]) }}">
                                        <span class="btn btn-danger"><i class="fa fa-trash"></i></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {!! $items->appends(request()->except('page'))->links() !!}
        </div>
    </div>

    <!--window-->
    <div class="modal fade" id="modal-danger" role="document">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4><i class="icon fa fa-ban"></i> 提醒</h4>
                </div>
                <div class="modal-body">
                    <p>確定要刪除嗎？</p>
                </div>
                <div class="modal-footer">

                    <form action="" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <button type="button" data-dismiss="modal" class="btn btn-default">取消</button>
                        <input type="submit" class="btn btn-primary" value="確定">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--window-->
@stop

@section('js')
    <script>

        $('#modal-danger').on('show.bs.modal', function(e) {
            var $target = $(e.relatedTarget);
            var variable = $target.attr('data-variable');
            console.log(variable);
            $('#delete_form').attr('action', variable);
//            var $modal = $(this);
//            var esseyId = e.relatedTarget.id;
//            $modal.find('.modal-body').html(esseyId);
        })

        @if (session('error') == 1)
        alert('請先刪除所有產品規格項目，再刪除此產品規格');
        @endif

    </script>
@stop