@extends('admin.layouts.page')

@section('title', 'Aidlite')

@section('content_header')
    <h1>產品列表</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.home') }}">
                <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li class="active">
            產品列表
        </li>

    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable">
                <div class="panel-heading clearfix">
                    <div class="col-lg-3 pull-left" style="display: inline-block;">
                        <form name="productCategorySearch" method="get" >
                            <label class="select-inline mar-left5">
                                <select name="category_id" class="form-control select2" >
                                    <option value="">請選擇</option>
                                    @foreach($subCategories as $subCategory)
                                    <option value="{{ $subCategory->id }}" @if ($category_id == $subCategory->id) selected @endif @if ($subCategory->layer == 0) disabled @endif >
                                    @if ($subCategory->layer > 0)
                                    @for ($i = 0; $i < $subCategory->layer; $i++)
                                    &nbsp;&nbsp;
                                    @endfor
                                    @endif
                                    {{ $subCategory->name }}
                                    </option>
                                    @endforeach
                                </select>
                            </label>
                            <label class="input-inline">
                                <button type="submit" style="margin-bottom: 3px;" class="btn btn-effect-ripple btn-primary">搜尋</button>
                            </label>
                        </form>
                    </div>
                    <div class="pull-right">
                        <a href="{{ route('admin.products.create') }}" class="btn btn-primary btn-sm">
                            <span><i class="fa fa-plus" id="addButton"></i>&nbsp;&nbsp;新增產品</span>
                        </a>
                    </div>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table" id="table3">
                        <thead>
                        <tr>
                            <th>圖</th>
                            <th>標題</th>
                            <th>分類</th>
                            <th>型號</th>
                            <!--th>推薦</th-->
                            <th>時間</th>
                            <th>動作</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td><img class="news-block-img" src="@if(isset($product->images[0]->image)){{ Storage::url($product->images[0]->image) }}@endif" data-src="@if(isset($product->images[0]->image)){{ Storage::url($product->images[0]->image) }}@endif" alt="@if(isset($product->alt)){{ $product->alt }}@endif" style="width: 50px;"></td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->productCategory['name'] }}</td>
                                <td>{{ $product->model }}</td>
                                <!--td>@if ($product->featured == 1) 開啟 @else  關閉 @endif</td-->
                                <td>{{ $product->updated_at }}</td>
                                <td>
                                    <a href="/product/{{ $product->id }}">
                                        <span class="btn btn-info"><i class="fa fa-eye"></i></span>
                                    </a>
                                    <a href="{{ route('admin.products.edit', ['id' => $product->id]) }}">
                                        <span class="btn btn-success"><i class="fa fa-edit"></i></span>
                                    </a>
                                    <a href="{{ route('admin.products.copy', ['id' => $product->id]) }}">
                                        <span class="btn btn-primary"><i class="fa fa-copy"></i></span>
                                    </a>
                                    <a href="#modal-danger" data-toggle="modal" data-target="#modal-danger" data-variable="{{ route('admin.products.destroy', ['id' => $product->id]) }}">
                                        <span class="btn btn-danger"><i class="fa fa-trash"></i></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {!! $products->appends(request()->all())->links() !!}
        </div>
    </div>

    <!--window-->
    <div class="modal fade" id="modal-danger" role="document">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4><i class="icon fa fa-ban"></i> 提醒</h4>
                </div>
                <div class="modal-body">
                    <p>確定要刪除嗎？</p>
                </div>
                <div class="modal-footer">

                    <form action="" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <button type="button" data-dismiss="modal" class="btn btn-default">取消</button>
                        <input type="submit" class="btn btn-primary" value="確定">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--window-->
@stop

@section('js')
    <script>
        $('#modal-danger').on('show.bs.modal', function(e) {
            var $target = $(e.relatedTarget);
            var variable = $target.attr('data-variable');
            console.log(variable);
            $('#delete_form').attr('action', variable);

        })
    </script>
@stop
