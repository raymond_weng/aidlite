@extends('admin.layouts.page')

@section('css')
    <link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen" />
    <link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/all.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/line/line.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/switchery/css/switchery.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/awesome-bootstrap-checkbox/css/awesome-bootstrap-checkbox.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/radio_checkbox.css') }}">

    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/intl-tel-input/css/intlTelInput.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/pages/form3.css') }}" rel="stylesheet" />

@stop

@section('title', 'Aidlite')

@section('content_header')
    <h1>關於我們</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.home') }}">
                <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li class="active">關於我們</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                @php
                    $url = route('admin.page.store');
                    if(isset($page->id)) {
                        $url = route('admin.page.update', ['id' => $page->id]);
                    }
                @endphp
                <div class="panel-heading">
                    <i class="livicon" data-name="info" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                </div>
                <div class="panel-body border">
                    <form id="tryitForm" class="form-horizontal form-bordered" role="form" action="{{ $url }}" method="POST">
                        <!-- PUT Method if we are editing -->
                        @if(isset($page->id))
                            {{ method_field("PUT") }}
                        @endif
                        {{ csrf_field() }}
                        <!-- text input -->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-textarea-input">內容</label>
                            <div class="col-md-8 bootstrap-admin-panel-content">
                                <textarea name="content" class="form-control my-editor" style="height: 300px;">@if (isset($page->content)){{ old('content', $page->content) }}@else{{ old('content') }}@endif</textarea>
                            </div>
                            @if ($errors->has('content'))
                                <div class="col-md-8 col-md-offset-2">
                                    <span class="text-danger">內容未填或格式錯誤</span>
                                </div>
                            @endif
                        </div>


                        <div class="form-group form-actions">
                            <div class="col-md-8 col-md-offset-2">
                                <button id="Submit" type="submit" class="btn btn-effect-ripple btn-primary">送出</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('assets/vendors/tinymce/tinymce.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/editor1.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/switchery/js/switchery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/card/lib/js/jquery.card.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/radio_checkbox.js') }}"></script>

    <script>
        var editor_config = {
            path_absolute : "/admin/",
            selector: "textarea.my-editor",
            language:'zh_TW',
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);

        $('#Submit').click(function() {
            alert('儲存完畢!!');
        });
    </script>
@stop
