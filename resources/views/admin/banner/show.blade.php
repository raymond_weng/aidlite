@extends('admin.layouts.page')

@section('title', 'Aidlite')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.css') }}" />
    <link href="{{ asset('admin/css/pages/alertmessage.css') }}" rel="stylesheet" type="text/css" />

@stop

@section('content_header')

    <h1>Banner</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.home') }}">
                <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="{{ route('admin.banners.index') }}">Banner 列表</a>
        </li>
        <li class="active">Banner</li>
    </ol>

@stop

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="image" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    </h3>
                    <span class="pull-right">{{ $banner->created_at }}</span>
                </div>
                <div class="panel-body">
                    <div>
                        @if(isset($banner->url))
                            <a href="{{ $banner->url }}" target="{{ $banner->target }}">
                        @endif
                        <img class="news-block-img" src="{{ Storage::url($banner->images[0]->image) }}" data-src="{{ Storage::url($banner->images[0]->image) }}" alt=@if(isset($banner->alt))"{{ $banner->alt }}"@else"{{ $banner->name }}"@endif style="width: 800px;">
                        @if(isset($banner->url))
                            </a>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop

