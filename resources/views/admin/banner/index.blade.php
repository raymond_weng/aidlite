@extends('admin.layouts.page')

@section('title', 'Aidlite')

@section('content_header')
    <h1>Banner 列表</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.home') }}">
                <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li class="active">
            Banner 列表
        </li>

    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable">
                <div class="panel-heading clearfix">
                    <div class="pull-left">
                        <i class="livicon" data-name="image" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                    </div>
                    <div class="pull-right">
                        <a href="{{ route('admin.banners.create') }}" class="btn btn-primary btn-sm">
                            <span><i class="fa fa-plus" id="addButton"></i>&nbsp;&nbsp;新增 Banner</span>
                        </a>
                    </div>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table" id="table3">
                        <thead>
                        <tr>
                            <th>標題</th>
                            <th>連結</th>
                            <th>狀態</th>
                            <th>時間</th>
                            <th>動作</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($banners as $banner)
                            <tr>
                                <td>{{ $banner->name }}</td>
                                <td>{{ $banner->url }}</td>
                                <td>
                                    @if ($banner->status)
                                        開啟
                                    @else
                                        關閉
                                    @endif
                                </td>
                                <td>{{ $banner->updated_at }}</td>
                                <td>
                                    <a href="{{ route('admin.banners.edit', ['id' => $banner->id]) }}">
                                        <span class="btn btn-success"><i class="fa fa-edit"></i></span>
                                    </a>
                                    <a href="#modal-danger" data-toggle="modal" data-target="#modal-danger" data-variable="{{ route('admin.banners.destroy', ['id' => $banner->id]) }}">
                                        <span class="btn btn-danger"><i class="fa fa-trash"></i></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {!! $banners->links() !!}
        </div>
    </div>

    <!--window-->
    <div class="modal fade" id="modal-danger" role="document">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4><i class="icon fa fa-ban"></i> 提醒</h4>
                </div>
                <div class="modal-body">
                    <p>確定要刪除嗎？</p>
                </div>
                <div class="modal-footer">

                    <form action="" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <button type="button" data-dismiss="modal" class="btn btn-default">取消</button>
                        <input type="submit" class="btn btn-primary" value="確定">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--window-->
@stop

@section('js')
    <script>

        $('#modal-danger').on('show.bs.modal', function(e) {
            var $target = $(e.relatedTarget);
            var variable = $target.attr('data-variable');
            console.log(variable);
            $('#delete_form').attr('action', variable);
//            var $modal = $(this);
//            var esseyId = e.relatedTarget.id;
//            $modal.find('.modal-body').html(esseyId);
        })

    </script>
@stop
