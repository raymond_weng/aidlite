@extends('admin.layouts.page')

@section('title', 'Aidlite')

@section('content_header')
    <h1>Banner分類列表</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.home') }}">
                <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li class="active">
            Banner分類列表
        </li>

    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable">
                <div class="panel-heading clearfix">
                    <div class="pull-left">
                        <i class="livicon" data-name="bell" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                    </div>
                    <div class="pull-right">
                        <a href="{{ route('admin.banner.categories.create') }}" class="btn btn-primary btn-sm">
                            <span><i class="fa fa-plus" id="addButton"></i>&nbsp;&nbsp;新增分類</span>
                        </a>
                    </div>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table" id="table3">
                        <thead>
                        <tr>
                            <th>名稱</th>
                            <th>編碼</th>
                            <th>時間</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->name }}</td>
                                <td>{{ $category->code }}</td>
                                <td>{{ $category->updated_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {!! $categories->links() !!}
        </div>
    </div>
@stop

@section('js')

    <script type="application/javascript">

        $('#modal-danger').on('show.bs.modal', function(e) {
            var $target = $(e.relatedTarget);
            var variable = $target.attr('data-variable');
            console.log(variable);
            $('#delete_form').attr('action', variable);

        })

        $(document).ready(function() {

        });

    </script>

@stop