@extends('admin.layouts.page')

@section('css')
    <link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen" />
    <link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/all.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/line/line.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/switchery/css/switchery.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/awesome-bootstrap-checkbox/css/awesome-bootstrap-checkbox.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/radio_checkbox.css') }}">

    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/intl-tel-input/css/intlTelInput.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/pages/form3.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/bootstrap-touchspin/css/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/vendors/jquery-spinner/css/bootstrap-spinner.css') }}" rel="stylesheet" />

@stop

@section('title', 'Aidlite')

@section('content_header')
    <h1>@if(isset($category->id))編輯@else新增@endif 產品分類</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.home') }}">
                <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="{{ route('admin.product.category.index') }}">產品分類列表</a>
        </li>
        <li class="active">@if(isset($category->id))編輯@else新增@endif產品分類</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                @php
                    $url = route('admin.product.category.store');
                    if(isset($category->id)) {
                        $url = route('admin.product.category.update', ['id' => $category->id]);
                    }
                @endphp
                <div class="panel-heading">
                    <i class="livicon" data-name="adjust" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                </div>
                <div class="panel-body border">
                    <form id="tryitForm" class="form-horizontal form-bordered" role="form" action="{{ $url }}" method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(isset($category->id))
                            {{ method_field("PUT") }}
                        @endif
                        {{ csrf_field() }}
                        <!-- text input -->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input">標題</label>
                            <div class="col-md-8">
                                <input type="text" id="form-text-input" name="name" class="form-control" placeholder="標題"
                                       value="@if(isset($category->name)){{ old('name', $category->name) }}@else{{ old('name') }}@endif" required>
                            </div>
                            @if ($errors->has('name'))
                                <div class="col-md-8 col-md-offset-2">
                                    <span class="text-danger">標題未填或格式錯誤</span>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input">上層分類</label>
                            <div class="col-md-3">
                                <select id="select" name="category_id" class="form-control select2" required>
                                    <option value="">請選擇...</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" @if((isset($product->category_id) && $product->category_id == $category->id) || old('category_id') == $category->id){{ 'selected="selected"' }}@endif>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <label class="col-md-2 control-label " for="form-text-input">排序</label>
                            <div class="col-md-2 input-group spinner" data-trigger="spinner">
                                <input name="sort" type="text" class="form-control" value="@if(isset($category->sort)){{ old('sort', $category->sort) }}@else{{ old('sort') }}@endif" data-rule="quantity">
                                <div class="input-group-addon pickers_spinners">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="fa fa-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="fa fa-chevron-down"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-effect-ripple btn-primary">送出</button>
                                <button type="reset" class="btn btn-effect-ripple btn-default">重設</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('assets/vendors/tinymce/tinymce.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/editor1.js') }}" type="text/javascript"></script>

    <script type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/switchery/js/switchery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/card/lib/js/jquery.card.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/radio_checkbox.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-touchspin/js/jquery.bootstrap-touchspin.js') }}"></script>
    <script src="{{ asset('assets/vendors/jquery-spinner/js/jquery.spinner.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/pickers.js') }}"></script>
    <script>
        $("#fileUpload").on('change', function () {
            //Get count of selected files
            var countFiles = $(this)[0].files.length;
            var imgPath = $(this)[0].value;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var image_holder = $("#preview");
            image_holder.empty();
            console.log(imgPath);
            if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                if (typeof (FileReader) != "undefined") {
                    //loop for each file selected for uploaded.
                    for (var i = 0; i < countFiles; i++) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#preview').attr('src',e.target.result);
                            $('#preview').attr('class',"thumb-image");
                        }
                        reader.readAsDataURL($(this)[0].files[i]);
                    }
                } else {
                    alert("This browser does not support FileReader.");
                }
            } else {
                alert("Pls select only images");
            }
        });

        function chaneImage(val) {
            console.log(val);
            $('#preview').attr('src',val);
            $('#preview').attr('class',"thumb-image");
        }
    </script>
@stop
