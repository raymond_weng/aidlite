@extends('admin.layouts.page')

@section('css')
    <link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen" />
    <link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/all.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/line/line.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/switchery/css/switchery.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/awesome-bootstrap-checkbox/css/awesome-bootstrap-checkbox.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/radio_checkbox.css') }}">

    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/intl-tel-input/css/intlTelInput.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/pages/form3.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/jquery-spinner/css/bootstrap-spinner.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />

@stop

@section('title', 'Aidlite')

@section('content_header')
    <h1>@if(isset($info->id))編輯@else新增@endif訊息</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.home') }}">
                <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="{{ route('admin.info.index') }}">訊息列表</a>
        </li>
        <li class="active">@if(isset($info->id))編輯@else新增@endif訊息</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                @php
                    $url = route('admin.info.store');
                    if(isset($info->id)) {
                        $url = route('admin.info.update', ['id' => $info->id]);
                    }
                @endphp
                <div class="panel-heading">
                    <i class="livicon" data-name="bell" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                </div>
                <div class="panel-body border">
                    <form id="tryitForm" class="form-horizontal form-bordered" role="form" action="{{ $url }}" method="POST">
                        <!-- PUT Method if we are editing -->
                        @if(isset($info->id))
                            {{ method_field("PUT") }}
                        @endif
                        {{ csrf_field() }}
                        <!-- text input -->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input">分類</label>
                            <div class="col-md-5">
                                <select id="select" name="category_id" class="form-control select2" required>
                                    <option value="">請選擇...</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" @if(isset($info->category_id) && $info->category_id == $category->id){{ 'selected="selected"' }}@endif>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <label class="col-md-1 control-label " for="form-text-input">排序</label>
                            <div class="col-md-2 input-group spinner" data-trigger="spinner">
                                <input name="sort" type="text" class="form-control" value="@if(isset($category->sort)){{ old('sort', $category->sort) }}@else{{ old('sort') }}@endif" data-rule="quantity">
                                <div class="input-group-addon pickers_spinners">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="fa fa-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="fa fa-chevron-down"></i></a>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input">標題</label>
                            <div class="col-md-5">
                                <input type="text" id="form-text-input" name="title" class="form-control" placeholder="標題" required
                                       value="@if(isset($info->title)){{ old('title', $info->title) }}@else{{ old('title') }}@endif">
                            </div>
                            <label class="col-md-1 control-label" for="form-text-input">狀態</label>
                            <div class="col-md-2">
                                <input type="checkbox" name="my-checkbox" data-on-color="primary" data-off-color="info" @if(!isset($info->status) || $info->status == 1) checked @endif>
                            </div>
                            @if ($errors->has('title'))
                                <div class="col-md-8 col-md-offset-2">
                                    <span class="text-danger">標題未填或格式錯誤</span>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="take_office_date"><span style="color: red;">*</span> 發文日期</label>
                            <div class="col-md-4">
                                @php
                                    $now = new DateTime();
                                @endphp
                                <input type="text" id="post_date" name="post_date" class="form-control" placeholder="2018-01-01" value="@if(isset($info->post_date)){{ 
                                old('post_date', \Carbon\Carbon::parse($info->post_date)->format('Y-m-d')) }}@else{{ \Carbon\Carbon::parse(old('post_date', $now->format('Y-m-d')))->format('Y-m-d') }}@endif" maxlength="10" minlength="10" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-textarea-input">關鍵字</label>
                            <div class="col-md-8">
                                <input type="text" id="form-text-input" name="seo_keyword" class="form-control"
                                       value="@if(isset($info->seo_keyword)){{ old('seo_keyword', $info->seo_keyword) }}@else{{ old('seo_keyword') }}@endif">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-textarea-input">摘要</label>
                            <div class="col-md-8">
                                <textarea id="form-textarea-input" name="excerpt" rows="4" class="form-control resize_vertical" placeholder="摘要..">@if (isset($info->excerpt)){{ old('excerpt', $info->excerpt) }}@else{{ old('excerpt') }}@endif</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-textarea-input">內容</label>
                            <div class="col-md-8 bootstrap-admin-panel-content">
                                <textarea class="form-control my-editor" name="content" style="height: 300px;">@if (isset($info->content)){{ old('content', $info->content) }}@else{{ old('content') }}@endif</textarea>
                            </div>
                            @if ($errors->has('content'))
                                <div class="col-md-8 col-md-offset-2">
                                    <span class="text-danger">內容未填或格式錯誤</span>
                                </div>
                            @endif
                        </div>


                        <div class="form-group form-actions">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-effect-ripple btn-primary">送出</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('assets/vendors/tinymce/tinymce.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/editor1.js') }}" type="text/javascript"></script>

    <script type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/switchery/js/switchery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/card/lib/js/jquery.card.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/radio_checkbox.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-touchspin/js/jquery.bootstrap-touchspin.js') }}"></script>
    <script src="{{ asset('assets/vendors/jquery-spinner/js/jquery.spinner.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/pickers.js') }}"></script>
    <script>
        $('input[name="post_date"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            locale: { format: "YYYY-MM-DD" }
            @if (isset($info->post_date))
            ,startDate: new Date($.datepicker.parseDate('yy-mm-dd', '{{ $info->post_date }}'))
            @endif
        });

        $('input[name="post_date"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD'));
        });

        var editor_config = {
            path_absolute : "/admin/",
            selector: "textarea.my-editor",
            language:'zh_TW',
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak autosave",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>
@stop
