@extends('admin.layouts.page')

@section('title', 'Aidlite')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.css') }}" />
    <link href="{{ asset('assets/css/pages/alertmessage.css') }}" rel="stylesheet" type="text/css" />

@stop

@section('content_header')

    <h1>資訊</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.home') }}">
                <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="{{ route('admin.news.index') }}">資訊列表</a>
        </li>
        <li class="active">資訊</li>
    </ol>

@stop

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="notebook" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>{{ $news->category['name'] }}
                    </h3>
                    <span class="pull-right">{{ $news->created_at }}</span>
                </div>
                <div class="panel-body">
                    <div class="alert-message alert-message-success">
                        <h4>{{ $news->title }}</h4>
                        <p>{{ $news->excerpt }}</p>
                    </div>
                    <div>{!! html_entity_decode($news->body) !!}</div>
                </div>
            </div>
        </div>
    </div>
@stop

