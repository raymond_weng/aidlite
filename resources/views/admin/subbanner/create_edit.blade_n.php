@extends('admin.layouts.page')

@section('css')
    <link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen" />
    <link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/all.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/line/line.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/switchery/css/switchery.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/awesome-bootstrap-checkbox/css/awesome-bootstrap-checkbox.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/radio_checkbox.css') }}">

    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/intl-tel-input/css/intlTelInput.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/pages/form3.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/bootstrap-touchspin/css/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/vendors/jquery-spinner/css/bootstrap-spinner.css') }}" rel="stylesheet" />
    <style type="text/css">
        #sortable li {
            display:inline;
            width: 60px;
            height: 60px;
            text-align: center;
        }
        #sortable_layout li {
            display:inline;
            width: 60px;
            height: 60px;
            text-align: center;
        }

        i#plus > svg {
            top: 5px;
        }

    </style>
@stop

@section('title', 'Aidlite')

@section('content_header')
    <h1>@if(isset($banner->id))編輯@else新增@endif SubBanner</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.home') }}">
                <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="{{ route('admin.sub.banners.index') }}">SubBanner 列表</a>
        </li>
        <li class="active">@if(isset($banner->id))編輯@else新增@endif SubBanner</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                @php
                    $url = route('admin.sub.banners.store');
                    if(isset($banner->id)) {
                        $url = route('admin.sub.banners.update', ['id' => $banner->id]);
                    }
                @endphp
                <div class="panel-heading">
                    <i class="livicon" data-name="image" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                </div>
                <div class="panel-body border">
                    <form id="tryitForm" class="form-horizontal form-bordered" role="form" action="{{ $url }}" method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(isset($banner->id))
                            {{ method_field("PUT") }}
                        @endif
                        {{ csrf_field() }}
                        <!-- text input -->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input">標題</label>
                            <div class="col-md-8">
                                <input type="text" id="form-text-input" name="name" class="form-control" placeholder="標題"
                                       value="@if(isset($banner->name)){{ old('name', $banner->name) }}@else{{ old('name') }}@endif" required>
                            </div>
                            @if ($errors->has('name'))
                                <div class="col-md-8 col-md-offset-2">
                                    <span class="text-danger">標題未填或格式錯誤</span>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input">連結</label>
                            <div class="col-md-8">
                                <input type="text" id="form-text-input" name="url" class="form-control" placeholder="連結"
                                       value="@if(isset($banner->url)){{ old('url', $banner->url) }}@else{{ old('url') }}@endif">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input">alt</label>
                            <div class="col-md-3">
                                <input type="text" id="form-text-input" name="alt" class="form-control"
                                       value="@if(isset($banner->alt)){{ old('alt', $banner->alt) }}@else{{ old('alt') }}@endif">
                            </div>
                            <label class="col-md-2 control-label " for="form-text-input">target</label>
                            <div class="col-md-3">
                                <select id="select" name="target" class="form-control select2" required>
                                    <option value="_self">請選擇...</option>
                                    <option value="_self" @if((isset($banner->target) && $banner->target == "_self") || old('target') == "_self"){{ 'selected="selected"' }}@endif>_self</option>
                                    <option value="_blank" @if((isset($banner->target) && $banner->target == "_blank") || old('target') == "_blank"){{ 'selected="selected"' }}@endif>_blank</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input">狀態</label>
                            <div class="col-md-3">
                                <input type="checkbox" name="my-checkbox" @if(isset($banner->status) && $banner->status == 1) checked @endif data-on-color="primary" data-off-color="info">
                            </div>
                            <label class="col-md-2 control-label " for="form-text-input">排序</label>
                            <div class="col-md-3 input-group spinner" data-trigger="spinner">
                                <input name="sort" type="text" class="form-control" value="@if(isset($banner->sort)){{ old('sort', $banner->sort) }}@else{{ old('sort') }}@endif" data-rule="quantity">
                                <div class="input-group-addon pickers_spinners">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="fa fa-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="fa fa-chevron-down"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input">圖</label>
                            <div class="col-md-8" style="position:relative;">

                                <ul id="sortable" style="position:static;">
                                    <li>
                                        <div id="btn_fileUpload" style="position:relative;text-align: center; line-height: 65px; border-width:3px;border-style:dashed;border-color:#FFAC55;padding:5px;width: 60px; height: 60px;">
                                            <i class="livicon" style="text-align: center;" data-name="plus" data-size="30" data-c="#FFAC55" data-hc="#FFAC55" data-loop="true"></i>
                                            <!--input id="fileUpload" name="image" type="file" style="display:none" /-->
                                            <input id="fileUpload" name="image" type="file" style="display: none;" />
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            @if ($errors->has('image'))
                                <div class="col-md-8 col-md-offset-2">
                                    <span class="text-danger">未上傳圖檔</span>
                                </div>
                            @endif
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" id="submit-all" class="btn btn-effect-ripple btn-primary">送出</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('assets/vendors/tinymce/tinymce.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/editor1.js') }}" type="text/javascript"></script>

    <script type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/switchery/js/switchery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/card/lib/js/jquery.card.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/radio_checkbox.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-touchspin/js/jquery.bootstrap-touchspin.js') }}"></script>
    <script src="{{ asset('assets/vendors/jquery-spinner/js/jquery.spinner.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/pickers.js') }}"></script>
    <script>
        $( function() {

            var fileSelect = document.getElementById("btn_fileUpload"),
                fileElem = document.getElementById("fileUpload");

            fileSelect.addEventListener("click", function (e) {

                if (fileElem) {
                    console.log("click");
                    fileElem.click();
                }
                //e.preventDefault(); // prevent navigation to "#"
            }, false);

            $("#fileUpload").on('change', function () {
                //Get count of selected files
                var countFiles = $(this)[0].files.length;
                var imgPath = $(this)[0].value;
                var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
                console.log(imgPath);
                if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                    if (typeof (FileReader) != "undefined") {
                        //loop for each file selected for uploaded.
                        for (var i = 0; i < countFiles; i++) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = new Image(60, 60);
                                img.src = e.target.result;
                                img.class = "thumb-image";
                                img.style.marginRight = "10px";
                                //$("#image_container").prepend(img);

                                $("#sortable").prepend($('<li>').append(img));
                                //$("#sortable_layout").prepend($('<li>'));

                            }
                            reader.readAsDataURL($(this)[0].files[i]);
                        }
                    } else {
                        alert("此瀏灠器不支援 FileReader.");
                    }
                } else {
                    alert("請選擇圖檔");
                }
            });

            function upload(files){
                /*
                var fileLimit = 0; //限制檔案數量 0: 不限制 1以上就限制數值上所寫之數量
                var fileSize = 102400; //限制檔案大小 0: 不限制 1以上就限制數值上所寫之數量
                //var total = files.length + $('#sortable li').length;

                if (fileLimit != 0) {
                    if (files.length + $('#sortable li').length > fileLimit) {
                        alert('Upload files over ' + fileLimit + ' file(s).');
                        return false;
                    }
                }

                var form_data = new FormData();

                for (var i = 0; i < files.length; i++) {
                    //判斷是否為圖片
//                    if (!files.type.match('image.*')) {
//                        continue;
//                    }

                    //判斷上傳檔案大小(MAX 100kBytes)
                    if (fileSize != 0) {
                        if (files[i].size <= fileSize) {
                            //continue;
                            form_data.append('file[]', files[i]);
                        }
                    }

                    //form_data.append('file[]', files[i]);
                    //form_data.append("file[]", $('#sortpicture').prop('files')[i]);
                }
                */
                $.ajax({
                    url: '/admin/upload/images', // point to server-side PHP script
                    dataType: 'json',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(result){
                        $.each(result,function(index,element){
                            $('#sortable').append('<li class="ui-state-default"><img src="uploads/' + element.imagePIC + '" width="100" height="90" /><br><input type="button" class="delete" value="Delete" /></li>');
                        });

                        $("#sortable").sortable();
                        $("#sortable").disableSelection();
                    }
                });

            }

            $.ajax({
                url: "ajax_php_file.php", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(data)   // A function to be called if request succeeds
                {
                    $('#loading').hide();
                    $("#message").html(data);
                }
            });

        });
    </script>
@stop
