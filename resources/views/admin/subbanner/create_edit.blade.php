@extends('admin.layouts.page')

@section('css')
    <link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen" />
    <link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/all.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/line/line.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/awesome-bootstrap-checkbox/css/awesome-bootstrap-checkbox.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/radio_checkbox.css') }}">

    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/intl-tel-input/css/intlTelInput.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/pages/form3.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/bootstrap-touchspin/css/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/vendors/jquery-spinner/css/bootstrap-spinner.css') }}" rel="stylesheet" />
    <style type="text/css">
        #sortable{
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            /* 也可以是：display: inline-flex; 此為「行內 Flex」 */
            padding:0;
            margin: 0 auto;
        }
        #sortable > li{
            list-style: none;
            width: 60px;
            height: 60px;
            margin: 5px;
            text-align: center;
        }

        .sortable-ghost {
            opacity: .4;
        }
        /*
        #sortable li {
            display:inline;
            width: 60px;
            height: 60px;
            text-align: center;
        }
        #sortable_layout li {
            display:inline;
            width: 60px;
            height: 60px;
            text-align: center;
        }
        */
        i#plus > svg {
        	top: 5px;
        }
    </style>
@stop

@section('title', 'Aidlite')

@section('content_header')
    <h1>@if(isset($banner->id))編輯@else新增@endif SubBanner</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.home') }}">
                <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="{{ route('admin.sub.banners.index') }}">SubBanner 列表</a>
        </li>
        <li class="active">@if(isset($banner->id))編輯@else新增@endif SubBanner</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                @php
                    $url = route('admin.sub.banners.store');
                    if(isset($banner->id)) {
                        $url = route('admin.sub.banners.update', ['id' => $banner->id]);
                    }
                @endphp
                <div class="panel-heading">
                    <i class="livicon" data-name="image" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                </div>
                <div class="panel-body border">
                    <form id="tryitForm" class="form-horizontal form-bordered" role="form" action="javascript:;" method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(isset($banner->id))
                            {{ method_field("PUT") }}
                        @endif
                        {{ csrf_field() }}
                        <!-- text input -->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input">標題</label>
                            <div class="col-md-8">
                                <input type="text" id="form-text-input" name="name" class="form-control" placeholder="標題"
                                       value="@if(isset($banner->name)){{ old('name', $banner->name) }}@else{{ old('name') }}@endif" required>
                            </div>
                            @if ($errors->has('name'))
                                <div class="col-md-8 col-md-offset-2">
                                    <span class="text-danger">標題未填或格式錯誤</span>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input">連結</label>
                            <div class="col-md-8">
                                <input type="text" id="form-text-input" name="url" class="form-control" placeholder="連結"
                                       value="@if(isset($banner->url)){{ old('url', $banner->url) }}@else{{ old('url') }}@endif">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input">狀態</label>
                            <div class="col-md-3">
                                <input type="checkbox" name="my-checkbox" @if(!isset($banner->status) || $banner->status == 1) checked @endif data-on-color="primary" data-off-color="info">
                            </div>
                            <label class="col-md-2 control-label " for="form-text-input">排序</label>
                            <div class="col-md-3 input-group spinner" data-trigger="spinner">
                                <input name="sort" type="text" class="form-control" value="@if(isset($banner->sort)){{ old('sort', $banner->sort) }}@else{{ old('sort') }}@endif" data-rule="quantity">
                                <div class="input-group-addon pickers_spinners">
                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="fa fa-chevron-up"></i></a>
                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="fa fa-chevron-down"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input"></label>
                            <div class="col-md-10"><p style="color: red; font-size: 16px;">* 圖的尺寸：寬60高60，檔案大小:2M以下，數量:2</p></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-text-input">圖</label>
                            <div class="col-md-8">
                                <div style="display:flex;">
                                    <div class="btn_add" id="btn_file_upload" style="text-align: center; border-width:3px;border-style:dashed;border-color:#FFAC55;padding:5px;margin:5px;width: 60px; height: 60px;display: inline-block;">
                                        <i id="plus" class="livicon" style="text-align: center;" data-name="plus" data-size="30" data-c="#FFAC55" data-hc="#FFAC55" data-loop="true"></i>
                                        <!--input id="fileUpload" name="image" type="file" style="display:none" /-->
                                        <input id="file_upload" name="file" type="file" style="display: none;" />
                                    </div>
                                    <div style="position:relative; border-width:3px;border-style:dashed;border-color:#FF0000;margin:5px;width: 60px; height: 60px;">
                                        <i id="trash" class="fa fa-fw fa-trash-o" style="position:absolute; font-size:30px; color:#FF0000; padding: 13px;"></i>
                                        <div class="trash_container" id="trash_container" style="width: 60px; height: 60px;"></div>
                                    </div>
                                </div>

                                <ul id="sortable">

                                </ul>

                            </div>
                            @if ($errors->has('image'))
                                <div class="col-md-8 col-md-offset-2">
                                    <span class="text-danger">未上傳圖檔</span>
                                </div>
                            @endif
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" id="submit-all" class="btn btn-effect-ripple btn-primary">送出</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('assets/vendors/tinymce/tinymce.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/ckeditor/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/Sortable/js/Sortable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/editor1.js') }}" type="text/javascript"></script>

    <script type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/card/lib/js/jquery.card.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/radio_checkbox.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-touchspin/js/jquery.bootstrap-touchspin.js') }}"></script>
    <script src="{{ asset('assets/vendors/jquery-spinner/js/jquery.spinner.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/pickers.js') }}"></script>

    <script>

        $( function() {

            var fileSelect = document.getElementById("btn_file_upload"),
                fileElem = document.getElementById("file_upload");

            fileSelect.addEventListener("click", function (e) {

                if (fileElem) {
                    console.log("click");
                    fileElem.click();
                }
                //e.preventDefault(); // prevent navigation to "#"
            }, false);

            $("#file_upload").on('change', function () {
                //Get count of selected files
                if($(this)[0].files.length > 0) {
                    upload($(this)[0].files);
                }

            });

            //var el = document.getElementById('sortable');
            var sortable = Sortable.create($("#sortable")[0], {
                group: "items",  // or { name: "...", pull: [true, false, clone], put: [true, false, array] }
                animation: 150,  // ms, animation speed moving items when sorting, `0` — without animation
                filter: ".btnAdd",
                ghostClass: "sortable-ghost",  // Class name for the drop placeholder
                chosenClass: "sortable-chosen",  // Class name for the chosen item
                forceFallback: false,  // ignore the HTML5 DnD behaviour and force the fallback to kick in
            });

            Sortable.create($("#trash_container")[0], {
                group: "items",
                onAdd: function (evt) {
                    //console.log(this.el);
                    this.el.removeChild(evt.item);
                }
            });

            $("#tryitForm").submit(function () {
                if ( $('#sortable li').length > 2 ) {
                    alert("輸入超過2張圖");
                    return false;
                }
                var formData = $(this).serialize();
                var list = [];

                $('#sortable li').each(function(e){
                    console.log($(this).attr('data-id'));
                    list.push($(this).attr('data-id'));
                });

                formData += '&images=' + JSON.stringify(list);
                //formData.append('images', JSON.stringify(list));

                var method = 'POST';
                @if(isset($banner->id))
                method = 'PUT';
                @endif
                $.ajax({
                    url: "{{ $url }}",
                    method: method,
                    data: formData,
                        success: function (data) {
                            //alert('Form Submitted!');
                            console.log(data);
                            var obj = jQuery.parseJSON(JSON.stringify(data));
                            var status = obj.status;
                            if(obj.status === "success") {
                                window.location.replace("{{ route('admin.sub.banners.index') }}");
                            } else {
                                alert("輸入資料有誤");
                            }

                        },
                        error: function(){
                            alert("上傳資料失敗");
                        }
                    });
                return false;
            });

            @php
                $new_images = array();
                if(isset($banner)) {
                    $images = $banner->images;
                    foreach ($images as $image) {
                        array_push($new_images, ["id"=>$image->id, "image"=>Storage::url($image->image)]);
                    }
                }
            @endphp

            console.log(<?php echo json_encode($new_images); ?>);
            var images = jQuery.parseJSON(JSON.stringify(<?php echo json_encode($new_images); ?>));
            if(images.length > 0) {
                console.log(images);
                images.forEach(function(image) {
                    console.log(image);
                    addImage(image);
                });
            }

        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function upload(files){
            var fileLimit = 2; //限制檔案數量 0: 不限制 1以上就限制數值上所寫之數量
            var fileSize = 2048000; //限制檔案大小 0: 不限制 1以上就限制數值上所寫之數量
            //var total = files.length + $('#sortable li').length;

            if (fileLimit != 0) {
                if (files.length + $('#sortable li').length > fileLimit) {
                    alert('上傳圖檔超過 ' + fileLimit + ' 個檔案');
                    return false;
                }
            }

            var form_data = new FormData();

            for (var i = 0; i < files.length; i++) {
                //判斷是否為圖片
                /*if (!files.type.match('image.*')) {
                    continue;
                }*/

                //判斷上傳檔案大小(MAX 100kBytes)
                if (fileSize != 0) {
                    if (files[i].size <= fileSize) {
                        //continue;
                        form_data.append('file[]', files[i]);
                    } else {

                    }
                }
            }

            console.log('call ajax');
            form_data.append('type', 'subbanner');
            form_data.append('width', 60);

            $.ajax({
                url: '/admin/upload/images', // point to server-side PHP script
                dataType: 'json',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(data){
                    console.log(data);
                    var obj = jQuery.parseJSON(JSON.stringify(data));
                    console.log('successful:'+ obj.data.id);

                    addImage(obj.data);

                },
                error: function (error) {
                    console.log('error');
                },
                complete: function (xhr) {
                    console.log('complete');
                }
            });

        }

        function addImage(data) {

            $("#sortable").append(
                $('<li>')
                    .attr('data-id', data.id)
                    .append(
                        $('<img>')
                            .attr('width', '60')
                            .attr('height', '60')
                            .attr('src', data.image)
                            .attr('class', 'thumb-image')
                )
            );

        }
    </script>
@stop
