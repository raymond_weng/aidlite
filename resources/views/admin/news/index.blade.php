@extends('admin.layouts.page')

@section('title', 'Aidlite')

@section('content_header')
    <h1>消息列表</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.home') }}">
                <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li class="active">
            消息列表
        </li>

    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable">
                <div class="panel-heading clearfix">
                    <div class="pull-left">
                        <i class="livicon" data-name="bell" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                    </div>
                    <div class="pull-right">
                        <a href="{{ route('admin.news.create') }}" class="btn btn-primary btn-sm">
                            <span><i class="fa fa-plus" id="addButton"></i>&nbsp;&nbsp;新增消息</span>
                        </a>
                    </div>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table" id="table3">
                        <thead>
                        <tr>
                            <th>標題</th>
                            <th>分類</th>
                            <th>狀態</th>
                            <th>發文日期</th>
                            <th>動作</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($newsList as $news)
                            <tr>
                                <td>{{ $news->title }}</td>
                                <td>{{ $news->category['name'] }}</td>
                                <td>
                                    @if ($news->status)
                                        開啟
                                    @else
                                        關閉
                                    @endif
                                </td>
                                <td>{{ \Carbon\Carbon::parse($news->post_date)->format('Y-m-d') }}</td>
                                <td>
                                    <a href="/news/{{  $news->id }}">
                                        <span class="btn btn-info"><i class="fa fa-eye"></i></span>
                                    </a>
                                    <a href="{{ route('admin.news.edit', ['id' => $news->id]) }}">
                                        <span class="btn btn-success"><i class="fa fa-edit"></i></span>
                                    </a>
                                    <a href="#modal-danger" data-toggle="modal" data-target="#modal-danger" data-variable="{{ route('admin.news.destroy', ['id' => $news->id]) }}">
                                        <span class="btn btn-danger"><i class="fa fa-trash"></i></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {!! $newsList->links() !!}
        </div>
    </div>

    <!--window-->
    <div class="modal fade" id="modal-danger" role="document">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4><i class="icon fa fa-ban"></i> 提醒</h4>
                </div>
                <div class="modal-body">
                    <p>確定要刪除嗎？</p>
                </div>
                <div class="modal-footer">

                    <form action="" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <button type="button" data-dismiss="modal" class="btn btn-default">取消</button>
                        <input type="submit" class="btn btn-primary" value="確定">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--window-->
@stop

@section('js')
    <script>

        $('#modal-danger').on('show.bs.modal', function(e) {
            var $target = $(e.relatedTarget);
            var variable = $target.attr('data-variable');
            console.log(variable);
            $('#delete_form').attr('action', variable);
//            var $modal = $(this);
//            var esseyId = e.relatedTarget.id;
//            $modal.find('.modal-body').html(esseyId);
        })

    </script>
@stop
